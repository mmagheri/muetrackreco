import xml.etree.ElementTree as ET
import json

def get_positions(xml_content):
    tree = ET.ElementTree(ET.fromstring(xml_content))
    positions = {}
    for module in tree.findall(".//Module"):
        number = module.attrib['number']
        position = module.find('Position')
        positions[number] = {
            'X': float(position.attrib['positionX']),
            'Y': float(position.attrib['positionY']),
            'Z': float(position.attrib['positionZ'])
        }
    return positions

def calculate_displacements(xml1, xml2):
    positions1 = get_positions(xml1)
    positions2 = get_positions(xml2)
    
    displacements = {}
    for module_number, position1 in positions1.items():
        position2 = positions2[module_number]
        displacements[module_number] = {
            'X': position1['X'] - position2['X'],
            'Y': position1['Y'] - position2['Y'],
            'Z': position1['Z'] - position2['Z']
        }
    
    return displacements

def main():
    with open('/Users/pinkdreud/muetrackreco/Structure/XML_ALIGNED.xml', 'r') as file1, open('/Users/pinkdreud/muetrackreco/Structure/MUonEStructure_TB2022_reissued.xml', 'r') as file2:
        xml1 = file1.read()
        xml2 = file2.read()

    displacements = calculate_displacements(xml1, xml2)
    
    with open('displacements.json', 'w') as file:
        json.dump(displacements, file, indent=4)

if __name__ == "__main__":
    main()
