#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "Aligner.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>
#include <chrono>
#include <cmath>
#include <vector>
#include <algorithm>
#include <random>
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

double calculateSumOfSqrtDistances(const std::vector<ModuleClass>& modules, const std::vector<Track>& tracks) {
    double sumOfSqrtDistances = 0.0;

    // Check if the sizes of the module and track vectors are the same
    if (modules.size() != tracks.size()) {
        // Handle this error as needed
        return -1.0;
    }

    for (size_t i = 0; i < modules.size(); ++i) {
        // Assuming each module has only one stub and getLineFromStub returns a Line object
        Line moduleLine = modules[i].getLineFromStub(0);

        // Calculate distance with each line in the corresponding track
        for (const Line& trackLine : tracks[i].lines) {
            double distance = moduleLine.distance(trackLine);
            sumOfSqrtDistances += sqrt(distance);
        }
    }

    return sumOfSqrtDistances;
}

double moveModuleAndCalculateSumOfSquareDistances(std::vector<ModuleClass>& modules, const std::vector<Track>& tracks, const double moveParams_[4]) {
    // Move the module
    // Assuming moveParams contains [deltaX, deltaY, deltaZ, rotationAngle]
	//std::cout << "moveParams_: ";
	//for (int i = 0; i < 4; ++i) {
	//	std::cout << moveParams_[i] << " ";
	//}
	for(auto& m: modules){
		//std::cout << "Module position before: " << m.pos[0] << " " << m.pos[1] << " " << m.pos[2] << " " << m.rot[2]<< std::endl;
		m.pos[0] = moveParams_[0];
		m.pos[1] = moveParams_[1];
		m.pos[2] = moveParams_[2];
		m.rot[2] = moveParams_[3];
		//std::cout << "Module position after: " << m.pos[0] << " " << m.pos[1] << " " << m.pos[2] <<" " << m.rot[2]<< std::endl;
		//getchar();
	}
    return calculateSumOfSqrtDistances(modules, tracks);
}


double moveModuleAndCalculateSumOfSquareDistances(std::vector<ModuleClass>& modules, const std::vector<Track>& tracks) {
    double sumOfSqrtDistances = 0.0;

    // Check if the sizes of the module and track vectors are the same
    if (modules.size() != tracks.size()) {
        // Handle this error as needed
        return -1.0;
    }

    for (size_t i = 0; i < modules.size(); ++i) {
        // Assuming each module has only one stub and getLineFromStub returns a Line object
        Line moduleLine = modules[i].getLineFromStub(0);

        // Calculate distance with each line in the corresponding track
        for (const Line& trackLine : tracks[i].lines) {
            double distance = moduleLine.distance(trackLine);
            sumOfSqrtDistances += sqrt(distance);
        }
    }

    return sumOfSqrtDistances;
}


class MyFunctor {
private:
    std::vector<ModuleClass>& modules;
    const std::vector<Track>& tracks;

public:
    MyFunctor(std::vector<ModuleClass>& m, const std::vector<Track>& t) : modules(m), tracks(t) {}

    double operator() (const double* x) {
        return moveModuleAndCalculateSumOfSquareDistances(modules, tracks, x);
    }
};


void shuffleVector(std::vector<int>& vec) {
	// Obtain a random seed from the system clock
	std::random_device rd;
	// Seed the random number generator
	std::mt19937 gen(rd());
	// Shuffle the vector using the random number generator
	std::shuffle(vec.begin(), vec.end(), gen);
}

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {
	std::cout << std::endl;
	std::cout << "Starting testAligner" << std::endl;
	//gROOT->SetBatch(1);

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/4775332_4785332.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "/muonedqm/muetrackreco/results/dgm_updated/test_";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "/muonedqm/muetrackreco/testAlignment_2.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	std::vector<int> nums {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	std::vector<int> modX {0, 4, 6, 10};
	std::vector<int> modY {1, 5, 7, 11};

	double modulesRes[12] = {
		20,
		20,
		20, 
		20,
		20, 
		20,
		20,
		20,
		20,
		20,
		20,
		20
	};
	tracker->setModulesResolutions(modulesRes);
	auto aligner = std::make_unique<Aligner>();
	aligner->linkTracker(tracker);
	aligner->linkTTreeReader(treeReader);
	aligner->linkStation(&experimentSetup);
	aligner->initHistograms();
	std::vector<int> modulesToAvoid;
	

	//std::cout << "Tracks without DUT: " << tks.size() << std::endl;
	//std::cout << "Sum of sqrt distances: " << calculateSumOfSqrtDistances(mod, tks) << std::endl;
	//double moveParams[4]{0, 0, 0, 0};
	//double sumOfSqrtDistances = moveModuleAndCalculateSumOfSquareDistances(mod, tks, moveParams);
	//std::cout << "Sum of sqrt distances after moving module: " << sumOfSqrtDistances << std::endl;
	//double newParams[4]{100, 0, 0, 0};
	//sumOfSqrtDistances = moveModuleAndCalculateSumOfSquareDistances(mod, tks, newParams);
	//std::cout << "Sum of sqrt distances after moving module: " << sumOfSqrtDistances << std::endl;

	aligner->setNTrackMax(10000);
	std::cout << "Am I even here?" << std::endl;
	for(int ntries = 0; ntries<20; ntries++){
		std::cout << "Try number: " << ntries << std::endl;
		for(int moduleNumber = 0; moduleNumber<12; moduleNumber++){
			std::cout << "Module number: " << moduleNumber << std::endl;
			
			// align translation
			aligner->getNTracksWithoutDUT(moduleNumber);
			auto mod = aligner->modules;
			auto tks = aligner->tracks;
			if(mod.size() == tks.size()) {
				std::cout << "Same number of tracks and modules" << std::endl;
			}
			MyFunctor functor(mod, tks);

			// Create Minimizer
			ROOT::Math::Minimizer* minimizer = ROOT::Math::Factory::CreateMinimizer("Minuit2", "");

			// Set Minimizer properties
			minimizer->SetMaxFunctionCalls(100000000); // for example
			minimizer->SetMaxIterations(1000000000);  // for GSL 
			minimizer->SetTolerance(1E-4);
			minimizer->SetPrintLevel(1);
			


			// Create Functor Wrapper for Minimizer
			ROOT::Math::Functor f(functor, 4); // 4 is the number of parameters in moveParams
			minimizer->SetFunction(f);

			// Set Initial Parameters
			
			double initialParams[4] = {
				aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[0],
				aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[1],
				aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[2],
				aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].rot[2]
			}; // Initial guess

			std::cout << "Initial parameters: ";
			for (int i = 0; i < 4; ++i) {
				std::cout << initialParams[i] << " ";
			}
			std::cout << std::endl;
			
			for (int i = 0; i < 4; ++i) {
				minimizer->SetVariable(i, "param" + std::to_string(i), initialParams[i], 1E-4); // 0.1 is step size
				if(i<3){
					minimizer->SetVariableLimits(i, initialParams[i] - 0.1, initialParams[i] + 0.1);
					std::cout << "Param: " << i << " " << initialParams[i] - 0.1 << " " << initialParams[i] + 0.1 << std::endl;
				}
				else if (i==3) {
					minimizer->SetVariableLimits(i, initialParams[i] - 0.01, initialParams[i] +0.01);
				}
			}
						
						
			if (std::find(modX.begin(), modX.end(), moduleNumber) != modX.end()) {
				minimizer->FixVariable(1); // Fix parameter 1
			} else if (std::find(modY.begin(), modY.end(), moduleNumber) != modY.end()) {
				minimizer->FixVariable(0); // Fix parameter 1
			}


			// Perform Minimization
			minimizer->Minimize();

			const double *xs = minimizer->X();
			aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[0] = xs[0];
			aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[1] = xs[1];
			aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].pos[2] = xs[2];
			aligner->experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].rot[2] = xs[3];
			std::cout << "Best move parameters: " << std::endl;

			for (int i = 0; i < 4; ++i) {
				std::cout << xs[i] << " init: " << initialParams[i] << std::endl;
				if(fabs(xs[i]- initialParams[i] ) > 1){
					std::cout << "Module " << moduleNumber << " is bad" << std::endl;
					throw std::runtime_error("Module is bad");
				}
			}
			std::cout << std::endl;
		}
		aligner->setNTrackMax(5000);
		aligner->clearHistograms();
		for(auto & dut: nums){
			aligner->fillHistograms(dut);
		
		}
		std::cout << "Saving in: " << "provaNuovoAligner" + std::to_string(ntries) + ".root" << std::endl;
		aligner->saveHistograms("provaNuovoAligner" + std::to_string(ntries) + ".root");
		experimentSetup.print();
		experimentSetup.writeStationClassToXml("provaNuovoAligner" + std::to_string(ntries) + ".xml");

	}
    return 0;
}
