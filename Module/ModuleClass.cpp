#include "ModuleClass.hpp"

ModuleClass::ModuleClass(bool considerBend_, double bendOffset_, double sensorSpacing_) {
	pos[0] = 0;
	pos[1] = 0;
	pos[2] = 0;
	rot[0] = 0;
	rot[1] = 0;
	rot[2] = 0;
	bendOffset = bendOffset_;
	sensorSpacing = sensorSpacing_;
	ptWidth = 0;
	module_number = 0;
	link = 0;
	considerBend = considerBend_;
	plane = Plane(Point(0, 0, 0), Point(40000, 0, 0), Point(0, 40000, 0));
}

void ModuleClass::applyBend(bool considerBend_) { considerBend = considerBend_; }

void ModuleClass::setBendOffset(double bendOffset_) { bendOffset = bendOffset_; }

void ModuleClass::setSensorSpacing(double spacing) { sensorSpacing = spacing; }

void ModuleClass::setRotation(double rotX, double rotY, double rotZ) {
	rot[0] = rotX;
	rot[1] = rotY;
	rot[2] = rotZ;
	plane.rotate(rotX, rotY, rotZ);
}

void ModuleClass::setPosition(double posX, double posY, double posZ) {
	pos[0] = posX;
	pos[1] = posY;
	pos[2] = posZ;
	plane.translateX(posX * 1e4);
	plane.translateY(posY * 1e4);
	plane.translateZ(posZ * 1e4);
}

void ModuleClass::setLength(double x, double y) {
	lengthX = x;
	lengthY = y;
}

int ModuleClass::getNStubs() { return stubs.size(); }

void ModuleClass::clearStubs() { stubs.clear(); }

void ModuleClass::pushStub(Stub s) { stubs.push_back(s); }

Stub ModuleClass::getStub(int i) { return stubs.at(i); }

Line ModuleClass::getLineFromStub(Stub s) const {
	double min = 50000;
	if (s.localY > 0.5)
		min *= -1; // CIC1 -> LHS
	Point p1, p2;
	p1 = Point(0, (s.localX - NSTRIPS/2) * 90, 0);
	p2 = Point(min, (s.localX - NSTRIPS/2) * 90, 0);
	Line l(p1, p2);
	l.rotate(rot[0], rot[1], rot[2]);
	l.translateX(pos[0] * 1e4);
	l.translateY(pos[1] * 1e4);
	l.translateZ(pos[2] * 1e4);
	return l;
}

Line ModuleClass::getLineFromStub(int i) const{
	double min = 50000;
	if (stubs[i].localY > 0.5)
		min *= -1; // CIC1 -> LHS
	Point p1, p2;
	p1 = Point(0, (stubs[i].localX - NSTRIPS/2) * 90, 0);
	p2 = Point(min, (stubs[i].localX - NSTRIPS/2) * 90, 0);
	Line l(p1, p2);
	l.rotate(rot[0], rot[1], rot[2]);

	l.translateX(pos[0] * 1e4);
	l.translateY(pos[1] * 1e4);
	l.translateZ(pos[2] * 1e4);
	return l;
}

std::vector<Line> ModuleClass::getMultipleLinesFromStub(int i) {
	// std::cout << "LocalX: " << stubs.at(i).localX << std::endl;
	std::vector<Line> lines;
	for (int i = -30; i != 30 * 2; i += 30) {
		Stub tmpStub = stubs[i];
		tmpStub.localX = tmpStub.localX + i;
		lines.push_back(getLineFromStub(tmpStub));
	}
	return lines;
}

void ModuleClass::print() {
	std::cout << "Module " << module_number << " (" << name << ") at link " << link << std::endl;
	std::cout << "Position: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
	std::cout << "Rotation: " << rot[0] << " " << rot[1] << " " << rot[2] << std::endl;
	std::cout << "Number of stubs: " << stubs.size() << std::endl;
}

void ModuleClass::printStub() {
	std::cout << "Module " << module_number << " (" << name << ") at link " << link << std::endl;
	std::cout << "Number of stubs: " << stubs.size() << std::endl;
	for (auto &s : stubs) {
		std::cout << s.bend << " " << s.link << " " << s.localX << " " << s.localY << " " << s.superID << " " << s.bx
				  << "\n";
	}
}

void ModuleClass::clear() { stubs.clear(); }

Point ModuleClass::intersect(Line l) { return plane.intersect(l); }

double ModuleClass::distanceAlongModule(Point p) {
	p.translateZ(-pos[2] * 1e4);
	p.translateY(-pos[1] * 1e4);
	p.translateX(-pos[0] * 1e4);
	p.rotate(-rot[0], -rot[1], -rot[2]);
	return ((stubs[0].localX - NSTRIPS/2) * 90 - p.x);
}

bool ModuleClass::removeStub(Stub s) {
	for (auto it = stubs.begin(); it != stubs.end(); ++it) {
		if (it->superID == s.superID && it->bx == s.bx && it->bend == s.bend && it->localX == s.localX &&
			it->localY == s.localY && it->meanModuleZ == s.meanModuleZ && it->link == s.link) {
			stubs.erase(it);
			return true;
		}
	}
	return false;
}


void ModuleClass::setResolution(double resX, double resY) {
	resolution[0] = resX;
	resolution[1] = resY;
}

void ModuleClass::setMeanResidualPosition(double meanResidualPositionX_, double meanResidualPositionY_) {
	meanResidualPosition[0] = meanResidualPositionX_;
	meanResidualPosition[1] = meanResidualPositionY_;
}

float ModuleClass::getMeanResidualPosition(int axis) {
	return meanResidualPosition[axis];
}

float ModuleClass::getResolution(int axis) {
	return resolution[axis];
}

int ModuleClass::getLocalXFromLine(Line l) {
	auto p = intersect(l);
	//p.print();
	p.translateX(-pos[0] * 1e4);
	p.translateY(-pos[1] * 1e4);
	p.translateZ(-pos[2] * 1e4);
	p.rotate(-rot[0], -rot[1], -rot[2]);
	//p.print();
	double returnValue = (p.x / 90 + NSTRIPS/2);
	if (fabs(returnValue) > 1064)
		throw std::runtime_error("bad localX" + std::to_string(returnValue));
	return p.x / 90 + NSTRIPS/2;
}