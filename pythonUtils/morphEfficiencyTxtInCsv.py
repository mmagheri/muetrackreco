import csv
import sys

#input_file = sys.argv[1]
#output_file = sys.argv[2]

# Input and output file paths
input_file = '/muonedqm/muetrackreco/results/dgm_updated/run_7/efficiency.txt'  # Replace with your input file path
output_file = '/muonedqm/muetrackreco/results/dgm_updated/run_7//EffVsModuleNumber_run7.csv'  # Replace with your desired output CSV file path

# Get input and output file paths from command line arguments

# Read the data from the input file
with open(input_file, 'r') as file:
    lines = file.readlines()

# Process and write data to CSV
with open(output_file, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    
    # Write the header
    writer.writerow(['Module Number', 'Efficiency', 'Binomial Error'])
    
    # Write each data row
    for line in lines:
        parts = line.split()
        module_number = parts[2]
        efficiency = round(float(parts[4]), 5)  # Round to three decimals
        binomial_error = round(float(parts[7]), 5)  # Round to three decimals
        writer.writerow([module_number, efficiency, binomial_error])

print(f"Data has been written to {output_file}")
