#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "Aligner.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>
#include <chrono>

#include <vector>
#include <algorithm>
#include <random>




void shuffleVector(std::vector<int>& vec) {
	// Obtain a random seed from the system clock
	std::random_device rd;
	// Seed the random number generator
	std::mt19937 gen(rd());
	// Shuffle the vector using the random number generator
	std::shuffle(vec.begin(), vec.end(), gen);
}

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {
	std::cout << std::endl;
	std::cout << "Starting testAligner" << std::endl;
	//gROOT->SetBatch(1);

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/4775332_4785332.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "/muonedqm/muetrackreco/results/dgm_updated/run_2/test_";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "/muonedqm/muetrackreco/Structure/lun6Nov_rotTrasl.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	double modulesRes[12] = {
		20,
		20,
		20, 
		20,
		20, 
		20,
		20,
		20,
		20,
		20,
		20,
		20
	};
	tracker->setModulesResolutions(modulesRes);
	auto aligner = std::make_unique<Aligner>();
	aligner->linkTracker(tracker);
	aligner->linkTTreeReader(treeReader);
	aligner->linkStation(&experimentSetup);
	aligner->initHistograms();
	std::vector<int> modulesToAvoid;
	int nIterations = 0;
	std::vector<int> nums {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	//std::vector<int> nums {2, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	std::vector<int> modX {0, 2, 3, 4, 6, 8, 9, 10};
	std::vector<int> modY {1, 2, 3, 5, 7, 8, 9, 11};

	std::vector<int> nModulesXgood {};
	std::vector<int> nModulesYgood {};

	aligner->setNTrackMax(1000000);
	// align translation
	while(nIterations < 5) {
		break;
		std::cout << "Modules okay: " << modulesToAvoid.size() << std::endl;
		std::cout << "Iteration: " << nIterations << std::endl;
		shuffleVector(nums);
		std::cout << "nums vector: ";
		for(int i = 0; i < nums.size(); i++) {
			std::cout << nums[i] << " ";
		}
		std::cout << std::endl;
		//getchar();
		for(auto & dut: nums){
			auto start = std::chrono::high_resolution_clock::now(); // end measuring time
			if(std::find(modulesToAvoid.begin(), modulesToAvoid.end(), dut) != modulesToAvoid.end()) {
				continue;
			}
			//aligner->fillHistograms_randomEntries(dut);
			
			//aligner->fillHistograms(dut);
			double meanPosX = 0;
			double meanPosY = 0;
			if (std::find(modX.begin(), modX.end(), dut) != modX.end()) {
				std::cout << "Aligning X on module: " << dut << std::endl;
				meanPosX = aligner->scanModuleTranslation(dut, 0);
				aligner->scanModuleTranslation(dut, 2);
				std::cout << "Mean position on axis: " << meanPosX << std::endl;
				if( fabs(meanPosX) < 1) {
					nModulesXgood.push_back(dut);
				}
				//std::cout << "Aligning module rotation axis X: " << std::endl;
				//aligner->scanModuleRotationOnX(dut, 0);
				//std::cout << "Aligning module rotation axis Y: " << std::endl;
				aligner->scanModuleRotationOnX(dut, 1);
				//std::cout << "Aligning module rotation axis Z: " << std::endl;
				aligner->scanModuleRotationOnX(dut, 2);
				// rest of the code
			}
			if (std::find(modY.begin(), modY.end(), dut) != modY.end()) {
				std::cout << "Aligning Y on module: " << dut << std::endl;
				meanPosY = aligner->scanModuleTranslation(dut, 1);
				aligner->scanModuleTranslation(dut, 2);
				if( fabs(meanPosY) < 1) {
					nModulesYgood.push_back(dut);
				}
				//std::cout << "Aligning module rotation axis X: " << std::endl;
				aligner->scanModuleRotationOnY(dut, 0);
				//std::cout << "Aligning module rotation axis Y: " << std::endl;
				//aligner->scanModuleRotationOnY(dut, 1);
				//std::cout << "Aligning module rotation axis Z: " << std::endl;
				aligner->scanModuleRotationOnY(dut, 2);
			}

			std::cout << "meanPosX: " << meanPosX << std::endl;
			std::cout << "meanPosY: " << meanPosY << std::endl;
			//if(fabs(meanPosX) <= 1 || fabs(meanPosY) <= 1) {
			//	modulesToAvoid.push_back(dut);
			//}
			auto end = std::chrono::high_resolution_clock::now(); // end measuring time
			auto duration = std::chrono::duration_cast<std::chrono::seconds>(end - start); // calculate duration
			std::cout << "Time taken for iteration " << nIterations << ": " << duration.count() << "s" << std::endl;
		}
		nIterations++;
		for(auto & dut: nums){
			aligner->fillHistograms(dut);
		}
		aligner->saveHistograms(outputFileName + "_translation_" + std::to_string(nIterations) + ".root");	
		aligner->clearHistograms();
		experimentSetup.writeStationClassToXml(outputFileName + "_" + std::to_string(nIterations) + ".xml");
	}
	aligner->clearHistograms();
	for(auto & dut: nums){
		aligner->fillHistograms(dut);
	}
	aligner->saveHistograms(outputFileName + ".root");
	experimentSetup.writeStationClassToXml(outputFileName + ".xml");

}
