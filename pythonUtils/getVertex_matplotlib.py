import ROOT
import matplotlib.pyplot as plt
import numpy as np

# Open the root file
file = ROOT.TFile.Open("test/vertexing/vertexSumUp.root")

# Get the histograms
vertexPosition = file.Get("vertexPosition")
vertexTrackS1Distance = file.Get("vertexTrackS1Distance")
angleBetweenTracks = file.Get("angleBetweenTracks")

outputFolder = "test/vertexing/plot/"
# Assuming vertex_trackS1_distance and angleBetweenTracks are similar to vertexPosition
# vertex_trackS1_distance = file.Get("vertexTrackS1Distance")
# angleBetweenTracks = file.Get("angleBetweenTracks")

# Function to plot a ROOT histogram using Matplotlib
def plot_root_histogram(root_hist, title, x_label, y_label, color_map):
    bins = root_hist.GetXaxis().GetNbins()
    x_edges_um = np.array([root_hist.GetXaxis().GetBinLowEdge(i) for i in range(1, bins + 2)])
    
    # Convert um to cm
    if(root_hist  == vertexPosition):
        x_edges_um = x_edges_um / 10000
    
    y_values = np.array([root_hist.GetBinContent(i) for i in range(1, bins + 1)])
    if(root_hist != angleBetweenTracks):
        # Normalize the values to 1
        y_values = y_values / np.sum(y_values)

    # Use a colormap for the bar colors
    cmap = plt.get_cmap(color_map)
    colors = cmap(np.linspace(0, 1, bins))

    plt.bar(x_edges_um[:-1], y_values, width=np.diff(x_edges_um), align='edge', color=colors)#, edgecolor='black')
    plt.title(title)
    plt.xlabel(x_label)
    if(root_hist == vertexTrackS1Distance):
        plt.xlim(0, 2000)
    plt.ylabel(y_label)
    plt.savefig(outputFolder + title + ".png")

def plot_root_th2(root_hist, title, x_label, y_label, color_map):
    x_values = []
    y_values = []
    weights = []  # Represents the bin content

    for i in range(1, root_hist.GetNbinsX() + 1):
        for j in range(1, root_hist.GetNbinsY() + 1):
            bin_content = root_hist.GetBinContent(i, j)
            if bin_content > 0:  # Assuming you only care about non-empty bins
                x_values.append(root_hist.GetXaxis().GetBinCenter(i))
                y_values.append(root_hist.GetYaxis().GetBinCenter(j))
                weights.append(bin_content)

    # Plot using Matplotlib
    plt.scatter(x_values, y_values, c=weights, cmap=color_map)
    plt.colorbar()  # To show the weight scale
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(outputFolder + title + ".png")
# Plot the vertexPosition histogram
plot_root_histogram(vertexPosition, "Position of the reconstructed vertex on the beam axis", "Z vertex (cm)", "fraction of the analyzed entries", "viridis")
plot_root_histogram(vertexTrackS1Distance, "Vertex - incoming track distance", "distance (um)", "fraction of the analyzed entries", "viridis")
plot_root_th2(angleBetweenTracks, "Angle between the two reconstructed tracks and the incoming one", "angle(tki,tk1)", "angle(tki,tk2)", "viridis")

# For the other histograms, call plot_root_histogram with appropriate arguments
# For example:
# plot_root_histogram(vertex_trackS1_distance, "Title", "X-Axis Label", "Y-Axis Label", "magma")
