#pragma once
#include "Plane.hpp"
#include "Stub.hpp"
#include "Track.hpp"
#include <array>
#include <cmath>
#include <map>
#include <vector>
// #include "Converter.hpp"

// const int NSTRIPS = 1016;
// const double STRIP_PITCH = 0.009;                       //cm (90um)
// const double FULL_DIMENSIONS_X = NSTRIPS*STRIP_PITCH;   //cm. full dimensions of a 2S module along the measured
// coordinate (9.144cm) const double FULL_DIMENSIONS_Y = 10;                    //cm. full dimensions of a 2S module
// along strip axis (2 sets of strips, each set is 5cm long)
//
class ModuleClass {
  private:
	float ptWidth;
	std::map<int, std::map<uint16_t, uint16_t>> LUTmap;
	double lengthX = 10; // cm
	double lengthY = 10; // cm
	const int NSTRIPS = 1016;
	bool considerBend = false;
	float resolution[2] {90, 90}; // um
	float meanResidualPosition[2] {0, 0}; // um

public:
	float bendOffset;
	double sensorSpacing = 1800; // um
	Plane plane;
	std::vector<Stub> stubs;
	int module_number;
	int link;
	std::string name;
	double pos[3], rot[3];

	ModuleClass(bool considerBend_ = false, double bendOffset_ = 0., double sensorSpacing_ = 1800.);
	void applyBend(bool considerBend_ = true);
	void setBendOffset(double bendOffset_);
	void setSensorSpacing(double spacing);
	void setRotation(double rotX, double rotY, double rotZ);
	void setPosition(double posX, double posY, double posZ);
	void setLength(double x, double y);
	void setResolution(double resX, double resY);
	void setMeanResidualPosition(double meanResidualPositionX_, double meanResidualPositionY_);
	float getResolution(int axis);
	float getMeanResidualPosition(int axis);
	int getNStubs();
	void clearStubs();
	void pushStub(Stub s);
	Stub getStub(int i);
	Line getLineFromStub(int i) const;
	Line getLineFromStub(Stub s) const;
	std::vector<Line> getMultipleLinesFromStub(int i);
	void print();
	void printStub();
	void clear();
	Point intersect(Line l);
	double distanceAlongModule(Point p);
	bool removeStub(Stub s);
	int getLocalXFromLine(Line l);
};