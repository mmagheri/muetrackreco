#include "TTreeReader.hpp"
#include "Converter.hpp"
#include "xmlUtils.hpp"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "parser.hpp"
#include "Tracker.hpp"

//const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

    //bool allModules = cmdOptionExists(argv, argv+argc, "-a");
    bool considerBend = cmdOptionExists(argv, argv+argc, "-b");
    std::string inputFileName = "../3090_014e0f3e0_014e133df_4201232.root";
    if(cmdOptionExists(argv, argv+argc, "-i")){
        inputFileName = getCmdOption(argv, argv + argc, "-i");
    }
    std::string outputFileName = "../prova";
    if(cmdOptionExists(argv, argv+argc, "-o")){
        outputFileName = getCmdOption(argv, argv + argc, "-o");
    }
    std::string xmlFileName = "../Structure/MUonEStructure_TB2022_reissued.xml";
    if(cmdOptionExists(argv, argv+argc, "-x")){
        xmlFileName = getCmdOption(argv, argv + argc, "-x");
    }

    bool dumpToJson = cmdOptionExists(argv, argv+argc, "-j");
    TTreeReader t;
    t.parseTree(inputFileName.c_str());
    ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), considerBend);
    t.setStation(&experimentSetup);
    experimentSetup.print();
    //TFile* newFile = new TFile((outputFileName + ".root").c_str(), "RECREATE");

    nlohmann::json TracksJson;
    Tracker tracker(experimentSetup);
    
    int nTracksMax = 10000;
    if(dumpToJson) nTracksMax = 1000;
    int nTracks = 0;
    for(int i=0; i< t.getEntries() && nTracks < nTracksMax; i++){
        if(i%1000==0) std::cout << "Event " << i << std::endl;
        
        t.loadEventNumber(i);
        if(!t.hasNStubsPerModule(2)) {
            continue;
        }
        nlohmann::json newEvent;
        if((t.experimentalSetup->stationMap[0]).modules.at(0).stubs.size() == 0) continue;
        newEvent["Bx"] = (t.experimentalSetup->stationMap[0]).modules.at(0).stubs.at(0).bx;
        newEvent["filename"] = inputFileName;
        newEvent["index"] = i;
        newEvent["superID"] = (t.experimentalSetup->stationMap[0]).modules.at(0).stubs.at(0).superID;

        auto tracks = tracker.buildAllTracksFromCurrentLoadedEvent(0);
        newEvent["multiplicity"] = tracks.size();
        if (tracks.size() < 2) continue;
        for(auto& tk: tracker.buildAllTracksFromCurrentLoadedEvent(0)) {
            //TracksJson.push_back(tk.dumpToJson());
            newEvent["tracks"].push_back(tk.dumpToJson());
        }
        TracksJson["events"].push_back(newEvent);            
        nTracks += newEvent["tracks"].size();
        //newEvent["multiplicity"] = 0;
    }

    std::cout << "Saving---" << nTracks << "\n";

    std::ofstream outputFileTracks(outputFileName);
    outputFileTracks << std::setw(4) << TracksJson << std::endl;
    outputFileTracks.close();
	return 0;
	// newFile->Write();
}