#pragma once
#include "ExperimentalSetup.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include <vector>

class Tracker {
  private:
	TTreeReader *t;
	std::string newFileName = "alignment.root";
	nlohmann::json StubsJson;
	nlohmann::json TrackJson;

	int nTracksMax = 50000;
	bool allModules = false;
	bool oneStubPerModule = false;
	std::vector<TH1F *> distanceFromInterceptedPointX;
	std::vector<TH1F *> distanceFromInterceptedPointY;
	std::vector<TH1F *> distanceFromInterceptedPointZ;
	std::vector<TH1F *> lineDistance;
	std::vector<TH1F *> sigmaTrackX;
	std::vector<TH1F *> sigmaTrackY;

	std::vector<TH2F *> hdYVsBend;
	std::vector<TH2F *> hdYVSCIC;
	std::vector<TH2F *> hdYVSLocalX;
	std::vector<TH2F *> hdYVSbx;
	int nStubsPerModule[6]{0, 0, 0, 0, 0, 0};

	std::vector<Track> allTracks;
	bool saveAlignmentHist = false;

  public:
	Track currentTrack;
	std::vector<Track> allTracksGood;
	ExperimentalSetup &experimentSetup;

	// Tracker(const std::string& inputFileName, bool considerBend, const std::string& outputFileName,
	// ExperimentalSetup& experimentSetup_);
	Tracker(ExperimentalSetup &experimentSetup_) : experimentSetup(experimentSetup_){};
    ~Tracker();
	void setTreeReader(TTreeReader *t_);
	void setNTracksMax(int newMax);
	void fillHistograms();
	void fillHistograms_multiBx(int nBx);
	void useAllModules();
	void moveModule(int mod, int axis, double distance);
	void clearHistograms();
	void saveHistograms();
	void initHistograms();
	double getEfficiency(int nModule);
	void getNStubsPerModule(int eventNumber = 1000000);
	double alignModule(int mod, int axis);
	double alignModule_multiBx(int mod, int axis, int nBx);
	bool buildTrackFromEvent(int eventNumber, int moduleToRemove = -999, int nStation = 0);
	bool buildTrackFromCurrentEvent(int moduleToRemove = -999, int nStation = 0);
	void buildAllTracks(int eventNumber, int stationId);
	std::vector<Track> buildAllTracksFromCurrentLoadedEvent(int stationId);
	void writeJson();
	void saveStructureToXml(std::string xmlName = "newStructure.xml");
	void addStubsAndBuildTrack(const std::vector<Stub> &newStubs);
	void setSaveAlignmentHist(bool val);
	void clear();
};
