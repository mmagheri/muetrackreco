import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1405206_1415206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1415206_1425206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1425206_1435206.root",
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("/muonedqm/muetrackreco/test/residuals/run_7/" + filename.replace(".root", "_alignment"))

print(output_files)
xmlFiles =[
"/muonedqm/muetrackreco/test/run_7/1405206_1415206_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_7/1415206_1425206_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_5.xml",
]
#xmlFile = "/muonedqm/muetrackreco/test/movingAllModules/_5.xml"

command = "./build/apps/testAligner"

def process_file(rootfile, output_file, xmlFile):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
    for rootfile, output_file, xmlFile in zip(rootfiles, output_files, xmlFiles):
        executor.submit(process_file, rootfile, output_file, xmlFile)

