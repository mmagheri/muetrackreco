#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "Tracker.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <algorithm>
#include <filesystem>

const int MAXIMUM_MODULE_MOVEMENT_IN_ALIGNMENT = 60; // IN UM
// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

	bool allModules = cmdOptionExists(argv, argv + argc, "-a");
	bool considerBend = cmdOptionExists(argv, argv + argc, "-b");
	std::string inputFileName = "../3090_014e0f3e0_014e133df_4201232.root";
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}

	std::filesystem::path filePath(inputFileName);
	std::string baseName = filePath.stem().string();
	std::string outputFileName = baseName + "_aligned.xml";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}
	std::string xmlFileName = "../Structure/MUonEStructure_TB2022_RH_RF_pnew.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	auto expsetup = getStationClassFromXml(xmlFileName.c_str());

	TTreeReader *t = new TTreeReader();
	t->parseTree(inputFileName.c_str());
	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), considerBend);
	t->setStation(&experimentSetup);
	experimentSetup.print();

	Tracker tracker(experimentSetup);
	tracker.setTreeReader(t);
	tracker.setNTracksMax(5000);
	tracker.initHistograms();
	if (allModules)
		tracker.useAllModules();

	int nModules = experimentSetup.getNModules();
	std::vector<double> lastMeanModulePosition(nModules * 2, -999);
	std::vector<bool> samePositionAsLastIteration(nModules * 2, false);

	for (int i = 0; i < 10; i++) {
		std::cout << "ITERATION: " << i << "\n";

		std::vector<double> currentMeanModulePosition(nModules * 2, 999);

		for (int j = 0; j < nModules * 2; j++) {
			std::cout << "Aligning module: " << j / 2 << " " << j % 2 << std::endl;
			currentMeanModulePosition[j] = tracker.alignModule_multiBx(j / 2, j % 2, 2);
			std::cout << currentMeanModulePosition[j] << std::endl;
		}

		for (int j = 0; j < nModules * 2; j++) {
			std::cout << "DeltaPositions: " << fabs(currentMeanModulePosition[j] - lastMeanModulePosition[j])
					  << std::endl;
			if (fabs(currentMeanModulePosition[j] - lastMeanModulePosition[j]) < MAXIMUM_MODULE_MOVEMENT_IN_ALIGNMENT)
				samePositionAsLastIteration[j] = true;
		}
		if (std::all_of(samePositionAsLastIteration.begin(), samePositionAsLastIteration.end(),
						[](bool v) { return v; })) {
			// All elements are true
			break;
		}

		for (int j = 0; j < nModules * 2; j++) {
			samePositionAsLastIteration[j] = false;
			lastMeanModulePosition[j] = currentMeanModulePosition[j];
		}
		std::cout << "Setup at iteration: " << i << "\n";
		//tracker.experimentSetup.print();
	}
	tracker.experimentSetup.print();
	tracker.experimentSetup.writeStationClassToXml(outputFileName);
	return 0;
}