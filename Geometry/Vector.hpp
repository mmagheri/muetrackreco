#pragma once
#include "Point.hpp"


class Vector {
public:
	double x, y, z;
	//default constructor
	Vector() : x(0), y(0), z(0) {}
	Vector(double x, double y, double z) : x(x), y(y), z(z) {}

	Vector(Point p1) : x(p1.x), y(p1.y), z(p1.z) {}
	Vector(Point p1, Point p2) : x(p2.x - p1.x), y(p2.y - p1.y), z(p2.z - p1.z) {}

	Vector operator+(Vector v);
	Vector operator-(Vector v);

	double operator*(Vector v);
	Vector operator^(Vector v);
	Vector operator*(double s);
	Vector operator/(double s);
	bool operator==(const Vector& v) const;

	Point toPoint();
    double length();
	void normalize();
	void print();

    double angle(Vector v1);

	friend std::ostream& operator<<(std::ostream& os, const Vector& v) {
		os << "x: " << v.x << " y: " << v.y << " z: " << v.z;
		return os;
	}
};
