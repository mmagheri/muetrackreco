import xml.etree.ElementTree as ET
import yaml

# Carica il file XML
tree = ET.parse('/Users/pinkdreud/muetrackreco/pythonUtils/MUonEStructure_TB2022_reissued.xml')
root = tree.getroot()

# Carica il file di correzione YAML
with open('correzioni.yaml', 'r') as file:
    correzioni = yaml.safe_load(file)

# Applica le correzioni al file XML
for index, module in enumerate(root.findall('.//Module')):
    if index < len(correzioni[0]):
        correzione = correzioni[0][index]
        position = module.find('Position')
        # Aggiorna gli attributi di posizione
        position.attrib['positionX'] = str(float(position.attrib['positionX']) + correzione['xOffset'])
        position.attrib['positionY'] = str(float(position.attrib['positionY']) + correzione['yOffset'])
        position.attrib['positionZ'] = str(float(position.attrib['positionZ']) + correzione['zOffset'])

        rotation = module.find('Rotation')

        # Aggiorna gli attributi di rotazione
        rotation.attrib['rotationX'] = str(float(rotation.attrib['rotationX'])) #+ correzione['angleOffset'])
        rotation.attrib['rotationY'] = str(float(rotation.attrib['rotationY'])) #+ correzione['tiltOffset'])
        rotation.attrib['rotationZ'] = str(float(rotation.attrib['rotationZ'])) #+ correzione['angleOffset'])

# Salva il file XML modificato
tree.write('XML_STRUCTURE_modified.xml', encoding='utf-8', xml_declaration=True)
