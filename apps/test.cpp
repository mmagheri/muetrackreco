#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/merged/run_2/5245626_5271354.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "../prova";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "../Structure/MUonEStructure_TB2023_updated.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	TTreeReader t;
	t.parseTree(inputFileName.c_str());
	t.setStation(&experimentSetup);
	experimentSetup.stationMap[1].printNModules();
	
	nlohmann::json StubsJson;
	nlohmann::json TracksJson;
	
	auto tracker = new TrackerFromStubs(experimentSetup);
	for(int entry = 0; entry< t.getEntries(); entry++) {
		t.loadEventNumber(entry);
		//experimentSetup.printStub();
		experimentSetup.printStub();

		
		for (auto& station : experimentSetup.stationMap) {
			for (auto& module : station.second.modules) {
				
				for(int nStub = 0; nStub < module.second.stubs.size(); nStub++) {
					auto stub = module.second.getStub(nStub);
					auto line = module.second.getLineFromStub(nStub);
					// Do something with the line
					tracker->pushStubAndLine(stub, line);
				}
			}
		}
		tracker->printLines();
		tracker->buildTrackFromCurrentLines();
		tracker->getCurrentTrack().dumpToJson(TracksJson);
		for (auto& station : experimentSetup.stationMap) {
			station.second.dumpStubsToJson(StubsJson, entry);
		}

		tracker->clear();
		//tracker->printLines();
		
		break;

	}
	std::ofstream o("prova_stubs.json");
	o << std::setw(4) << StubsJson << std::endl;
	std::ofstream o2("prova_track.json");
	o2 << std::setw(4) << TracksJson << std::endl;
}
