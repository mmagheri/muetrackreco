#include "ExperimentalSetup.hpp"

void ExperimentalSetup::print() {
	for (auto &s : stationMap) {
		s.second.print();
	}
};

void ExperimentalSetup::printStub() {
	for (auto &s : stationMap) {
		s.second.printStub();
	}
}

void ExperimentalSetup::clear() {
	for (auto &m : stationMap) {
		m.second.clear();
	}
}

void ExperimentalSetup::pushStub(Stub s) {
	stationMap[int(s.link / 6)].pushStub(s);
}

void ExperimentalSetup::helper(std::vector<std::vector<Stub>> &input, std::vector<Stub> &current,
							   std::vector<std::vector<Stub>> &result, unsigned int depth) {
	if (depth == input.size()) {
		result.push_back(current);
		return;
	}
	for (auto &stub : input[depth]) {
		current[depth] = stub;
		helper(input, current, result, depth + 1);
	}
}

std::vector<std::vector<Stub>> ExperimentalSetup::cartesianProduct(int stationId) {
	// Get modules in the given station
	auto &station = stationMap[stationId];

	// Prepare input
	std::vector<std::vector<Stub>> input;
	for (auto &module : station.modules) {
		input.push_back(module.second.stubs);
	}

	// Prepare space for the current combination and the result
	std::vector<Stub> current(input.size());
	std::vector<std::vector<Stub>> result;
	// Generate all combinations
	helper(input, current, result, 0);
	return result;
}

void ExperimentalSetup::LineHelper(std::vector<std::vector<Line>> &input, std::vector<Line> &current,
								   std::vector<std::vector<Line>> &result, unsigned int depth) {
	if (depth == input.size()) {
		result.push_back(current);
		return;
	}

	for (auto &stub : input[depth]) {
		current[depth] = stub;
		LineHelper(input, current, result, depth + 1);
	}
}

std::vector<std::vector<Line>> ExperimentalSetup::getAllLineFromStubsCombinations(int stationId) {
	// Get modules in the given station
	auto &station = stationMap[stationId];

	// Prepare input
	std::vector<std::vector<Line>> input;
	for (auto &module : station.modules) {
		std::vector<Line> tmpLines;
		for (auto &stub : module.second.stubs) {
			tmpLines.push_back(module.second.getLineFromStub(stub));
		}
		input.push_back(tmpLines);
	}

	// Prepare space for the current combination and the result
	std::vector<Line> current(input.size());
	std::vector<std::vector<Line>> result;

	// Generate all combinations
	LineHelper(input, current, result, 0);

	return result;
}

void ExperimentalSetup::writeStationClassToXml(const std::string &xmlName) {
	rapidxml::xml_document<> doc;

	// Add declaration
	rapidxml::xml_node<> *decl = doc.allocate_node(rapidxml::node_declaration);
	decl->append_attribute(doc.allocate_attribute("version", "1.0"));
	decl->append_attribute(doc.allocate_attribute("encoding", "UTF-8"));
	doc.append_node(decl);

	// Root node
	rapidxml::xml_node<> *root = doc.allocate_node(rapidxml::node_element, "MUonE_structure");
	doc.append_node(root);

	for (const auto &stationPair : stationMap) {
		const StationClass &stat = stationPair.second;

		// Station node
		rapidxml::xml_node<> *station = doc.allocate_node(rapidxml::node_element, "Station");
		station->append_attribute(
			doc.allocate_attribute("stationId", doc.allocate_string(std::to_string(stat.ID).c_str())));
		station->append_attribute(doc.allocate_attribute("name", "MUonE_station"));
		station->append_attribute(
			doc.allocate_attribute("nModules", doc.allocate_string(std::to_string(stat.nModules).c_str())));

		// Position and Rotation
		rapidxml::xml_node<> *pos = doc.allocate_node(rapidxml::node_element, "Position");
		rapidxml::xml_node<> *rot = doc.allocate_node(rapidxml::node_element, "Rotation");

		pos->append_attribute(
			doc.allocate_attribute("positionX", doc.allocate_string(std::to_string(stat.pos[0]).c_str())));
		pos->append_attribute(
			doc.allocate_attribute("positionY", doc.allocate_string(std::to_string(stat.pos[1]).c_str())));
		pos->append_attribute(
			doc.allocate_attribute("positionZ", doc.allocate_string(std::to_string(stat.pos[2]).c_str())));
		rot->append_attribute(
			doc.allocate_attribute("rotationX", doc.allocate_string(std::to_string(stat.rot[0]).c_str())));
		rot->append_attribute(
			doc.allocate_attribute("rotationY", doc.allocate_string(std::to_string(stat.rot[1]).c_str())));
		rot->append_attribute(
			doc.allocate_attribute("rotationZ", doc.allocate_string(std::to_string(stat.rot[2]).c_str())));

		station->append_node(pos);
		station->append_node(rot);

		// Modules
		for (const auto &modulePair : stat.modules) {
			const ModuleClass &m = modulePair.second;

			rapidxml::xml_node<> *module = doc.allocate_node(rapidxml::node_element, "Module");
			module->append_attribute(
				doc.allocate_attribute("number", doc.allocate_string(std::to_string(m.module_number).c_str())));
			module->append_attribute(doc.allocate_attribute("name", doc.allocate_string(m.name.c_str())));
			module->append_attribute(
				doc.allocate_attribute("linkId", doc.allocate_string(std::to_string(m.link).c_str())));
			module->append_attribute(
				doc.allocate_attribute("sensorSpacing", doc.allocate_string(std::to_string(m.sensorSpacing / 1000).c_str())));
			module->append_attribute(
				doc.allocate_attribute("bendOffset", doc.allocate_string(std::to_string(m.bendOffset).c_str())));
			// module->append_attribute(doc.allocate_attribute("isOn",
			// doc.allocate_string(std::to_string(m.isOn).c_str()))); // Assuming isOn is a bool or integer

			rapidxml::xml_node<> *position = doc.allocate_node(rapidxml::node_element, "Position");
			position->append_attribute(
				doc.allocate_attribute("positionX", doc.allocate_string(std::to_string(m.pos[0] - stat.pos[0]).c_str())));
			position->append_attribute(
				doc.allocate_attribute("positionY", doc.allocate_string(std::to_string(m.pos[1] - stat.pos[1]).c_str())));
			position->append_attribute(
				doc.allocate_attribute("positionZ", doc.allocate_string(std::to_string(m.pos[2] - stat.pos[2]).c_str())));
			module->append_node(position);

			rapidxml::xml_node<> *rotation = doc.allocate_node(rapidxml::node_element, "Rotation");
			rotation->append_attribute(
				doc.allocate_attribute("rotationX", doc.allocate_string(std::to_string(m.rot[0] - stat.rot[0]).c_str())));
			rotation->append_attribute(
				doc.allocate_attribute("rotationY", doc.allocate_string(std::to_string(m.rot[1] - stat.rot[1]).c_str())));
			rotation->append_attribute(
				doc.allocate_attribute("rotationZ", doc.allocate_string(std::to_string(m.rot[2] - - stat.rot[2]).c_str())));
			module->append_node(rotation);

			station->append_node(module);
		}

		root->append_node(station);
	}

	// Write to file
	std::ofstream file_stored(xmlName);
	file_stored << doc;
	file_stored.close();
	doc.clear(); // clear the document when done
}

int ExperimentalSetup::getNModules() {
	int n = 0;
	for (const auto &stationPair : stationMap) {
		n += stationPair.second.nModules;
	}
	return n;
}