from bs4 import BeautifulSoup
import yaml
from yaml.loader import SafeLoader
import math

outfile = open("newCorrected.xml", "w")

with open('aligned.yaml') as f:
    correctedValues = yaml.load(f, Loader=SafeLoader)

# Reading the data inside the xml
# file to a variable under the name
# data
with open('test.xml', 'r') as f:
    data = f.read()
 
# Passing the stored data inside
# the beautifulsoup parser, storing
# the returned object
Bs_data = BeautifulSoup(data, "xml")
 
# Finding all instances of tag
# `unique`
b_module = Bs_data.find_all('Module')

for module in b_module:
    print(module["number"])
    moduleNumber = int(module["number"])
    #get stuff and update
    pos = [ 
        float(module.Position["positionX"]),
        float(module.Position["positionY"]),
        float(module.Position["positionZ"])
    ]
    rot = [ 
        float(module.Rotation["rotationX"]),
        float(module.Rotation["rotationY"]),
        float(module.Rotation["rotationZ"])
    ]
    print(pos)
    print(rot)
    pos[0] += correctedValues[0][moduleNumber]["xOffset"]
    pos[1] += correctedValues[0][moduleNumber]["yOffset"]
    pos[2] += correctedValues[0][moduleNumber]["zOffset"]

    rot[2] += math.radians(correctedValues[0][moduleNumber]["angleOffset"])
    outfile.write("Module number: " + str(moduleNumber) + "\n")
    outfile.write("\t" + str(pos[0]) + " " + str(pos[1]) + " " + str(pos[2]) + "\n")
    outfile.write("\t" + str(rot[0]) + " " + str(rot[1]) + " " + str(rot[2]) + "\n")

#b_unique = Bs_data.find_all('Rotation')
 
#print(b_unique)
# Using find() to extract attributes
# of the first instance of the tag
#b_name = Bs_data.find('Module', {'name':'Frank'})
 
#print(b_name)
 
# Extracting the data stored in a
# specific attribute of the
# `child` tag
#value = b_name.get('test')
 
#print(value)