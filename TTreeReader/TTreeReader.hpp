#pragma once

#include <vector>
#include <string>
#include <stdint.h>
#include "Stub.hpp"
#include "ExperimentalSetup.hpp"
#include <map>

///////////////
// ROOT HEADERS
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>


class TTreeReader
{
protected:
    int currentEvNumber = 0;
    TTree *eventTree;
    const std::string TREENAME = "cbmsim";
	std::vector<unsigned short> *Bx      = 0;
	std::vector<unsigned short> *Link    = 0;
	std::vector<float> *Bend   = 0;
	std::vector<float> *LocalX = 0;
	std::vector<float> *LocalY = 0;
	std::vector<unsigned int>   *SuperID = 0;
public:
    ExperimentalSetup* experimentalSetup;
    void parseTree(std::string filename);
    void setStation(ExperimentalSetup* expSetup) {experimentalSetup = expSetup;};
    TTreeReader() {currentEvNumber = 0;};
    ~TTreeReader();
	void loadEventNumber(int nEv);
	void loadThreeNextBx(int nEv);
	void loadNextNBx(int nEv, int nConsecutiveBxs);
    void loadNextEvent();
	long getEntries();
    bool hasOneStubPerModule();
    bool hasOneEvenStubPerModule();
    bool hasOneOddStubPerModule();
    bool hasAtLeastNStubs(int n);
    bool hasNStubsPerModule(int n);
	int getNStubsInside() { return SuperID->size(); }
    std::map<int, int> getNStubsPerModuleInStation(int station);
};

