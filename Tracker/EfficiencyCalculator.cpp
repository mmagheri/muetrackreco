#include "EfficiencyCalculator.hpp"

EfficiencyCalculator::~EfficiencyCalculator() {
	for (auto &histo : distanceFromInterceptedPointX) {
		delete histo;
	}
	for (auto &histo : distanceFromInterceptedPointY) {
		delete histo;
	}
	for (auto &histo : distanceFromInterceptedPointZ) {
		delete histo;
	}
	for (auto &histo : lineDistance) {
		delete histo;
	}
	for (auto &histo : sigmaTrackX) {
		delete histo;
	}
	for (auto &histo : sigmaTrackY) {
		delete histo;
	}
}

void EfficiencyCalculator::linkTracker(TrackerFromStubs* trackerFromStubs_){
	trackerFromStubs = trackerFromStubs_;
}

void EfficiencyCalculator::linkTTreeReader(TTreeReader* treeReader_){
	treeReader = treeReader_;
}

void EfficiencyCalculator::linkStation(ExperimentalSetup* expSetup){
	experimentalSetup = expSetup;
}

void EfficiencyCalculator::updateResolutionFromFile(std::string fileName) {
	if(!experimentalSetup) {
		throw std::runtime_error("Experimental setup not linked");
	}
	std::cout << "Updating resolution from file: " << fileName << std::endl;
	std::string histoBaseName {"hDistanceFromInterceptedPoint"};
	
	TFile *file = TFile::Open(fileName.c_str());
	if (!file || file->IsZombie()) {
		throw std::runtime_error("EfficiencyCalculator::updateResolutionFromFile - Could not open file: " + fileName);
	}
	for(int i = 0; i < experimentalSetup->getNModules(); i++) {
		//get X resolution
		std::string histoName_X = histoBaseName + "X_" + std::to_string(i);
		std::string histoName_Y = histoBaseName + "Y_" + std::to_string(i);
		TH1F* histoX = (TH1F*)file->Get(histoName_X.c_str());
		TH1F* histoY = (TH1F*)file->Get(histoName_Y.c_str());

		// fit histo with a gaussian and get mean and std deviation
		TF1 *fitX = new TF1("fit", "gaus", histoX->GetMean() - 2*histoX->GetRMS(), histoX->GetMean() + 2*histoX->GetRMS());
		TF1 *fitY = new TF1("fit", "gaus", histoY->GetMean() - 2*histoY->GetRMS(), histoY->GetMean() + 2*histoY->GetRMS());
		histoX->Fit(fitX, "Q");
		histoY->Fit(fitY, "Q");
		double resoX = fabs(fitX->GetParameter(2)) < 2  ? 1000 : fitX->GetParameter(2);
		double resoY = fabs(fitY->GetParameter(2)) < 2  ? 1000 : fitY->GetParameter(2);
		experimentalSetup->stationMap[int(i/6)].modules[i].setResolution(resoX, resoY);
		experimentalSetup->stationMap[int(i/6)].modules[i].setMeanResidualPosition(fitX->GetParameter(1), fitY->GetParameter(1));
		//experimentalSetup->stationMap[int(i/6)].modules[i].setResolution(0, 0);
	}

	file->Close();
	//std::vector<std::vector<double>> resolution = readResolutionFile(fileName);
	//for (int i = 0; i < experimentalSetup->getNModules(); i++) {
	//	experimentalSetup->stationMap[int(i/6)].modules[i].setResolution(resolution[i][0], resolution[i][1]);
	//}
}

void EfficiencyCalculator::updateResolutionFromCsv(std::string fileName) {


    if(!experimentalSetup) {
        throw std::runtime_error("Experimental setup not linked");
    }
    std::cout << "Updating resolution from CSV file: " << fileName << std::endl;

    std::ifstream csvFile(fileName);
    if (!csvFile.is_open()) {
        throw std::runtime_error("Could not open CSV file: " + fileName);
    }
	std::vector<int> modX {0, 4, 6, 10};
	std::vector<int> modY {1, 5, 7, 11};
    std::vector<int> modUV{2, 3, 8, 9};
	std::string line;
    // Skip the header line
    std::getline(csvFile, line);

    while (std::getline(csvFile, line)) {
        std::stringstream ss(line);
        std::string token;
        std::vector<std::string> tokens;

        while (std::getline(ss, token, ',')) {
            tokens.push_back(token);
        }

        if (tokens.size() != 3) {
            throw std::runtime_error("Invalid line format in CSV file");
        }

        int moduleNumber = std::stoi(tokens[0]);
        double resolution = std::stod(tokens[1]);
        // Note: If you need to use the SEM, it's stored in tokens[2]

		double resoX, resoY;
		if (std::find(modX.begin(), modX.end(), moduleNumber) != modX.end()) {
			resoX = resolution;
			resoY = 10000;
		} else if (std::find(modY.begin(), modY.end(), moduleNumber) != modY.end()) {
			resoX = 10000;
			resoY = resolution;
		} else if (std::find(modUV.begin(), modUV.end(), moduleNumber) != modUV.end()) {
			resoX = resolution/sqrt(2);
			resoY = resolution/sqrt(2);
		} else {
			throw std::runtime_error("Invalid module number in CSV file");
		}

        // Updating resolution for the module
        if (moduleNumber < experimentalSetup->getNModules() + 1) {
            experimentalSetup->stationMap[int(moduleNumber / 6)].modules[moduleNumber].setResolution(resoX, resoY);
			experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].setMeanResidualPosition(0, 0);
            // If X and Y resolutions are different, modify the above line accordingly
        }
    }

    csvFile.close();
}



void EfficiencyCalculator::initHistograms() {
	for (int i = 0; i < experimentalSetup->getNModules(); i++) {
		distanceFromInterceptedPointX.push_back(
			new TH1F(("hDistanceFromInterceptedPointX_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointX_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		distanceFromInterceptedPointY.push_back(
			new TH1F(("hDistanceFromInterceptedPointY_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointY_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		distanceFromInterceptedPointZ.push_back(
			new TH1F(("hDistanceFromInterceptedPointZ_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointZ_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		lineDistance.push_back(new TH1F(("hLineDistance_" + std::to_string(i)).c_str(),
										("hLineDistance_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
										10000, -10000, 10000));
		sigmaTrackX.push_back(new TH1F(("hSigmaTrackX_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackX_" + std::to_string(i) + "; sigmaTrack on X[um]; entries").c_str(),
									   1000, 0, 1000));
		sigmaTrackY.push_back(new TH1F(("hSigmaTrackY_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackY_" + std::to_string(i) + "; sigmaTrack on Y[um]; entries").c_str(),
									   1000, 0, 1000));
		nGoodHitsPerStrip.push_back(new TH1F(("hNGoodHitsPerStrip_" + std::to_string(i)).c_str(),
									   ("hNGoodHitsPerStrip_" + std::to_string(i) + "; nGoodHitsPerStrip; entries").c_str(),
										1016*2, -1016, 1016));
									   //50000*2/90, -50000, 50000));
		nTotalHitsPerStrip.push_back(new TH1F(("hNTotalHitsPerStrip_" + std::to_string(i)).c_str(),
									   ("hNTotalHitsPerStrip_" + std::to_string(i) + "; nTotalHitsPerStrip; entries").c_str(),
									   	1016*2, -1016, 1016));
									   //50000*2/90, -50000, 50000));
		efficiencyPerStrip.push_back(new TH1F(("hEfficiencyPerStrip_" + std::to_string(i)).c_str(),
									   ("hEfficiencyPerStrip_" + std::to_string(i) + "; efficiencyPerStrip; entries").c_str(),
									   1016*2, -1016, 1016));
									   //50000*2/90, -50000, 50000));
	}
}

void EfficiencyCalculator::clearHistograms() {
	for (auto &histo : distanceFromInterceptedPointX) {
		histo->Reset();
	}
	for (auto &histo : distanceFromInterceptedPointY) {
		histo->Reset();
	}
	for (auto &histo : distanceFromInterceptedPointZ) {
		histo->Reset();
	}
	for (auto &histo : lineDistance) {
		histo->Reset();
	}
	for (auto &histo : sigmaTrackX) {
		histo->Reset();
	}
	for (auto &histo : sigmaTrackY) {
		histo->Reset();
	}
}

void EfficiencyCalculator::saveHistograms(std::string outputFileName) {
	TFile *newFile = new TFile(outputFileName.c_str(), "RECREATE");
	newFile->cd();
	if (distanceFromInterceptedPointX.size() == 0) {
		throw std::runtime_error("Histograms not initialized");
	}
	for (size_t i = 0; i < distanceFromInterceptedPointX.size(); i++) {
		efficiencyPerStrip[i]->Clone(("hNGoodHitsPerStrip_" + std::to_string(i)).c_str());
		efficiencyPerStrip[i]->Divide(nTotalHitsPerStrip[i]);
		efficiencyPerStrip[i]->SetTitle(("Efficiency module: " + std::to_string(i)).c_str());
		efficiencyPerStrip[i]->SetName(("hEfficiency_" + std::to_string(i)).c_str());
		efficiencyPerStrip[i]->GetXaxis()->SetTitle("position over x [um]");
		efficiencyPerStrip[i]->GetYaxis()->SetTitle("efficiency");
		nGoodHitsPerStrip[i]->Write();
		nTotalHitsPerStrip[i]->Write();
		efficiencyPerStrip[i]->Write();
	}
	newFile->Close();
}

efficiency EfficiencyCalculator::getEfficiencyForModuleNumber(int dut) {
	int nGoodHits = 0;
	int nTotalHits = 0;
	for (int i = 0; i < treeReader->getEntries(); i++) {
		if (i % 10000 == 0) {
			std::cout << "\rEvent " << i << std::flush;
		}
		treeReader->loadEventNumber(i);
		//if (!treeReader->hasOneStubPerModule()) {
		//	continue;
		//}
		trackerFromStubs->clear();

		int totalStubs = 0;
		bool allModulesHaveStub = true;

		for (auto &stationPair : experimentalSetup->stationMap) { // Loop through all stations in stationMap
			StationClass &station = stationPair.second;
			for (auto &module : station.modules) {
				if(module.first == dut) { //skip the module we are aligning
					continue;
				} else {
					for (size_t nStub = 0; nStub < module.second.stubs.size(); nStub++) {
						if(nStub > 0) break; //only use the first stub
						auto stub = module.second.getStub(nStub);
						auto line = module.second.getLineFromStub(nStub);
						trackerFromStubs->pushStubAndLine(stub, line);
					}
					if (module.second.stubs.empty()) {
						allModulesHaveStub = false;
						break;
					}
				}
				totalStubs += module.second.stubs.size();
			}
			//nice, now we have pushed all the other stubs in the tracker, we should be able to make a track
		}
		if(totalStubs < 11 || !allModulesHaveStub) {
			continue;
		} else {
			trackerFromStubs->buildTrackFromCurrentLines();
			if(trackerFromStubs->getCurrentTrack().minDistance < 500*11) {
				nTotalHits++;
				std::cout << "Hey?" << std::endl;
			} else {
				continue;
			}
			Point pointInterceptAtDUT = experimentalSetup->stationMap[int(dut/6)].modules[dut].intersect(trackerFromStubs->getCurrentTrack().trackLine);
			for(size_t stubsIndex = 0; stubsIndex< experimentalSetup->stationMap[int(dut/6)].modules[dut].stubs.size(); stubsIndex++) {
				Line stubLine = experimentalSetup->stationMap[int(dut/6)].modules[dut].getLineFromStub(stubsIndex);
				auto distanceOnX = stubLine.vectorDistance(pointInterceptAtDUT).x;
				auto distanceOnY = stubLine.vectorDistance(pointInterceptAtDUT).y;
				//std::cout << std::endl;
				//std::cout << "Distance on X: " << distanceOnX << " and distance on Y: " << distanceOnY << std::endl;
				//std::cout << "Mean residuals: " << experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(0) << " and " << experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(1) << std::endl;
				//std::cout << "Mean residual positions " << experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(0) << " and " << experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(1) << std::endl;
				//std::cout << "Passes on x: " << (fabs(distanceOnX - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(0) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(0)) ) << std::endl;
				//std::cout << "Passes on y: " << (fabs(distanceOnY - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(1) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(1) )) << std::endl;
				////if((fabs(distanceOnX - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(0) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(0)) )) { std::cout << "Passed on x" << std::endl;}
				//getchar();
				if(    fabs(distanceOnX - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(0) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(0)) 
					&& fabs(distanceOnY - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(1) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(1) ) ) {
					nGoodHits++;
					//if(nGoodHits % 1000 == 0) {
					//	std::cout << std::endl << "Good hit on module " << dut << " with distance on X: " << distanceOnX << " and distance on Y: " << distanceOnY << std::endl;
					//}
					break;
				}
			}
		}
	}
	std::cout << "Module :" << dut <<std::endl << " found " << nGoodHits << " good hits out of " << nTotalHits << " total hits" << std::endl;
	efficiency eff;
	eff.efficiency = nGoodHits*1.0 /nTotalHits;
	eff.error = calculateBinomialError(eff.efficiency, nTotalHits);
	eff.nGood = nGoodHits;
	eff.nTotal = nTotalHits;
	std::cout << "Efficiency: " << eff.efficiency << " +- " << eff.error << std::endl;
	getchar();
	return eff;
}


std::vector<efficiency> EfficiencyCalculator::getEfficiencyInTimeForModuleNumber(int dut) {
	auto startTime = std::chrono::high_resolution_clock::now();
	
	int nGoodHits = 0;
	int nTotalHits = 0;
	std::vector<efficiency> efficiencies;
	unsigned oldSuperID = 0;
	unsigned actualSuperID = 1;
	int nTimesPassedBy = 0;
	std::vector<int> modX {0, 4, 6, 10};
	std::vector<int> modY {1, 5, 7, 11};
    std::vector<int> modUV{2, 3, 8, 9};
	for (int i = 0; i < treeReader->getEntries(); i++) {
		
		if (i % 100000 == 0) {
			auto currentTime = std::chrono::high_resolution_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();
			std::cout << "\rEvent " << i << " over " << treeReader->getEntries() << " : " << i*100.0/treeReader->getEntries() << "% " << " Time elapsed: " << duration << "s - estimated remaining time: " << duration * treeReader->getEntries()*1.0/i <<  std::flush;
		}
		
		treeReader->loadEventNumber(i);

		trackerFromStubs->clear();

		int totalStubs = 0;
		bool allModulesHaveStub = true;
		for (auto &stationPair : experimentalSetup->stationMap) { // Loop through all stations in stationMap
			StationClass &station = stationPair.second;

			for (auto &module : station.modules) {
				if(module.first == dut) { //skip the module we are aligning
					continue;
				} else {
					
					for (size_t nStub = 0; nStub < module.second.stubs.size(); nStub++) {
						if(nStub > 0) break; //only use the first stub
						auto stub = module.second.getStub(nStub);
						actualSuperID = stub.superID;
						if(i != 0 && actualSuperID > oldSuperID + 50) {
							efficiency eff;
							eff.efficiency = nGoodHits*1.0 /nTotalHits;
							eff.error = calculateBinomialError(eff.efficiency, nTotalHits);
							eff.nGood = nGoodHits;
							eff.nTotal = nTotalHits;
							eff.superID = oldSuperID;
							std::cout << std::endl;
							std::cout << "Module: " << dut << std::endl;
							std::cout << eff.nGood << " over " << eff.nTotal << " " << eff.efficiency << " time: " << eff.superID << std::endl;
							std::cout << "Ngood: " << nGoodHits << " nTotal: " << nTotalHits << " eff: " << eff.efficiency << " error: " << eff.error << " superID: " << eff.superID << std::endl;
							efficiencies.push_back(eff);
							nGoodHits = 0;
							nTotalHits = 0;
							oldSuperID = actualSuperID;
							nTimesPassedBy++;

						}
						auto line = module.second.getLineFromStub(nStub);
						trackerFromStubs->pushStubAndLine(stub, line);
					}
				}
				if (module.second.stubs.empty()) {
					allModulesHaveStub = false;
					break;
				}
				totalStubs += module.second.stubs.size();
			}
			//nice, now we have pushed all the other stubs in the tracker, we should be able to make a track
		}

		if(totalStubs < 11 || !allModulesHaveStub) {
			continue;
		} else {
			trackerFromStubs->buildTrackFromCurrentLines();
			Point pointInterceptAtDUT = experimentalSetup->stationMap[int(dut/6)].modules[dut].intersect(trackerFromStubs->getCurrentTrack().trackLine);
			double stripToFill = -999;
			if(trackerFromStubs->getCurrentTrack().minDistance < 11*2) {
				nTotalHits++;
				
				//if (std::find(modX.begin(), modX.end(), dut) != modX.end()) {
				//	stripToFill = pointInterceptAtDUT.x;
				//	
				//} else if (std::find(modY.begin(), modY.end(), dut) != modY.end()) {
				//	stripToFill = pointInterceptAtDUT.y;
				//} else if (std::find(modUV.begin(), modUV.end(), dut) != modUV.end()) {
				//	stripToFill = (pointInterceptAtDUT.x + pointInterceptAtDUT.y) / sqrt(2);
				//}
				//nTotalHitsPerStrip[dut]->Fill(stripToFill);
				nTotalHitsPerStrip[dut]->Fill(experimentalSetup->stationMap[int(dut/6)].modules[dut].getLocalXFromLine(trackerFromStubs->getCurrentTrack().trackLine));
			} else {
				continue;
			}
			for(size_t stubsIndex = 0; stubsIndex< experimentalSetup->stationMap[int(dut/6)].modules[dut].stubs.size(); stubsIndex++) {
				Line stubLine = experimentalSetup->stationMap[int(dut/6)].modules[dut].getLineFromStub(stubsIndex);
				auto distanceOnX = stubLine.vectorDistance(pointInterceptAtDUT).x;
				auto distanceOnY = stubLine.vectorDistance(pointInterceptAtDUT).y;
				if(    fabs(distanceOnX - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(0) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(0)) 
					&& fabs(distanceOnY - experimentalSetup->stationMap[int(dut/6)].modules[dut].getMeanResidualPosition(1) ) < fabs(5*experimentalSetup->stationMap[int(dut/6)].modules[dut].getResolution(1) ) ) {
					nGoodHits++;
					nGoodHitsPerStrip[dut]->Fill(experimentalSetup->stationMap[int(dut/6)].modules[dut].getLocalXFromLine(trackerFromStubs->getCurrentTrack().trackLine));
					//nGoodHitsPerStrip[dut]->Fill(stripToFill);
					break;
				}
			}
		}
	}
	std::cout << "Module " << dut <<std::endl << " found " << nGoodHits << " good hits out of " << nTotalHits << " total hits" << std::endl;
	return efficiencies;
}
