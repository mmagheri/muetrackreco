#pragma once
#include "ExperimentalSetup.hpp"
#include "TrackerFromStubs.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include <vector>
#include <set>
#include <cmath>
#include <chrono>

struct efficiency {
	double efficiency;
	double error;
	int nGood;
	int nTotal;
	unsigned superID;
};


class EfficiencyCalculator {
  private:
	TrackerFromStubs* trackerFromStubs;
	TTreeReader* treeReader;
    ExperimentalSetup* experimentalSetup;

	std::vector<TH1F *> distanceFromInterceptedPointX;
	std::vector<TH1F *> distanceFromInterceptedPointY;
	std::vector<TH1F *> distanceFromInterceptedPointZ;
	std::vector<TH1F *> lineDistance;
	std::vector<TH1F *> sigmaTrackX;
	std::vector<TH1F *> sigmaTrackY;
	
	std::vector<TH1F *> nGoodHitsPerStrip;
	std::vector<TH1F *> nTotalHitsPerStrip;
	std::vector<TH1F *> efficiencyPerStrip;


	//int dut = 0;
	int nTracks = 0;
	int nTracksMax = 10000;

  public:
	EfficiencyCalculator() {};
	~EfficiencyCalculator();
	void linkTracker(TrackerFromStubs* trackerFromStubs_);
	void linkTTreeReader(TTreeReader* treeReader_);
    void linkStation(ExperimentalSetup* expSetup);

	void updateResolutionFromFile(std::string fileName);

	void initHistograms();
	void clearHistograms();
	void saveHistograms(std::string outputFileName);

	efficiency getEfficiencyForModuleNumber(int dut);
	std::vector<efficiency> getEfficiencyInTimeForModuleNumber(int dut);
	void updateResolutionFromCsv(std::string fileName);
	double calculateBinomialError(double efficiency, int totalEvents) {
		double error = sqrt((efficiency * (1 - efficiency)) / totalEvents);
		return error;
	}
};