#pragma once

#include <vector>
#include <string>
#include <stdint.h>
#include "Stub.hpp"
#include "TTreeReader.hpp"

///////////////
// ROOT HEADERS
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>


class StubTTreeReader : public TTreeReader
{
private:
    TTree *eventTree;
    Event* currentEvent = new StubEvent();
    const std::string TREENAME = "cbmsim";
	std::vector<unsigned int>   *SuperID = 0;
	std::vector<unsigned short> *Bx      = 0;
	std::vector<unsigned short> *Link    = 0;
	std::vector<float> *Bend   = 0;
	std::vector<float> *LocalX = 0;
	std::vector<float> *LocalY = 0;

public:
    StubTTreeReader();
    ~StubTTreeReader();
    void parseTree(std::string filename);
    Event* getEvent(int nEv);
    Event* getNextEvent();
};

