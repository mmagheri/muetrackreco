#include "Tracker.hpp"
#include "xmlUtils.hpp"
#include <gtest/gtest.h>


TEST(Struct, multistubs) {
    ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_reissued.xml");
    
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 1, 0, 0, 0, 0, 0});

    Tracker tracker(experimentSetup);
    tracker.buildAllTracksFromCurrentLoadedEvent(0);
    std::cout << "NUMBER OF TRACKS: " << tracker.allTracksGood.size() << std::endl;
}