#include "Vertexer.hpp"

Vertexer::Vertexer() {
	vertexPosition = new TH1F("vertexPosition", "vertexPosition", 500, 0.8e6, 1.5e6);
	vertexTrackS1Distance = new TH1F("vertexTrackS1Distance", "vertexTrackS1Distance", 500, 0, 1e4);
	angleBetweenTracks = new TH2F("angleBetweenTracks", "angleBetweenTracks", 500, 0, 0.07, 500, 0, 0.003);
	chiSquareFirstStation = new TH1F("chiSquareFirstStation", "chiSquareFirstStation", 500, 0, 100);
	chiSquareSecondStation = new TH1F("chiSquareSecondStation", "chiSquareSecondStation", 500, 0, 100);
	
	outTree = new TTree("tree", "Vertex Data");
	outTree->Branch("vertexPosition", &vertexPositionValue);
	outTree->Branch("vertexTrackS1Distance", &vertexTrackS1DistanceValue);
	outTree->Branch("angleBetweenTracksMax", &angleBetweenTracksMinValue);
	outTree->Branch("angleBetweenTracksMin", &angleBetweenTracksMaxValue);
	outTree->Branch("chi2S1tk1", &chiSquareTk1S1);
	outTree->Branch("chi2S2tk1", &chiSquareTk1S2);
	outTree->Branch("chi2S2tk2", &chiSquareTk2S2);
}

Vertexer::~Vertexer() {
	delete vertexPosition;
	delete vertexTrackS1Distance;
	delete angleBetweenTracks;
}

void Vertexer::linkTracker(TrackerFromStubs* tracker_){
	tracker = tracker_;
}

void Vertexer::linkTTreeReader(TTreeReader* treeReader_){
	treeReader = treeReader_;
}

void Vertexer::linkStation(ExperimentalSetup* expSetup){
	experimentalSetup = expSetup;
}

void Vertexer::getVertex() {
	reinitTreeValues();
	int nTotal = 0;
	int nEventsWithSelectedNumberOfTracks = 0;
	int nPassTrackChi2 = 0;
	int nPassVertex = 0;
	int nPassDistance = 0;
	int nGoodEvents = 0;
	for (int i = 0; i < treeReader->getEntries(); i++) {
		nTotal++;
		if (i % 100000 == 0) {
			std::cout << "\rEvent " << i << " over - " << treeReader->getEntries() << std::flush;
		}
		treeReader->loadEventNumber(i);
		//filter the events: only events with just one stub per module in the first station and two stubs per module in the second one
		bool s1IsGood = true;
		StationClass &firstStation = experimentalSetup->stationMap[0];
		for (auto &module : firstStation.modules) {
			if(module.second.stubs.size() != 1) {
				s1IsGood = false;
				break;
			}
		}
		if (!s1IsGood) {
			continue;
		}
		bool s2IsGood = true;
		StationClass &secondStation = experimentalSetup->stationMap[1];
		for (auto &module : secondStation.modules) {
			if(module.second.stubs.size() != 2) {
				s2IsGood = false;
				break;
			}
		}
		if (!s2IsGood) {
			continue;
		}
		//auto trkS1_good = tracker->buildAllTracksFromCurrentLoadedEventInStation(0);
		auto trkS2_good = tracker->buildAllTracksFromCurrentLoadedEventInStation(1);
		auto trkS1_good = tracker->buildSingleTrackFromCurrentLinesInStation(0);

		//std::cout << std::endl << "We have: " << trkS1_good.size() << "  tracks in station 1 and " << trkS2_good.size() << " tracks in station 2" << std::endl;
		//filter the event if we have != than 1 track in s1 and 2 in s2
		if(trkS1_good.size() != 1 || trkS2_good.size() != 2) {
			continue;
		}
		nEventsWithSelectedNumberOfTracks++;
		chiSquareFirstStation->Fill(trkS1_good[0].minDistance);
		chiSquareSecondStation->Fill(trkS2_good[0].minDistance);
		chiSquareSecondStation->Fill(trkS2_good[1].minDistance);

		Line & line1 = trkS2_good[0].trackLine;
		Line & line2 = trkS2_good[1].trackLine;
		Point vertex = line1.closestPointWrtLine(line2);
		vertexPosition->Fill(vertex.z);
		vertexPositionValue = vertex.z;

		Line & incomingLine = trkS1_good[0].trackLine;
		double distance = incomingLine.distance(vertex);
		vertexTrackS1Distance->Fill(distance);
		vertexTrackS1DistanceValue = distance;
		double mean = 1.01173e+06;
		double sigma = 1.47609e+04;

		double angle1 = incomingLine.angle(line1);
		double angle2 = incomingLine.angle(line2);
		double minAngle = std::min(angle1, angle2);
		double maxAngle = std::max(angle1, angle2);
		angleBetweenTracksMaxValue = maxAngle;
		angleBetweenTracksMinValue = minAngle;
		chiSquareTk1S1 = trkS1_good[0].minDistance;
		chiSquareTk1S2 = trkS2_good[0].minDistance;
		chiSquareTk2S2 = trkS2_good[1].minDistance;
		outTree->Fill();
		if(trkS1_good[0].minDistance > 6 || trkS2_good[0].minDistance > 6 || trkS2_good[1].minDistance > 6) {
			continue;
		}
		nPassTrackChi2++;
		if (vertex.z < mean - 3*sigma || vertex.z > mean + 3*sigma) {
			continue;
		}
		nPassVertex++;
		//select good matching distance, like, .5 cm
		if(distance > 200) {
			continue;
		}
		nPassDistance++;
		angleBetweenTracks->Fill(maxAngle, minAngle);
		//angle between the two tracks
	}
	std::cout << std::endl;
	std::cout << "We have found: " << vertexPosition->GetEntries() << " vertices" << std::endl 
			  << "nPassSelectedTracks: " << nEventsWithSelectedNumberOfTracks << std::endl
			  << "nPassTrackChi2: " << nPassTrackChi2 << std::endl
			  << "nPassVertex: " << nPassVertex << std::endl
			  << "nPassDistance: " << nPassDistance << std::endl;
}

void Vertexer::saveHistograms(std::string outputFile) {
	TFile *file = new TFile(outputFile.c_str(), "RECREATE");
	vertexPosition->Write();
	vertexTrackS1Distance->Write();
	angleBetweenTracks->Write();
	chiSquareFirstStation->Write();
	chiSquareSecondStation->Write();
	outTree->Write();
	file->Close();
}


void Vertexer::updateResolutionFromCsv(std::string fileName) {

    if(!experimentalSetup) {
        throw std::runtime_error("Experimental setup not linked");
    }
    std::cout << "Updating resolution from CSV file: " << fileName << std::endl;

    std::ifstream csvFile(fileName);
    if (!csvFile.is_open()) {
        throw std::runtime_error("Could not open CSV file: " + fileName);
    }
	std::vector<int> modX {0, 4, 6, 10};
	std::vector<int> modY {1, 5, 7, 11};
    std::vector<int> modUV{2, 3, 8, 9};
	std::string line;
    // Skip the header line
    std::getline(csvFile, line);
	double allReso[12];
    while (std::getline(csvFile, line)) {
        std::stringstream ss(line);
        std::string token;
        std::vector<std::string> tokens;

        while (std::getline(ss, token, ',')) {
            tokens.push_back(token);
        }

        if (tokens.size() != 3) {
            throw std::runtime_error("Invalid line format in CSV file");
        }

        int moduleNumber = std::stoi(tokens[0]);
        double resolution = std::stod(tokens[1]);
		allReso[moduleNumber] = resolution;
        // Note: If you need to use the SEM, it's stored in tokens[2]

		double resoX, resoY;
		if (std::find(modX.begin(), modX.end(), moduleNumber) != modX.end()) {
			resoX = resolution;
			resoY = 10000;
		} else if (std::find(modY.begin(), modY.end(), moduleNumber) != modY.end()) {
			resoX = 10000;
			resoY = resolution;
		} else if (std::find(modUV.begin(), modUV.end(), moduleNumber) != modUV.end()) {
			resoX = resolution/sqrt(2);
			resoY = resolution/sqrt(2);
		} else {
			throw std::runtime_error("Invalid module number in CSV file");
		}

        // Updating resolution for the module
        if (moduleNumber < experimentalSetup->getNModules() + 1) {
            experimentalSetup->stationMap[int(moduleNumber / 6)].modules[moduleNumber].setResolution(resoX, resoY);
			experimentalSetup->stationMap[int(moduleNumber/6)].modules[moduleNumber].setMeanResidualPosition(0, 0);
            // If X and Y resolutions are different, modify the above line accordingly
        }
    }
	tracker->setModulesResolutions(allReso);

    csvFile.close();
}

void Vertexer::reinitTreeValues() {
	vertexPositionValue = 999;
	vertexTrackS1DistanceValue = 999;
	angleBetweenTracksMaxValue = 999;
	angleBetweenTracksMinValue = 999;
	chiSquareTk1S1 = 999;
	chiSquareTk1S2 = 999;
	chiSquareTk2S2 = 999;
}