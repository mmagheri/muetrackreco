import requests
import json

def query_data():
    url = "http://muedaq-supervisor/api/v1/data/merged/2/query"
    payload = {
        "query": {
            "all_module_events": {"$gt": 0}
        }
    }

    response = requests.post(url, json=payload)
    return response.json()

# Call the function and print the result in a formatted way
result = query_data()
data = result["data"]

good_paths = []
for item in data:
    path = item["path"]
    events = item["events"]
    all_module_events = item["all_module_events"]
    print("Path:", path)
    print("Events:", events)
    print("All Module Events:", all_module_events)
    if(all_module_events > 1e5):
        good_paths.append(path)

for path in good_paths:
    print("\"", path, "\",")
#print(good_paths)
#print(json.dumps(result, indent=4))
