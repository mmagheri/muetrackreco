FROM gitlab-registry.cern.ch/linuxsupport/alma9-base
RUN dnf -y install cmake epel-release
RUN yum -y install git make cmake gcc-c++ gcc binutils libX11-devel libXpm-devel libXft-devel libXext-devel python openssl-devel
RUN yum -y install gcc-gfortran pcre-devel mesa-libGL-devel mesa-libGLU-devel glew-devel ftgl-devel mysql-devel fftw-devel cfitsio-devel graphviz-devel libuuid-devel avahi-compat-libdns_sd-devel openldap-devel python-devel python3-numpy libxml2-devel gsl-devel readline-devel qt5-qtwebengine-devel R-devel R-Rcpp-devel R-RInside-devel
RUN dnf -y install root*
RUN dnf -y install root*
RUN git clone --recurse-submodule https://gitlab.cern.ch/mmagheri/muetrackreco.git
WORKDIR /muetrackreco
RUN mkdir -p build
WORKDIR /muetrackreco/build
RUN cmake ..
RUN make -j20