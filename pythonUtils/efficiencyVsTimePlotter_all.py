import pandas as pd
import sys
import os
import matplotlib.pyplot as plt

# Read the CSV file
input_file = sys.argv[1]  # Get the input file from command-line argument
output_file = sys.argv[2]  # Get the output file from command-line argument

mNumb = [str(i) for i in range(0, 12)]
folder_path = input_file

# Create subplots in a 2x6 grid
fig, axs = plt.subplots(4, 3, figsize=(20, 30))  # Adjusted figsize for better visibility

for idx, module_number in enumerate(mNumb):
    print("Processing module " + module_number)
    col = idx // 4  # Determine row index
    row = idx % 4   # Determine column index

    chosen_string = "_module" + module_number + ".csv"
    csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv') and chosen_string in file]

    # Initialize an empty list to store the dataframes
    dfs = []

    # Iterate over the CSV files and load them into dataframes
    for file in csv_files:
        file_path = os.path.join(folder_path, file)
        df = pd.read_csv(file_path)
        dfs.append(df)

    # Concatenate the dataframes into a single dataframe
    combined_df = pd.concat(dfs, ignore_index=True)

    # Filter out rows where superID is 0 and nTotal > 100
    df_filtered = combined_df[(combined_df['superID'] != 0) & (combined_df['nTotal'] > 100)].copy()

    # Multiply superID by 25*3564*32*E-9
    df_filtered['superID'] *= 25 * 3564 * 32 * 1e-9 
    df_filtered['superID'] -= (15010 + 2.5)

    # Filter out rows where superID is bigger than 10
    df_filtered = df_filtered[df_filtered['superID'] <= 5]

    # Plotting on the current subplot
    axs[row, col].scatter(df_filtered['superID'], df_filtered['efficiency'], alpha=0.7)
    axs[row, col].set_title('Module ' + module_number)
    axs[row, col].set_xlabel('Time [s]')
    axs[row, col].set_ylabel('Efficiency')
    axs[row, col].grid(True)

    # Set y-axis limit
    axs[row, col].set_ylim([0.92, 1.025])

# Adjust layout to prevent overlapping
plt.tight_layout()

# Save the entire figure as a PNG file
plt.savefig(output_file, dpi=300)

# Optionally display the plot
# plt.show()
