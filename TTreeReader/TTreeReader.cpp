#include "TTreeReader.hpp"


TTreeReader::~TTreeReader() {
    delete eventTree;
    delete SuperID;
    delete Bx;
    delete Link;
    delete Bend;
    delete LocalX;
    delete LocalY;
}

void TTreeReader::parseTree(std::string filename) {
    TFile * f = new TFile(filename.c_str(), "READ");
    eventTree = (TTree*) f->Get("cbmsim");
    eventTree->SetBranchAddress("SuperID", &SuperID);
	eventTree->SetBranchAddress("Bx",      &Bx);
	eventTree->SetBranchAddress("Link",    &Link);
	eventTree->SetBranchAddress("Bend",    &Bend);
	eventTree->SetBranchAddress("LocalX",  &LocalX);
	eventTree->SetBranchAddress("LocalY",  &LocalY);
}

//set station

void TTreeReader::loadEventNumber(int nEv) {
    eventTree->GetEntry(nEv);
    experimentalSetup->clear();
    //currentEvent->clear();
    if(SuperID->size() == 0) {
        throw std::runtime_error("No stubs in this event");
    }
    Stub tmpStub;
    for(size_t i=0; i<SuperID->size(); i++) {
        tmpStub.superID = SuperID->at(i);
        tmpStub.bend = Bend->at(i);
        tmpStub.bx = Bx->at(i);
        tmpStub.link = Link->at(i);
        tmpStub.localX = LocalX->at(i);
        tmpStub.localY = LocalY->at(i);
        experimentalSetup->pushStub(tmpStub);
    }
}

void TTreeReader::loadNextNBx(int nEv, int nConsecutiveBxs) {
    experimentalSetup->clear();
	while (nEv + nConsecutiveBxs > eventTree->GetEntries()) {
		nConsecutiveBxs--;
	}
	for (int evToLoad = nEv; evToLoad < nEv + nConsecutiveBxs; evToLoad++) {
        eventTree->GetEntry(evToLoad);

        // currentEvent->clear();
        if (SuperID->size() == 0) {
            throw std::runtime_error("No stubs in this event");
        }
        Stub tmpStub;
        for (size_t i = 0; i < SuperID->size(); i++) {
            tmpStub.superID = SuperID->at(i);
            tmpStub.bend = Bend->at(i);
            tmpStub.bx = Bx->at(i);
            tmpStub.link = Link->at(i);
            tmpStub.localX = LocalX->at(i);
            tmpStub.localY = LocalY->at(i);
            experimentalSetup->pushStub(tmpStub);
        }
	}
}


long TTreeReader::getEntries() {
    return eventTree->GetEntries();
}

bool TTreeReader::hasOneStubPerModule(){
    for(auto & station : experimentalSetup->stationMap) {
        for(auto & m: station.second.modules) {
            if(m.second.getNStubs() != 1) return false;
        }
    }
    return true;
};

bool TTreeReader::hasAtLeastNStubs(int n){
    int nTotStubs = 0;
    for(auto & station : experimentalSetup->stationMap) {
        for(auto & m: station.second.modules) {
            nTotStubs += m.second.getNStubs();
        }
    }
    if(nTotStubs >=n ) return true;
    return false;
};

bool TTreeReader::hasNStubsPerModule(int n){
    bool output = true;
    for(auto & station : experimentalSetup->stationMap) {
        for(auto & m: station.second.modules) {
            //std::cout << "Module " << m.first << " has " << m.second.getNStubs() << " stubs" << std::endl;
            if(m.second.getNStubs()!=n){
                output = false;
            }
        }
    }
    return output;
};

bool TTreeReader::hasOneEvenStubPerModule(){
    for(auto & station : experimentalSetup->stationMap) {
        for(auto & m: station.second.modules) {
            for(auto & s: m.second.stubs) {
                if(int(s.localX*2) %2  != 0) return false;
            }
        }
    }
    return true;
};

bool TTreeReader::hasOneOddStubPerModule(){
    for(auto & station : experimentalSetup->stationMap) {
        for(auto & m: station.second.modules) {
            for(auto & s: m.second.stubs) {
                if(int(s.localX*2) %2  == 0) return false;
            }
        }
    }
    return true;
};

std::map<int, int> TTreeReader::getNStubsPerModuleInStation(int station){
    std::map<int, int> stubsPerModule;
    for(auto & m: experimentalSetup->stationMap[station].modules) {
        stubsPerModule[m.first] = m.second.getNStubs();
    }
    return stubsPerModule;
};