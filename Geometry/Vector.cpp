#include "Vector.hpp"


double Vector::length() {
    return sqrt(x * x + y * y + z * z);
}

void Vector::normalize() {
    double len = length();
    x /= len;
    y /= len;
    z /= len;
}

void Vector::print() {
    std::cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
}

Vector Vector::operator+(Vector v) {
    return Vector(x + v.x, y + v.y, z + v.z);
}

Vector Vector::operator-(Vector v) {
    return Vector(x - v.x, y - v.y, z - v.z);
}

double Vector::operator*(Vector v) {
    return x * v.x + y * v.y + z * v.z;
}

Vector Vector::operator^(Vector v) {
    return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
}

Vector Vector::operator*(double s) {
    return Vector(x * s, y * s, z * s);
}

Vector Vector::operator/(double s) {
    return Vector(x / s, y / s, z / s);
}

bool Vector::operator==(const Vector& v) const {
    return x == v.x && y == v.y && z == v.z;
}

Point Vector::toPoint() {
    return Point(x, y, z);
}

double Vector::angle(Vector v1) {
    double dotProduct = v1 * *this;
    double magnitudeProduct = v1.length() * this->length();
    return acos(dotProduct / magnitudeProduct);
}

