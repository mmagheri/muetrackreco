import pandas as pd
import sys
import os
from math import sqrt
import matplotlib.pyplot as plt


def calculateBinomialError(efficiency, totalEvents):
    return sqrt((efficiency * (1 - efficiency)) / totalEvents)

module_number = [str(i) for i in range(12)]

# Read the CSV file
input_file = sys.argv[1]  # Get the input file from command-line argument
folder_path = input_file

efficiencies = []
efficiencies_errors = []

for mNumb in module_number:
    chosen_string = "_module" + mNumb
    csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv') and chosen_string in file]

    dfs = []
    for file in csv_files:
        file_path = os.path.join(folder_path, file)
        df = pd.read_csv(file_path)
        dfs.append(df)

    combined_df = pd.concat(dfs, ignore_index=True)

    df_filtered = combined_df[(combined_df['superID'] != 0) & (combined_df['nTotal'] > 100)]

    efficiency = df_filtered['nGood'].sum() / df_filtered['nTotal'].sum()
    n_total = df_filtered['nTotal'].sum()

    binomial_error = calculateBinomialError(efficiency, n_total)
    efficiencies_errors.append(binomial_error)
    efficiencies.append(efficiency)

module_number = list(map(int, module_number))

results_df = pd.DataFrame({
    'Module Number': module_number,
    'Efficiency': efficiencies,
    'Binomial Error': efficiencies_errors
})

plt.errorbar(module_number, efficiencies, yerr=efficiencies_errors, fmt='o')
plt.xlabel('Module Number')
plt.ylabel('Efficiency')
plt.ylim([0.92, 1.025])
plt.title('Efficiency with Error')
plt.grid(True)



plt.savefig('efficiency_vs_module_number_run2.png', dpi=300)
csv_output_path = 'efficiency_results_run2.csv'
results_df.round(4).to_csv(csv_output_path, index=False)

print(f"Results saved to {csv_output_path}")
