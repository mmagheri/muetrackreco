#include "StationClass.hpp"

void StationClass::print() {
	std::cout << "Station " << name << " with " << modules.size() << " modules" << std::endl;
	std::cout << "Position: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
	std::cout << "Rotation: " << rot[0] << " " << rot[1] << " " << rot[2] << std::endl;
	for (auto &m : modules) {
		m.second.print();
	}
}

void StationClass::printNModules() {
	std::cout << "Station " << name << " with " << modules.size() << " modules" << std::endl;
}

void StationClass::printStub() {
	std::cout << "Station " << name << " with " << nModules << " modules" << std::endl;
	//std::cout << "Position: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
	//std::cout << "Rotation: " << rot[0] << " " << rot[1] << " " << rot[2] << std::endl;
	for (auto &m : modules) {
		m.second.printStub();
	}
}

void StationClass::clear() {
	for (auto &m : modules) {
		m.second.clear();
	}
}

void StationClass::pushStub(Stub s) {
	modules[s.link].pushStub(s); // TODO rewrite to search between links
	if(modules.size() != nModules) {
		throw std::runtime_error("Number of modules in station is not equal to nModules");
	}
}

Track StationClass::buildTrackExceptModule(int nModuleToRemove) {
	Track t;

	for (auto &m : modules) {
		if (m.first == nModuleToRemove)
			continue;
		if (m.second.getNStubs() == 1) {
			t.addLine(m.second.getLineFromStub(0));
			t.addStub(m.second.stubs.at(0));
		}
	}

	if (t.hasEnoughLines())
		t.minimizeDistances();
	return t;
}

Track StationClass::buildMultipleTrackExceptModule(int nModuleToRemove) {
	Track t;
	for (auto &m : modules) {
		if (m.first == nModuleToRemove)
			continue;
		if (m.second.getNStubs() > 0) {
			auto lines = m.second.getMultipleLinesFromStub(0);
			for (auto &l : lines)
				t.addLine(l);
		}
	}
	if (t.hasEnoughLines())
		t.minimizeDistances();
	return t;
}

Track StationClass::buildXYTrack() {
	Track t;
	// std::cout << "----Building XY Track---\n";
	for (auto &m : modules) {
		if (m.first == 2)
			continue;
		if (m.first == 3)
			continue;
		if (m.second.getNStubs() > 0) {
			t.addLine(m.second.getLineFromStub(0));
		}
	}
	if (t.hasEnoughLines())
		t.minimizeDistances();
	return t;
}

Track StationClass::buildSimpleXYTrack() {
	// std::cout << "----Building XY Track---\n";
	Track t;
	Point p1(0, 0, 0);
	Point p2(0, 0, 0);
	for (auto &m : modules) {
		if (m.first == 2)
			continue;
		if (m.first == 3)
			continue;
		if (m.second.getNStubs() > 0) {
			t.addLine(m.second.getLineFromStub(0));
			if (m.first == 0) {
				p1.x = m.second.getLineFromStub(0).p1.x;
				p1.z = m.second.getLineFromStub(0).p1.z;
			}
			if (m.first == 1) {
				p1.y = m.second.getLineFromStub(0).p1.x;
			}
			if (m.first == 4) {
				p2.x = m.second.getLineFromStub(0).p1.x;
				p2.z = m.second.getLineFromStub(0).p1.z;
			}
			if (m.first == 5) {
				p2.y = m.second.getLineFromStub(0).p1.x;
			}
		}
	}
	t.trackLine = Line(p1, p2);
	t.minDistance = 0;
	for (auto l : t.lines) {
		t.minDistance += fabs(t.trackLine.distance(l));
	}
	// getchar();
	return t;
}

Track StationClass::buildTrack() {
	Track t;
	for (auto &m : modules) {
		if (m.second.getNStubs() > 0) {
			t.addLine(m.second.getLineFromStub(0));
			t.addStub(m.second.stubs.at(0));
		}
	}
	if (t.hasEnoughLines())
		t.minimizeDistances();
	return t;
}

void StationClass::dumpStubsToJson(nlohmann::json &existingJson, int eventNumber) {
	nlohmann::json newEvent;
	newEvent["Number"] = eventNumber;

	for (auto &pair : modules) {
		ModuleClass &module = pair.second;
		for (size_t i = 0; i < module.stubs.size(); i++) {
			Line line = module.getLineFromStub(i);
			newEvent["stubs"].push_back(
				{{"p1", {line.p1.x, line.p1.y, line.p1.z}}, {"p2", {line.p2.x, line.p2.y, line.p2.z}}});
		}
	}
	existingJson["Event"].push_back(newEvent);
}

Track StationClass::buildTrackFromStubs(std::vector<Stub> stubs) {
	Track t;
	for (auto &s : stubs) {
		t.addLine(modules[s.link].getLineFromStub(s));
		t.addStub(s);
	}
	if (t.hasEnoughLines()) {
		t.minimizeDistances();
	}
	return t;
}

bool StationClass::removeStub(Stub s) { return modules[s.link % 6].removeStub(s); }
