#include "Tracker.hpp"

// Tracker::Tracker(const std::string& inputFileName, bool considerBend, const std::string& outputFileName,
// ExperimentalSetup& experimentSetup_) : experimentSetup(experimentSetup_) {
//     t->parseTree(inputFileName.c_str());
//     experimentSetup.print();
//     newFileName = outputFileName;
//

//}

Tracker::~Tracker() {
    for (size_t i = 0; i < distanceFromInterceptedPointX.size(); i++) {
        delete distanceFromInterceptedPointX[i];
        delete distanceFromInterceptedPointY[i];
        delete distanceFromInterceptedPointZ[i];
        delete sigmaTrackX[i];
        delete sigmaTrackY[i];
        delete lineDistance[i];
    }
}

void Tracker::setNTracksMax(int newMax) { nTracksMax = newMax; }

void Tracker::setTreeReader(TTreeReader *t_) { t = t_; }

void Tracker::useAllModules() { allModules = true; }

void Tracker::fillHistograms() {
	int nTracks = 0;
	for (int i = 0; i < t->getEntries() && nTracks < nTracksMax; i++) {
		if (i % 10000 == 0)
			std::cout << "Event " << i << std::endl;
		t->loadEventNumber(i);
		if (!t->hasOneStubPerModule() && oneStubPerModule)
			continue;

		for (auto &stationPair : t->experimentalSetup->stationMap) { // Loop through all stations in stationMap
			StationClass &station = stationPair.second;
            //get number of modules in station
			if (allModules) {
				currentTrack = station.buildTrack();
			}

			for (size_t dut = 0; dut < station.modules.size(); dut++) {
				if (!allModules) {
					currentTrack = station.buildTrackExceptModule(dut);
					currentTrack.DUT = dut;
				}
				if (sqrt(currentTrack.minDistance) < 100 * 5) {
					Line stubLine = station.modules.at(dut).getLineFromStub(0);
					if (fabs(stubLine.distance(currentTrack.trackLine)) > 10000)
						continue;

					Point pointInterceptAtDUT = station.modules.at(dut).intersect(currentTrack.trackLine);

					double t = currentTrack.trackLine.getT(pointInterceptAtDUT);

					sigmaTrackX[dut]->Fill(currentTrack.getErrorsFromTrackCalculatedAtT(t)[0]);
					sigmaTrackY[dut]->Fill(currentTrack.getErrorsFromTrackCalculatedAtT(t)[1]);
					distanceFromInterceptedPointX[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).x);
					distanceFromInterceptedPointY[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).y);
					distanceFromInterceptedPointZ[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).z);
					lineDistance[dut]->Fill(stubLine.distance(currentTrack.trackLine));

					if (dut == 0)
						nTracks++;
				}
			}
		}
	}
}

void Tracker::fillHistograms_multiBx(int nBxs) {
	int nTracks = 0;
	for (int i = 0; i < t->getEntries() && nTracks < nTracksMax; i++) {
		if (i % 10000 == 0)
			std::cout << "Event " << i << std::endl;
		t->loadNextNBx(i, 2);

		for (auto &stationPair : t->experimentalSetup->stationMap) { // Loop through all stations in stationMap
			buildAllTracksFromCurrentLoadedEvent(stationPair.first);
			StationClass &station = stationPair.second;
			// get number of modules in station
			if (allTracksGood.size() != 1) continue; 

			for (size_t dut = 0; dut < station.modules.size(); dut++) {
				if (sqrt(currentTrack.minDistance) < 100 * 5) {
					Line stubLine = station.modules.at(dut).getLineFromStub(0);
					auto track = allTracksGood.at(0);
					for (auto &tk : allTracksGood) {
						if (tk.trackLine.distance(stubLine) < track.trackLine.distance(stubLine)) {
							track = tk;
						}
					}
					if (fabs(stubLine.distance(track.trackLine)) > 10000)
						continue;

					Point pointInterceptAtDUT = station.modules.at(dut).intersect(track.trackLine);

					double t = track.trackLine.getT(pointInterceptAtDUT);

					sigmaTrackX[dut]->Fill(track.getErrorsFromTrackCalculatedAtT(t)[0]);
					sigmaTrackY[dut]->Fill(track.getErrorsFromTrackCalculatedAtT(t)[1]);
					distanceFromInterceptedPointX[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).x);
					distanceFromInterceptedPointY[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).y);
					distanceFromInterceptedPointZ[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).z);
					lineDistance[dut]->Fill(stubLine.distance(track.trackLine));

					if (dut == 0)
						nTracks++;
				}
			}
		}
	}
}

void Tracker::moveModule(int mod, int axis, double distance) {
	experimentSetup.stationMap[0].modules[mod].pos[axis] += distance;
}

void Tracker::initHistograms() {
    for (int i = 0; i < experimentSetup.getNModules(); i++) {
		distanceFromInterceptedPointX.push_back(
			new TH1F(("hDistanceFromInterceptedPointX_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointX_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		distanceFromInterceptedPointY.push_back(
			new TH1F(("hDistanceFromInterceptedPointY_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointY_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		distanceFromInterceptedPointZ.push_back(
			new TH1F(("hDistanceFromInterceptedPointZ_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointZ_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 10000, -10000, 10000));
		lineDistance.push_back(new TH1F(("hLineDistance_" + std::to_string(i)).c_str(),
										("hLineDistance_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
										10000, -10000, 10000));
		sigmaTrackX.push_back(new TH1F(("hSigmaTrackX_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackX_" + std::to_string(i) + "; sigmaTrack on X[um]; entries").c_str(),
									   1000, 0, 1000));
		sigmaTrackY.push_back(new TH1F(("hSigmaTrackY_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackY_" + std::to_string(i) + "; sigmaTrack on Y[um]; entries").c_str(),
									   1000, 0, 1000));
	}
}

void Tracker::clearHistograms() {
	if (distanceFromInterceptedPointX.size() == 0) {
		throw std::runtime_error("Histograms not initialized");
	}
	for (size_t i = 0; i < distanceFromInterceptedPointX.size(); i++) {
		distanceFromInterceptedPointX[i]->Reset();
		distanceFromInterceptedPointY[i]->Reset();
		distanceFromInterceptedPointZ[i]->Reset();
		sigmaTrackX[i]->Reset();
		sigmaTrackY[i]->Reset();
		lineDistance[i]->Reset();
	}
}

void Tracker::saveHistograms() {
	TFile *newFile = new TFile(newFileName.c_str(), "RECREATE");
	newFile->cd();
	if (distanceFromInterceptedPointX.size() == 0) {
		throw std::runtime_error("Histograms not initialized");
	}
	for (size_t i = 0; i < distanceFromInterceptedPointX.size(); i++) {
		distanceFromInterceptedPointX[i]->Write();
		distanceFromInterceptedPointY[i]->Write();
		distanceFromInterceptedPointZ[i]->Write();
		sigmaTrackX[i]->Write();
		sigmaTrackY[i]->Write();
		lineDistance[i]->Write();
	}
	newFile->Close();
}

double Tracker::alignModule(int mod, int axis) {
	double meanPositionOnAxis = 10000;
	oneStubPerModule = true;
	// int iterations = 0;
	// do {
	// std::cout << "Iteration " << iterations << std::endl;
	clearHistograms();
	fillHistograms();
	int binmaxX = distanceFromInterceptedPointX[mod]->GetMaximumBin();
	double x = distanceFromInterceptedPointX[mod]->GetXaxis()->GetBinCenter(binmaxX);

	int binmaxY = distanceFromInterceptedPointY[mod]->GetMaximumBin();
	double y = distanceFromInterceptedPointY[mod]->GetXaxis()->GetBinCenter(binmaxY);

	int binmaxZ = distanceFromInterceptedPointZ[mod]->GetMaximumBin();
	double z = distanceFromInterceptedPointZ[mod]->GetXaxis()->GetBinCenter(binmaxZ);

	double meanPos[3]{x, y, z};
	meanPositionOnAxis = meanPos[axis];
	std::cout << "Mean position of residual for module " << mod << " on axis " << axis << " is " << meanPositionOnAxis
			  << std::endl;
	moveModule(mod, axis,
			   meanPositionOnAxis /
				   10000.0); // TODO: Difference points in the wrong direction - to adjust with the sums
	// iterations++;
	if (saveAlignmentHist)
		saveHistograms();
	// if(iterations >500) break;
	// if(fabs(meanPositionOnAxis) < 30) break;
	//} while(true);
	return meanPositionOnAxis;
	// getchar();
}

double Tracker::alignModule_multiBx(int mod, int axis, int nBx) {
	double meanPositionOnAxis = 10000;
	oneStubPerModule = true;
	// int iterations = 0;
	// do {
	// std::cout << "Iteration " << iterations << std::endl;
	clearHistograms();
	fillHistograms_multiBx(nBx);
	int binmaxX = distanceFromInterceptedPointX[mod]->GetMaximumBin();
	double x = distanceFromInterceptedPointX[mod]->GetXaxis()->GetBinCenter(binmaxX);

	int binmaxY = distanceFromInterceptedPointY[mod]->GetMaximumBin();
	double y = distanceFromInterceptedPointY[mod]->GetXaxis()->GetBinCenter(binmaxY);

	int binmaxZ = distanceFromInterceptedPointZ[mod]->GetMaximumBin();
	double z = distanceFromInterceptedPointZ[mod]->GetXaxis()->GetBinCenter(binmaxZ);

	double meanPos[3]{x, y, z};
	meanPositionOnAxis = meanPos[axis];
	std::cout << "Mean position of residual for module " << mod << " on axis " << axis << " is " << meanPositionOnAxis
			  << std::endl;
	moveModule(mod, axis,
			   meanPositionOnAxis /
				   10000.0); // TODO: Difference points in the wrong direction - to adjust with the sums
	// iterations++;
	if (saveAlignmentHist)
		saveHistograms();
	// if(iterations >500) break;
	// if(fabs(meanPositionOnAxis) < 30) break;
	//} while(true);
	return meanPositionOnAxis;
	// getchar();
}

double Tracker::getEfficiency(int nModule) {
	int nTracks = 0;
	int nCompatibleInModuleI = 0;
	bool good = true;
	// double MaxModule[6]{10000, 4000, 3000, 3000, 10000, 5000};
	// double MinModule[6]{5000,  0,    1500, 1500,  5000,  0};

	for (int i = 0; i < t->getEntries() && nTracks < nTracksMax; i++) {
		t->loadEventNumber(i);
		if (i % 10000 == 0) {
			std::cout << "Event " << i << " nTrack: " << nTracks << std::endl;
		}
		good = true;
		int nTotStubs = 0;
		nTotStubs += t->experimentalSetup->stationMap[0].modules[nModule].getNStubs();
		for (int j = 0; j < 6; j++) {
			if (j == nModule)
				continue;
			nTotStubs += t->experimentalSetup->stationMap[0].modules[j].getNStubs();
			if (t->experimentalSetup->stationMap[0].modules[j].getNStubs() != 1) {
				good = false;
			}
		}
		if (!good) {
			// std::cout << "Skipping - " << i << " nstubs: " << nTotStubs << " " <<
			// t->experimentalSetup->stationMap[0].modules[0].getNStubs() << std::endl;
			continue;
		}
		if (buildTrackFromEvent(i, nModule)) {
			if (nTracks < 1000 || nTotStubs < 6) {
				t->experimentalSetup->stationMap[0].dumpStubsToJson(StubsJson, i);
				currentTrack.dumpToJson(TrackJson);
			}
			nTracks++;
			for (int nstub = 0; nstub < t->experimentalSetup->stationMap[0].modules[nModule].getNStubs(); nstub++) {
				// std::cout << currentTrack.trackLine << "\n";
				// std::cout << t->experimentalSetup->stationMap[0].modules[nModule].getLineFromStub(nstub) << "\n";
				// std::cout << "Dist: " <<
				// sqrt(t->experimentalSetup->stationMap[0].modules[nModule].getLineFromStub(nstub).distance(currentTrack.trackLine))
				// << std::endl; std::cout << "-----" << std::endl; std::cout << "This is the current track: " <<
				// currentTrack.trackLine << "\n";

				// if(t->experimentalSetup->stationMap[0].modules[nModule].getLineFromStub(nstub).distance(currentTrack.trackLine)
				// < MaxModule[nModule] &&
				// t->experimentalSetup->stationMap[0].modules[nModule].getLineFromStub(nstub).distance(currentTrack.trackLine)
				// > MinModule[nModule]){
				if (t->experimentalSetup->stationMap[0].modules[nModule].getLineFromStub(nstub).distance(
						currentTrack.trackLine) < 100) {
					nCompatibleInModuleI++;
					break;
				}
				// getchar();
			}
		}
	}
	std::cout << "Efficiency for module: " << nModule << " is " << (double)nCompatibleInModuleI << " "
			  << (double)nTracks << std::endl;
	writeJson();
	return (double)nCompatibleInModuleI / (double)nTracks;
}

bool Tracker::buildTrackFromEvent(int eventNumber, int moduleToRemove, int nStation) {
	// std::cout << "Building track from event " << eventNumber << std::endl;
	t->loadEventNumber(eventNumber);
	if (moduleToRemove != -999)
		currentTrack = (t->experimentalSetup->stationMap[nStation]).buildTrackExceptModule(moduleToRemove);
	else
		currentTrack = (t->experimentalSetup->stationMap[nStation]).buildTrack();
	// std::cout << "currentTrack.minDistance: " << currentTrack.minDistance << std::endl;
	if (currentTrack.hasEnoughLines() && currentTrack.minDistance < 100 * 5 * 4) { // Get stuck here with 5 modules
		// getchar();
		return true;
	}
	return false;
}

bool Tracker::buildTrackFromCurrentEvent(int moduleToRemove, int nStation) {
	// std::cout << "Building track from event " << eventNumber << std::endl;
	if (moduleToRemove != -999)
		currentTrack = (experimentSetup.stationMap[nStation]).buildTrackExceptModule(moduleToRemove);
	else
		currentTrack = (experimentSetup.stationMap[nStation]).buildTrack();
	// std::cout << "currentTrack.minDistance: " << currentTrack.minDistance << std::endl;
	if (currentTrack.hasEnoughLines() && currentTrack.minDistance < 100 * 5 * 4) { // Get stuck here with 5 modules
		// getchar();
		return true;
	}
	return false;
}

void Tracker::getNStubsPerModule(int eventNumber) {
	for (int i = 0; i < eventNumber; i++) {
		t->loadEventNumber(i);
		for (int j = 0; j < 6; j++) {
			nStubsPerModule[j] += t->experimentalSetup->stationMap[0].modules[j].getNStubs();
		}
	}
}

void Tracker::writeJson() {
	std::ofstream o(newFileName + "_stubs.json");
	o << std::setw(4) << StubsJson << std::endl;
	std::ofstream o2(newFileName + "_tracks.json");
	o2 << std::setw(4) << TrackJson << std::endl;
}

void Tracker::saveStructureToXml(std::string xmlName) {
	// writeStationToXml(experimentSetup, xmlName);
}

void Tracker::buildAllTracks(int eventNumber, int stationId) {
	t->loadEventNumber(eventNumber);
	buildAllTracksFromCurrentLoadedEvent(stationId);
}

std::vector<Track> Tracker::buildAllTracksFromCurrentLoadedEvent(int stationId) {
	allTracks.clear();
	allTracksGood.clear();
	auto tmpStubs = experimentSetup.cartesianProduct(stationId);

	for (auto &stub : tmpStubs) {
		if (stub.size() < 4)
			return allTracksGood;
	}
	std::cout << "Well, we actually have enough stubs on station: " << stationId << std::endl;
	for (auto &stubsCombination : tmpStubs) {
		Track tmpTrack = experimentSetup.stationMap[stationId].buildTrackFromStubs(stubsCombination);
		if (tmpTrack.minDistance < 100 * 5 * 5) {
			allTracks.push_back(tmpTrack);
		}
	}

	std::vector<bool> trackToRemove = std::vector<bool>(allTracks.size(), false);
	for (size_t i = 0; i < allTracks.size(); i++) {
		if (trackToRemove[i])
			continue;
		for (size_t j = i + 1; j < allTracks.size(); j++) {
			if (trackToRemove[j]) {
				continue;
			}
			if (allTracks[i].sharesHitWith(allTracks[j])) {
				if (allTracks[i].minDistance < allTracks[j].minDistance) {
					trackToRemove[j] = true;
				} else {
					trackToRemove[i] = false;
				}
			}
		}
	}
	for (size_t i = 0; i < allTracks.size(); i++) {
		if (!trackToRemove[i]) {
			allTracksGood.push_back(allTracks[i]);
		}
	}
	return allTracksGood;
}

void Tracker::setSaveAlignmentHist(bool val) {
	saveAlignmentHist = val;
}

