import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5285332_5295332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5265332_5275332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5275332_5285332.root",
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("/muonedqm/muetrackreco/test/efficiency_vsTime_updated/run2/" + filename.replace(".root", "_efficiencyvsTime"))

xmlFile = "/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml"

command = "./build/apps/testEfficiencyVsTime"

def process_file(rootfile, output_file):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() -1) as executor:
    for rootfile, output_file in zip(rootfiles, output_files):
        executor.submit(process_file, rootfile, output_file)

