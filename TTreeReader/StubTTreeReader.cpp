#include "StubTTreeReader.hpp"


StubTTreeReader::StubTTreeReader() {}

StubTTreeReader::~StubTTreeReader() {
    delete eventTree;
    delete SuperID;
    delete Bx;
    delete Link;
    delete Bend;
    delete LocalX;
    delete LocalY;
}

void StubTTreeReader::parseTree(std::string filename) {
    TFile * f = new TFile(filename.c_str(), "READ");
    eventTree = (TTree*) f->Get("cbmsim");
    eventTree->SetBranchAddress("SuperID", &SuperID);
	eventTree->SetBranchAddress("Bx",      &Bx);
	eventTree->SetBranchAddress("Link",    &Link);
	eventTree->SetBranchAddress("Bend",    &Bend);
	eventTree->SetBranchAddress("LocalX",  &LocalX);
	eventTree->SetBranchAddress("LocalY",  &LocalY);
}

Event* StubTTreeReader::getEvent(int nEv) {
    eventTree->GetEntry(nEv);
    Stub tmpStub;
    currentEvent->clear();
    for(int i=0; i<SuperID->size(); i++) {
        tmpStub.superID = SuperID->at(i);
        tmpStub.bend = Bend->at(i);
        tmpStub.bx = Bx->at(i);
        tmpStub.link = Link->at(i);
        tmpStub.localX = LocalX->at(i);
        tmpStub.localY = LocalY->at(i);
        currentEvent->pushBack(tmpStub);
    }
    return currentEvent;
}

Event* StubTTreeReader::getNextEvent() {
    currentEvNumber++;
    eventTree->GetEntry(currentEvNumber);
    Stub tmpStub;
    currentEvent->clear();
    for(int i=0; i<SuperID->size(); i++) {
        tmpStub.superID = SuperID->at(i);
        tmpStub.bend = Bend->at(i);
        tmpStub.bx = Bx->at(i);
        tmpStub.link = Link->at(i);
        tmpStub.localX = LocalX->at(i);
        tmpStub.localY = LocalY->at(i);
        currentEvent->pushBack(tmpStub);
    }
    return currentEvent;
}



