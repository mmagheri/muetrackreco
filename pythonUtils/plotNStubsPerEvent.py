import pandas as pd
import plotly.graph_objects as go

# Replace with the actual path to your CSV file
csv_file_path = '/muonedqm/muetrackreco/nStubsPerModule_run_7_1405206_1415206_s2.csv'

# Reading the data from the CSV file
df = pd.read_csv(csv_file_path)

# Extracting the data and labels
data = df.iloc[0].values
labels = df.columns

total_events = sum(data)
normalized_data = [x / total_events for x in data]

# Creating the bar plot
fig = go.Figure([go.Bar(x=labels, y=normalized_data)])
fig.update_layout(
    title="",
    xaxis_title="Category",
    yaxis_title="Fraction of events",
    #template="plotly_dark"
)

# Showing the plot

fig.write_image("nStubsPerModule_run_7_1405206_1415206_s1.png")
