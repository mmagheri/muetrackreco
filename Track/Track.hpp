#pragma once
#include "Stub.hpp"
#include "Plane.hpp"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"
#include "TMatrixD.h"

#include "nlohmann/json.hpp"
#include <fstream>
#include <iomanip>
const double Z0 = 0.0*1e4; //um
const double Z1 = (101.2 + 93.0)*1e4; //um

struct SumDistance2 {
    std::vector<Line> lines;
    std::vector<double> resolutions;
    SumDistance2(std::vector<Line> lines_) : lines(lines_) {};
    SumDistance2(std::vector<Line> lines_, std::vector<double> resolutions_) : lines(lines_), resolutions(resolutions_) {};
    
    void setResolutions(std::vector<double> resolutions_){
        resolutions = resolutions_;
    }
    // implementation of the function to be minimized
    double operator() (const double *xx) {
        Point p = Point(xx[0], xx[1], Z0);
        Point p2 = Point(xx[2], xx[3], Z1);
        double distance = 0;
        Line lToMinimize(p, p2);
        for(size_t i = 0; i < lines.size(); i++){
            resolutions.push_back(30);
            distance += pow((lines[i].distance(lToMinimize)), 2) / pow(resolutions[i], 2);
        }
        //for(auto l: lines){
        //    distance += pow((l.distance(lToMinimize)), 2) / 900; //TODO -> strange dependence from this parameter
        //}
        return distance;
    }
 
};


// Track class definition
class Track {
private:
public:
    int DUT = -1;  
    std::vector<Line> lines;
    std::vector<Stub> stubs;
    double minDistance = 999; //cm
    Line trackLine;

    double moduleResolutions[12] = {
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30
    }; //um

    double errorMatrix[4];
    TMatrixD covMatrix;

    // standard constructor
    Track();

    void addLine(Line line);
    void addStub(Stub stub);
    bool minimizeDistances();
    void print();
    bool hasEnoughLines();
    TMatrixD getCovMatrix();
    std::vector<double> getErrorsFromTrackCalculatedAtT(double t);

    void dumpToJson(nlohmann::json& existingJson);

    bool sharesHitWith(Track& otherTrack);
    bool sharesHitWith(std::vector<Track>& otherTrack);
    bool sharesMoreThanOneHitWith(Track& otherTrack);
	bool containsStub(Stub& s);

    std::vector<Stub> selectStubssFromLink (int link);
    void setModulesResolutions(double resolutions[12]);
    void printModulesResolutions();

    void clear();

    //overload == operator for tracks
    bool operator==(const Track& otherTrack) const;

    double findCorrespondingResolutionFromLine(Line& l);
};