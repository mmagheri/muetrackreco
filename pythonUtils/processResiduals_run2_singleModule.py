import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5285332_5295332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5265332_5275332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5275332_5285332.root",
]

output_files = []


print(output_files)
xmlFiles = [
"/muonedqm/muetrackreco/test/run_2/5265332_5275332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5285332_5295332_alignment_5.xml",
]

command = "./build/apps/calculateResolution"

def process_file(rootfile, output_file, xmlFile, nModule):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile, "-m", str(nModule)])

for i in range(0, 12):
    for rootfile in rootfiles:
        filename = os.path.basename(rootfile)
        output_files.append("muonedqm/muetrackreco/test/Resolution/run_2/mod"+ str(i)+ "/" + filename.replace(".root", "_resolution_start20_singleModuleUpdate_Module" + str(i)))

    with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() - 3) as executor:
        for rootfile, output_file, xmlFile in zip(rootfiles, output_files, xmlFiles):
            executor.submit(process_file, rootfile, output_file, xmlFile, i)

