#pragma once
#include "ExperimentalSetup.hpp"
#include "TrackerFromStubs.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include <vector>
#include <set>

class Vertexer {
  private:
	TrackerFromStubs* tracker;
	TTreeReader* treeReader;
    ExperimentalSetup* experimentalSetup;
	TH1F *vertexPosition, *vertexTrackS1Distance;
	TH2F *angleBetweenTracks;
	TH1F *chiSquareFirstStation, *chiSquareSecondStation; 
	TTree* outTree;
	double vertexPositionValue, vertexTrackS1DistanceValue, angleBetweenTracksMaxValue, angleBetweenTracksMinValue, chiSquareTk1S1, chiSquareTk1S2, chiSquareTk2S2;
  public:
	Vertexer();
	~Vertexer();
	void linkTracker(TrackerFromStubs* tracker_);
	void linkTTreeReader(TTreeReader* treeReader_);
    void linkStation(ExperimentalSetup* expSetup);

	void saveHistograms(std::string outputFile);
	void updateResolutionFromCsv(std::string fileName);
	void getVertex();
	void reinitTreeValues();
};