#pragma once
#include <stdio.h>
#include <iostream>
#include <map>
#include <vector>
#include <array>
#include <fstream>
#include "StationClass.hpp"
#include <sstream>

#include "rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"

const int NSTRIPS = 1016;
const double STRIP_PITCH = 0.009;                       //cm (90um)
const double FULL_DIMENSIONS_X = NSTRIPS*STRIP_PITCH;   //cm. full dimensions of a 2S module along the measured coordinate (9.144cm)
const double FULL_DIMENSIONS_Y = 10;                    //cm. full dimensions of a 2S module along strip axis (2 sets of strips, each set is 5cm long)

struct Module{
    std::string name;
    int linkId;
    int module_number;
    std::map<int, std::map<uint16_t, uint16_t>> LUTmap;
    float bendOffset;
    float ptWidth;
    double pos[3]; //xyz position
    double rot[3]; //xyz rotation
};

 struct Station{
    std::string name;
    int nModules;
    int ID;
    std::map<int, Module> modules; //key = module number
    double pos[3]; //xyz position
    double rot[3]; //xyz rotation
};

std::map<int, Station> getStationFromXml(std::string xmlName);//key = stationID

std::map<int, StationClass> getStationClassFromXml(std::string xmlName, bool considerBend = false);//key = stationID

std::map<int, int> getLinkIDMapFromXml(std::string xmlname);

std::string writeStationToXml(const StationClass& station, std::string outFileName = "newStructure.xml");