#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "Aligner.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>

#include <vector>
#include <algorithm>
#include <random>

void shuffleVector(std::vector<int>& vec) {
	// Obtain a random seed from the system clock
	std::random_device rd;
	// Seed the random number generator
	std::mt19937 gen(rd());
	// Shuffle the vector using the random number generator
	std::shuffle(vec.begin(), vec.end(), gen);
}

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/merged/run_2/5245626_5271354.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "../getResiduals";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "../Structure/MUonEStructure_TB2023_mueAlignment.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	auto aligner = std::make_unique<Aligner>();
	aligner->linkTracker(tracker);
	aligner->linkTTreeReader(treeReader);
	aligner->linkStation(&experimentSetup);
	aligner->initHistograms();
	aligner->setNTrackMax(10000);
	//std::vector<int> nums {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	std::vector<int> nums {0, 1, 2};//, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	for(auto & dut: nums){
		std::cout << "Filling DUT: " << dut << std::endl;
		aligner->fillHistograms(dut);
	}
	aligner->saveHistograms(outputFileName + ".root");

}
