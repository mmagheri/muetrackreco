#include "Aligner.hpp"

Aligner::~Aligner() {
	for (auto &histo : distanceFromInterceptedPointX) {
		delete histo;
	}
	for (auto &histo : distanceFromInterceptedPointY) {
		delete histo;
	}
	for (auto &histo : distanceFromInterceptedPointZ) {
		delete histo;
	}
	for (auto &histo : lineDistance) {
		delete histo;
	}
	for (auto &histo : sigmaTrackX) {
		delete histo;
	}
	for (auto &histo : sigmaTrackY) {
		delete histo;
	}
}

void Aligner::linkTracker(TrackerFromStubs* trackerFromStubs_){
	trackerFromStubs = trackerFromStubs_;
}

void Aligner::linkTTreeReader(TTreeReader* treeReader_){
	treeReader = treeReader_;
}

void Aligner::linkStation(ExperimentalSetup* expSetup){
	experimentalSetup = expSetup;
}

void Aligner::initHistograms() {
	for (int i = 0; i < experimentalSetup->getNModules(); i++) {
		distanceFromInterceptedPointX.push_back(
			new TH1F(("hDistanceFromInterceptedPointX_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointX_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 1000, -300, 300));
		distanceFromInterceptedPointY.push_back(
			new TH1F(("hDistanceFromInterceptedPointY_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointY_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 1000, -300, 300));
		distanceFromInterceptedPointZ.push_back(
			new TH1F(("hDistanceFromInterceptedPointZ_" + std::to_string(i)).c_str(),
					 ("hDistanceFromInterceptedPointZ_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
					 1000, -300, 300));
		lineDistance.push_back(new TH1F(("hLineDistance_" + std::to_string(i)).c_str(),
										("hLineDistance_" + std::to_string(i) + "; deltaLines [um]; entries").c_str(),
										1000, -300, 300));
		sigmaTrackX.push_back(new TH1F(("hSigmaTrackX_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackX_" + std::to_string(i) + "; sigmaTrack on X[um]; entries").c_str(),
									   1000, 0, 50));
		sigmaTrackY.push_back(new TH1F(("hSigmaTrackY_" + std::to_string(i)).c_str(),
									   ("hSigmaTrackY_" + std::to_string(i) + "; sigmaTrack on Y[um]; entries").c_str(),
									   1000, 0, 50));
	}
}

void Aligner::clearHistograms() {
	for (auto &histo : distanceFromInterceptedPointX) {
		histo->Reset();
	}
	for (auto &histo : distanceFromInterceptedPointY) {
		histo->Reset();
	}
	for (auto &histo : distanceFromInterceptedPointZ) {
		histo->Reset();
	}
	for (auto &histo : lineDistance) {
		histo->Reset();
	}
	for (auto &histo : sigmaTrackX) {
		histo->Reset();
	}
	for (auto &histo : sigmaTrackY) {
		histo->Reset();
	}
}

void Aligner::saveHistograms(std::string outputFileName) {
	TFile *newFile = new TFile(outputFileName.c_str(), "RECREATE");
	newFile->cd();
	if (distanceFromInterceptedPointX.size() == 0) {
		throw std::runtime_error("Histograms not initialized");
	}
	for (size_t i = 0; i < distanceFromInterceptedPointX.size(); i++) {
		distanceFromInterceptedPointX[i]->Write();
		distanceFromInterceptedPointY[i]->Write();
		distanceFromInterceptedPointZ[i]->Write();
		sigmaTrackX[i]->Write();
		sigmaTrackY[i]->Write();
		lineDistance[i]->Write();
	}
	newFile->Close();
}

void Aligner::fillHistograms(int dut) {
	nTracks = 0;
	for (int i = 0; i < treeReader->getEntries() && nTracks < nTracksMax; i++) {
		if (i % 100000 == 0 && i != 0) {
			std::cout << "nTracks " << nTracks << " FOR DUT: " << dut << std::endl;
			std::cout << "Event: " << i << " over " << treeReader->getEntries() << std::endl;
		}

		treeReader->loadEventNumber(i);
		if (!treeReader->hasOneStubPerModule()) {
			continue;
		}
		trackerFromStubs->clear();
		bool allModulesHaveStub = true;
		int totalStubs = 0;
		for (auto &stationPair : experimentalSetup->stationMap) { // Loop through all stations in stationMap
			StationClass &station = stationPair.second;
			for (auto &module : station.modules) {
				if(module.first == dut) { //skip the module we are aligning
					continue;
				} else {
					for (size_t nStub = 0; nStub < module.second.stubs.size(); nStub++) {
						auto stub = module.second.getStub(nStub);
						auto line = module.second.getLineFromStub(nStub);
						trackerFromStubs->pushStubAndLine(stub, line);
					}
					if (module.second.stubs.size() != 1) {
						allModulesHaveStub = false;
						break;
					}
				}
				totalStubs += module.second.stubs.size();
			}
			//nice, now we have pushed all the other stubs in the tracker, we should be able to make a track
		}
		trackerFromStubs->buildTrackFromCurrentLines();
		//std::cout << trackerFromStubs->getCurrentTrack().minDistance << std::endl;
		if (trackerFromStubs->getCurrentTrack().minDistance < 5*50*50*11 && allModulesHaveStub) {
		//if (allModulesHaveStub) {
			//std::cout << "Here!	" << std::endl;
			Line stubLine = experimentalSetup->stationMap[int(dut/6)].modules[dut].getLineFromStub(0);
			if (fabs(stubLine.distance(trackerFromStubs->getCurrentTrack().trackLine)) > 10000) {
				continue;
			} else {
				Point pointInterceptAtDUT = experimentalSetup->stationMap[int(dut/6)].modules[dut].intersect(trackerFromStubs->getCurrentTrack().trackLine);
				double t = trackerFromStubs->getCurrentTrack().trackLine.getT(pointInterceptAtDUT);
				sigmaTrackX[dut]->Fill(trackerFromStubs->getCurrentTrack().getErrorsFromTrackCalculatedAtT(t)[0]);
				sigmaTrackY[dut]->Fill(trackerFromStubs->getCurrentTrack().getErrorsFromTrackCalculatedAtT(t)[1]);
				distanceFromInterceptedPointX[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).x);
				distanceFromInterceptedPointY[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).y);
				distanceFromInterceptedPointZ[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).z);
				lineDistance[dut]->Fill(stubLine.signedDistance(trackerFromStubs->getCurrentTrack().trackLine));
				nTracks++;
			}
		}
	}

	//saveHistograms("aligner.root");
}


void Aligner::fillHistograms_randomEntries(int dut) {
		nTracks = 0;
		std::set<int> selectedEntries;
		while(nTracks < nTracksMax) {
			int i = rand() % treeReader->getEntries();
			while (selectedEntries.count(i) > 0) {
				i = rand() % treeReader->getEntries();
			}

			selectedEntries.insert(i);

			treeReader->loadEventNumber(i);
			if (!treeReader->hasOneStubPerModule()) {
				continue;
			}
			trackerFromStubs->clear();

			int totalStubs = 0;
			for (auto &stationPair : experimentalSetup->stationMap) { // Loop through all stations in stationMap
				StationClass &station = stationPair.second;
				for (auto &module : station.modules) {
					if(module.first == dut) { //skip the module we are aligning
						continue;
					} else {
						for (size_t nStub = 0; nStub < module.second.stubs.size(); nStub++) {
							auto stub = module.second.getStub(nStub);
							auto line = module.second.getLineFromStub(nStub);
							trackerFromStubs->pushStubAndLine(stub, line);
						}
					}
					totalStubs += module.second.stubs.size();
					
				}
				//nice, now we have pushed all the other stubs in the tracker, we should be able to make a track
			}
			trackerFromStubs->buildTrackFromCurrentLines();

			if (sqrt(trackerFromStubs->getCurrentTrack().minDistance) < 500 * 11) {
				Line stubLine = experimentalSetup->stationMap[int(dut/6)].modules[dut].getLineFromStub(0);
				if (fabs(stubLine.distance(trackerFromStubs->getCurrentTrack().trackLine)) > 10000) {
					continue;
				} else {
					Point pointInterceptAtDUT = experimentalSetup->stationMap[int(dut/6)].modules[dut].intersect(trackerFromStubs->getCurrentTrack().trackLine);
					double t = trackerFromStubs->getCurrentTrack().trackLine.getT(pointInterceptAtDUT);
					sigmaTrackX[dut]->Fill(trackerFromStubs->getCurrentTrack().getErrorsFromTrackCalculatedAtT(t)[0]);
					sigmaTrackY[dut]->Fill(trackerFromStubs->getCurrentTrack().getErrorsFromTrackCalculatedAtT(t)[1]);
					distanceFromInterceptedPointX[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).x);
					distanceFromInterceptedPointY[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).y);
					distanceFromInterceptedPointZ[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).z);
					lineDistance[dut]->Fill(stubLine.distance(trackerFromStubs->getCurrentTrack().trackLine));

					nTracks++;
					if (nTracks % 10 == 0) {
						std::cout << "\rnTracks: " << nTracks << std::flush;
					}
					
				}
			}
		}
	std::cout << std::endl;
	saveHistograms("aligner.root");
}



double Aligner::scanModuleTranslation(int mod, int axis) {
    clearHistograms();
    fillHistograms(mod);

    double x = distanceFromInterceptedPointX[mod]->GetMean();
    double y = distanceFromInterceptedPointY[mod]->GetMean();
    double z = distanceFromInterceptedPointZ[mod]->GetMean();

    double meanPos[3]{x, y, z};
    double meanPositionOnAxis = meanPos[axis];

    double movementPosition = experimentalSetup->stationMap[int(mod/6)].modules[mod].pos[axis] += meanPositionOnAxis/(10000);
    std::cout << "Moving module to " << movementPosition << " cm on axis " << axis << std::endl;
    clearHistograms();
    return meanPositionOnAxis;
}


double Aligner::scanModuleRotation(int mod, int axis) {


	//get rotation of module around axis
	double nominalRotationAroundAxis = experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis];
	//calculate all points of rotation, from nominal +- 0.1*nominal, in steps of 0.01*nominal
	std::cout << "Nominal rotation: " << nominalRotationAroundAxis  << std::endl;
	std::vector<double> rotations;
	std::vector<double> chi2s;
	double minToStartWith = nominalRotationAroundAxis - 0.01;
	double maxToStartWith = nominalRotationAroundAxis + 0.01;

	for(double i = minToStartWith; i < maxToStartWith; i += 0.005) {
		rotations.push_back(i);
	}
	std::cout << "We're gonna scan " << rotations.size() << " rotations" << std::endl;
	clearHistograms();
	//for each rotation, calculate the track parameters
	for(auto& rot: rotations) {
		//std::cout << "Calculating rotation: " << rot << std::endl;
		experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rot;
		fillHistograms(mod);
		//fit distanceFromInterceptedPointX with a gaussian
		auto& histoToConsider = (axis == 0 ? distanceFromInterceptedPointY[mod] : distanceFromInterceptedPointX[mod]); //rotation around axis X -> look at Y, rotation around axis Y -> look at X
		if(axis == 2) {
			histoToConsider = lineDistance[mod];
		}
		TF1 *fit = new TF1("fit", "gaus", histoToConsider->GetMean() - 2*histoToConsider->GetRMS(), histoToConsider->GetMean() + 2*histoToConsider->GetRMS());
		histoToConsider->Fit(fit, "Q");
		//std::cout << "Chi2 of the fit: " << fit->GetChisquare() << " on axis " << axis <<  std::endl;
		chi2s.push_back(fit->GetChisquare());
		//do the same for y
		clearHistograms();
	}
	//std::cout << "All rotation: " << std::endl;
	//find the minimum chi2
	double minChi2 = 1000000;
	int minChi2Index = 0;
	for(size_t i = 0; i < chi2s.size(); i++) {
		if(chi2s[i] < minChi2) {
			minChi2 = chi2s[i];
			minChi2Index = i;
		}
	}
	// rotate the station to the minimum chi2
	experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rotations[minChi2Index];
	std::cout << "Rotating module to " << rotations[minChi2Index] << " rad around axis " << axis << std::endl;
	return 1;
}

double Aligner::scanModuleRotationOnX(int mod, int axis) {

	//get rotation of module around axis
	double nominalRotationAroundAxis = experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis];
	//calculate all points of rotation, from nominal +- 0.1*nominal, in steps of 0.01*nominal
	std::cout << "Nominal rotation: " << nominalRotationAroundAxis  << std::endl;
	std::vector<double> rotations;
	std::vector<double> chi2s;
	double minToStartWith = nominalRotationAroundAxis - 0.001;
	double maxToStartWith = nominalRotationAroundAxis + 0.001;
	double step = (maxToStartWith - minToStartWith)/10; 

	for(double i = minToStartWith; i < maxToStartWith; i += step) {
		rotations.push_back(i);
	}
	std::cout << "Attempting: " << rotations.size() << " rotations" << std::endl;

	clearHistograms();
	//for each rotation, calculate the track parameters
	for(auto& rot: rotations) {
		//std::cout << "Calculating rotation: " << rot << std::endl;
		experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rot;
		fillHistograms(mod);
		//fit distanceFromInterceptedPointX with a gaussian
		auto& histoToConsider = distanceFromInterceptedPointX[mod]; 
		TF1 *fit = new TF1("fit", "gaus", histoToConsider->GetMean() - 2*histoToConsider->GetRMS(), histoToConsider->GetMean() + 2*histoToConsider->GetRMS());
		histoToConsider->Fit(fit, "Q");
		//std::cout << "Chi2 of the fit: " << fit->GetChisquare() << " on axis " << axis <<  std::endl;
		chi2s.push_back(fit->GetChisquare());
		//do the same for y
		clearHistograms();
	}
	//std::cout << "All rotation: " << std::endl;
	//find the minimum chi2
	double minChi2 = 1000000;
	int minChi2Index = 0;
	for(size_t i = 0; i < chi2s.size(); i++) {
		if(chi2s[i] < minChi2) {
			minChi2 = chi2s[i];
			minChi2Index = i;
		}
	}
	// rotate the station to the minimum chi2
	experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rotations[minChi2Index];
	std::cout << "Rotating module to " << rotations[minChi2Index] << " rad around axis " << axis << std::endl;
	return 1;
}



double Aligner::scanModuleRotationOnY(int mod, int axis) {

	//get rotation of module around axis
	double nominalRotationAroundAxis = experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis];
	//calculate all points of rotation, from nominal +- 0.1*nominal, in steps of 0.01*nominal
	std::cout << "Nominal rotation: " << nominalRotationAroundAxis  << std::endl;
	std::vector<double> rotations;
	std::vector<double> chi2s;
	double minToStartWith = nominalRotationAroundAxis - 0.001;
	double maxToStartWith = nominalRotationAroundAxis + 0.001;
	double step = (maxToStartWith - minToStartWith)/10; 
	for(double i = minToStartWith; i < maxToStartWith; i += step) {
		rotations.push_back(i);
	}
	std::cout << "Attempting: " << rotations.size() << " rotations" << std::endl;
	clearHistograms();
	//for each rotation, calculate the track parameters
	for(auto& rot: rotations) {
		//std::cout << "Calculating rotation: " << rot << std::endl;
		experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rot;
		fillHistograms(mod);
		//fit distanceFromInterceptedPointX with a gaussian
		auto& histoToConsider = distanceFromInterceptedPointY[mod]; 
		TF1 *fit = new TF1("fit", "gaus", histoToConsider->GetMean() - 2*histoToConsider->GetRMS(), histoToConsider->GetMean() + 2*histoToConsider->GetRMS());
		histoToConsider->Fit(fit, "Q");
		//std::cout << "Chi2 of the fit: " << fit->GetChisquare() << " on axis " << axis <<  std::endl;
		chi2s.push_back(fit->GetChisquare());
		//do the same for y
		clearHistograms();
	}
	//std::cout << "All rotation: " << std::endl;
	//find the minimum chi2
	double minChi2 = 1000000;
	int minChi2Index = 0;
	for(size_t i = 0; i < chi2s.size(); i++) {
		if(chi2s[i] < minChi2) {
			minChi2 = chi2s[i];
			minChi2Index = i;
		}
	}
	// rotate the station to the minimum chi2
	experimentalSetup->stationMap[int(mod/6)].modules[mod].rot[axis] = rotations[minChi2Index];
	std::cout << "Rotating module to " << rotations[minChi2Index] << " rad around axis " << axis << std::endl;
	return 1;
}

void Aligner::setNTrackMax(int nTracksMax_) {
	nTracksMax = nTracksMax_;
}

void Aligner::setModulesResolutions(double moduleResolutions_[12]) {
	trackerFromStubs->setModulesResolutions(moduleResolutions_);
}

void Aligner::printModulesResolutions() {
	trackerFromStubs->printModulesResolutions();
}

void Aligner::getNTracksWithoutDUT(int dut) {
	tracks.clear();
	modules.clear();
	nTracks = 0;
	for (int i = 0; i < treeReader->getEntries() && nTracks < nTracksMax; i++) {
		if (i % 100000 == 0 && i != 0) {
			std::cout << "nTracks " << nTracks << " FOR DUT: " << dut << std::endl;
			std::cout << "Event: " << i << " over " << treeReader->getEntries() << std::endl;
		}

		treeReader->loadEventNumber(i);
		if (!treeReader->hasOneStubPerModule()) {
			continue;
		}
		trackerFromStubs->clear();
		bool allModulesHaveStub = true;
		int totalStubs = 0;
		for (auto &stationPair : experimentalSetup->stationMap) { // Loop through all stations in stationMap
			StationClass &station = stationPair.second;
			for (auto &module : station.modules) {
				if(module.first == dut) { //skip the module we are aligning
					continue;
				} else {
					for (size_t nStub = 0; nStub < module.second.stubs.size(); nStub++) {
						auto stub = module.second.getStub(nStub);
						auto line = module.second.getLineFromStub(nStub);
						trackerFromStubs->pushStubAndLine(stub, line);
					}
					if (module.second.stubs.size() != 1) {
						allModulesHaveStub = false;
						break;
					}
				}
				totalStubs += module.second.stubs.size();
			}
			//nice, now we have pushed all the other stubs in the tracker, we should be able to make a track
		}
		trackerFromStubs->buildTrackFromCurrentLines();
		//std::cout << trackerFromStubs->getCurrentTrack().minDistance << std::endl;
		if (allModulesHaveStub) {
		//if (allModulesHaveStub) {
			//std::cout << "Here!	" << std::endl;
			Line stubLine = experimentalSetup->stationMap[int(dut/6)].modules[dut].getLineFromStub(0);
			if (fabs(stubLine.distance(trackerFromStubs->getCurrentTrack().trackLine)) > 10000) {
				continue;
			} else {
				modules.push_back(experimentalSetup->stationMap[int(dut/6)].modules[dut]);
				tracks.push_back(trackerFromStubs->getCurrentTrack());
				nTracks++;
			}
		}
	}
	//saveHistograms("aligner.root");

}