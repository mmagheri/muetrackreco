import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5285332_5295332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5265332_5275332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5275332_5285332.root",
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("/muonedqm/muetrackreco/test/test_startingAt10/run2/" + filename.replace(".root", "_resolution_start20_noUpdateOnUV"))

print(output_files)
xmlFiles = [
"/muonedqm/muetrackreco/test/run_2/5265332_5275332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5285332_5295332_alignment_5.xml",
]


command = "./build/apps/calculateResolution"

def process_file(rootfile, output_file, xmlFile):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() - 3) as executor:
    for rootfile, output_file, xmlFile in zip(rootfiles, output_files, xmlFiles):
        executor.submit(process_file, rootfile, output_file, xmlFile)

