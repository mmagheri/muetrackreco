#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "Tracker.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"

int main(int argc, char *argv[]) {

	int TRACKDISTANCE = 100*5; // IN UM
	// bool allModules = cmdOptionExists(argv, argv+argc, "-a");
	bool considerBend = cmdOptionExists(argv, argv + argc, "-b");
	std::string inputFileName = "../3090_014e0f3e0_014e133df_4201232.root";
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "../efficiencyTest.json";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}
	std::string xmlFileName = "../Structure/MUonEStructure_TB2022_reissued.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}
	int nTracksMax = 10000;
	if (cmdOptionExists(argv, argv + argc, "-n")) {
		nTracksMax = std::stoi(getCmdOption(argv, argv + argc, "-n"));
	}

	if (cmdOptionExists(argv, argv + argc, "--maximumTrackDistance")) {
		TRACKDISTANCE = std::stoi(getCmdOption(argv, argv + argc, "--maximumTrackDistance"));
	}
	nlohmann::json jsonEfficiencies;

	TTreeReader t;
	t.parseTree(inputFileName.c_str());
	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), considerBend);
	t.setStation(&experimentSetup);
	experimentSetup.print();

	Tracker tracker(experimentSetup);
	tracker.initHistograms();
	std::vector<double> efficiencies;
	std::cout << "Getting efficiency: \n";
	int nModules = experimentSetup.getNModules();
	for (int dut = 0; dut < nModules; dut++) {
		int nTracks = 0;
		std::cout << "PROCESSING MODULE: " << dut << std::endl;
		int nCompatibleInModuleI = 0;
		for (int i = 0; i < t.getEntries() && nTracks < nTracksMax; i++) {
			if (i % 10000 == 0 && i != 0) {
				std::cout << "Event " << i << " nTracks: " << nTracks << std::endl;
			}

			t.loadEventNumber(i);

			if (!t.hasAtLeastNStubs(5)) {
				continue;
			}
			int nStation = dut/6; // TODO: get link - module - station from xml
			int nModuleInStation = dut;
			if (tracker.buildTrackFromCurrentEvent(dut, nStation)) {
				nTracks++;
				for (int nstub = 0;
					 nstub < tracker.experimentSetup.stationMap[nStation].modules[nModuleInStation].getNStubs();
					 nstub++) {
					if (tracker.experimentSetup.stationMap[nStation]
							.modules[nModuleInStation]
							.getLineFromStub(nstub)
							.distance(tracker.currentTrack.trackLine) < TRACKDISTANCE) {
						nCompatibleInModuleI++;
						break;
					}
				}
			}
		}
		std::cout << "Efficiency for module: " << dut << " is " << (double)nCompatibleInModuleI << " "
				  << (double)nTracks << std::endl;
		efficiencies.push_back((double)nCompatibleInModuleI / (double)nTracks);
	}

	std::cout << "Efficiencies: \n";
	for (int i = 0; i < nModules; i++) {
		std::cout << efficiencies[i] << std::endl;
		jsonEfficiencies["efficiency_module_" + std::to_string(i)] = efficiencies[i];
	}
	std::cout << "Writing output in: " << outputFileName << std::endl;
	std::ofstream jsonFile(outputFileName);
	jsonFile << jsonEfficiencies.dump(4);
	jsonFile.close();
	return 0;
}