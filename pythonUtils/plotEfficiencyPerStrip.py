import ROOT

# Open the ROOT file
f = ROOT.TFile("/muonedqm/muetrackreco/test/efficiency_vsTime_updated/run2/EffAllModules_run2.root", "READ")

# Number of modules/histograms
nModules = 12

# Create a canvas and divide it into 2x6 pads
canvas = ROOT.TCanvas("canvas", "Canvas for Histograms", 800*10, 1200*8)
canvas.SetLeftMargin(0.12)
canvas.SetRightMargin(0.12)
canvas.SetTopMargin(0.12)
canvas.SetBottomMargin(0.12)
canvas.Divide(3, 4)
ROOT.gStyle.SetOptStat(0)
# List to store the cloned histograms to prevent them from being garbage collected
clonedHistograms = []
# Loop over each module
for i in range(nModules):

    # Construct histogram names
    goodHitsName = f"hNGoodHitsPerStrip_{i}"
    totalHitsName = f"hNTotalHitsPerStrip_{i}"

    # Load histograms
    hGoodHits = f.Get(goodHitsName)
    hTotalHits = f.Get(totalHitsName)
    print(f"Good Hits Bins: {hGoodHits.GetNbinsX()}, Range: [{hGoodHits.GetXaxis().GetXmin()}, {hGoodHits.GetXaxis().GetXmax()}]")
    print(f"Total Hits Bins: {hTotalHits.GetNbinsX()}, Range: [{hTotalHits.GetXaxis().GetXmin()}, {hTotalHits.GetXaxis().GetXmax()}]")

    # Check if histograms are loaded
    if hGoodHits and hTotalHits:
        hGoodHits.Rebin(4)
        hTotalHits.Rebin(4)

        hGoodHits.Sumw2()
        hTotalHits.Sumw2()
        print(hGoodHits.GetNbinsX())
        print(hTotalHits.GetNbinsX())
        hEfficiency= ROOT.TGraphAsymmErrors()
        hEfficiency.Divide(hGoodHits, hTotalHits, "b(1,1)")
        # Create an efficiency histogram
        #hEfficiency = hGoodHits.Clone(f"hEfficiency_{i}")
        hEfficiency.SetTitle(f"Module {i}")
        hEfficiency.GetXaxis().SetTitle("Strip number")
        hEfficiency.GetYaxis().SetTitle("Efficiency")

        # Calculate the efficiency
        #hEfficiency.Divide(hGoodHits, hTotalHits, 1, 1, "B")
        hEfficiency.GetYaxis().SetRangeUser(0.9, 1.01)
        hEfficiency.GetXaxis().SetRangeUser(0, 1016)

        # Add to the list to prevent garbage collection
        clonedHistograms.append(hEfficiency)

        # Draw the histogram in its respective pad
        pad = canvas.cd(i + 1)
        pad.SetLeftMargin(0.15)
        pad.SetRightMargin(0.15)
        pad.SetTopMargin(0.15)
        pad.SetBottomMargin(0.15)
        
        hEfficiency.Draw()

    else:
        print(f"Histograms for module {i} not found in file")

# Draw the canvas
canvas.Draw()

# Save canvas to a PNG file
canvas.SaveAs("/muonedqm/muetrackreco/test/efficiency_vsTime_updated/run2/EffAllModules_run2.png")

# Close the ROOT file
f.Close()
