#include "Converter.hpp"
#include <gtest/gtest.h>


TEST(Stub, prova) {
  Stub s;
  s.superID = 10;
  s.bx = 11;
  s.bend = 12;
  s.localX = 13;
  s.localY = 0.75;
  s.meanModuleZ = 15;
  s.link = 0;
  Stub s2;
  s2.superID = 10;
  s2.bx = 11;
  s2.bend = 12;
  s2.localX = 13;
  s2.localY = 0.75;
  s2.meanModuleZ = 15;
  s2.link = 1;

  Converter* conv = new Converter();
  conv->readStructure("../Structure/MUonEStructure_TB2022.xml");
  std::cout << conv->getPhysicalX(s) << " " << conv->getPhysicalY(s) << "\n";
  std::vector<float> point = conv->getPointFromXY(s, s2);
  std::cout << conv->getPhysicalX(s2) << " " << conv->getPhysicalY(s2) << "\n";
  std::cout << point[0] << " " << point[1] << " " << point[2] << "\n";
}