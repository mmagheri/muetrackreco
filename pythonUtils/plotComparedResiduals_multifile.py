import ROOT
import os
import math
import matplotlib.pyplot as plt
import sys



# Paths to the root files and x_or_y choice
root_file_paths = [
    "/muonedqm/muetrackreco/test/run_2/5265332_5275332_alignment_translation_5.root",
    "/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_translation_5.root",
    "/muonedqm/muetrackreco/test/run_2/5285332_5295332_alignment_translation_5.root",
    "/muonedqm/muetrackreco/test/run_7/1405206_1415206_alignment_translation_5.root",
    "/muonedqm/muetrackreco/test/run_7/1415206_1425206_alignment_translation_5.root",
    "/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_translation_5.root",
]
x_or_y = sys.argv[1]

# Validate x_or_y choice
if x_or_y not in ["X", "Y"]:
    print("Invalid choice. Please choose either 'X' or 'Y'.")
    sys.exit()

# Define modules based on the choice
xModules = ["0", "2", "3", "4", "6", "8", "9", "10"]
yModules = ["1", "2", "3", "5", "7", "8", "9", "11"]
modules = xModules if x_or_y == "X" else yModules

# Initialize lists for standard deviations and labels
StdDevs = [[] for _ in range(root_file_paths.__len__())]
labels = [
    "5265332_5275332",
    "5275332_5285332",
    "5285332_5295332",
    "1405206_1415206",
    "1415206_1425206",
    "1425206_1435206",
]

# Process each file
for i, file_path in enumerate(root_file_paths):
    root_file = ROOT.TFile(file_path)
    for module in modules:
        histo = root_file.Get("hDistanceFromInterceptedPoint" + x_or_y + "_" + module)
        histo.GetXaxis().SetRangeUser(-300, 300)
        std_dev = histo.GetStdDev()
        StdDevs[i].append(std_dev)

# Plotting
for i in range(6):
    plt.plot(modules, StdDevs[i], marker='o', linestyle='none', label=labels[i])

plt.xlabel('Module number')
plt.ylabel('Residual [um]')
plt.title(f'{x_or_y} Residual per module number')
plt.ylim(0, 70)  # Set the y range from 0 to 70
plt.grid(True)
plt.legend()
plt.savefig(x_or_y + 'ResidualComparison_update.png')
