#pragma once
#include "Point.hpp"
#include "Vector.hpp"

class Line {

public:

	Vector v1, v2; //v2 direzione della retta
	Point p1, p2;
	double resolutionOnAxis = 1/sqrt(12); //um
	std::vector<double> errors {30,0,0};
	Vector intercept, slope;
    enum xyz {x,y,z};
	
	//default constructor
	Line() : p1(0, 0, 0), p2(0, 0, 0) {}
	Line(Point p1, Point p2) : p1(p1), p2(p2) {
		v1 = Vector(p1);
		v2 = Vector(p2) - v1;
		calculateInterceptAndSlope();
	}

	void calculateInterceptAndSlope();

	//line at point t
	Point at(double z);

	//distance from point to line
	double distance(Point p);
	double distance(Line l);
	double signedDistance(Line l);

	double distanceX(Point p);
	double distanceY(Point p);
	double distanceZ(Point p);

	Vector getPerpendicularVector();
	Vector vectorDistance(Point m);

	//is parallel
	bool isParallel(Line l);

	void rotate(double angX, double angY, double angZ);
	void translateZ(double z);
	void translateX(double x);
	void translateY(double y);

	//< operator returning true if the line has p1.z less than another line
	bool operator<(const Line& l) const;
	bool operator==(const Line& l) const;

	double getT(Point p) ;
    double angle(Line& l);

	Point getPointAtT(double t);
	double getZatX(double x);
	Point closestPointWrtLine(Line otherLine);
	void print();

    friend std::ostream& operator<<(std::ostream& os, const Line& l) {
		os << "p1: " << l.p1 << " p2: " << l.p2;// << " V: " << l.v2;
		return os;
	}

};
