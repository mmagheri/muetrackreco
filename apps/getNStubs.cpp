#include "TTreeReader.hpp"
#include "Converter.hpp"
#include "xmlUtils.hpp"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "parser.hpp"
#include "Tracker.hpp"
#include <fstream>


std::vector<int> allModulesHaveOneStubExcept(std::map<int, int> nStubsPerModuleInS0)
{
    // Check if every module has one stub except one
    std::vector<int> moduleWithLessThanOneStub;
    for (const auto& entry : nStubsPerModuleInS0) {
        if (entry.second != 1) {
            moduleWithLessThanOneStub.push_back(entry.first);
        }
    }
    return moduleWithLessThanOneStub;
}

std::vector<int> allModulesHaveTwoStubsExcept(std::map<int, int> nStubsPerModuleInS0)
{
    // Check if every module has one stub except one
    std::vector<int> moduleWithLessThanOneStub;
    for (const auto& entry : nStubsPerModuleInS0) {
        if (entry.second != 2) {
            moduleWithLessThanOneStub.push_back(entry.first);
        }
    }
    return moduleWithLessThanOneStub;
}

//const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

    //bool allModules = cmdOptionExists(argv, argv+argc, "-a");
    bool considerBend = cmdOptionExists(argv, argv+argc, "-b");
    std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1405206_1415206.root";
    if(cmdOptionExists(argv, argv+argc, "-i")){
        inputFileName = getCmdOption(argv, argv + argc, "-i");
    }
    std::string csvFileName = "output.csv"; // Specify the CSV file name
    if (cmdOptionExists(argv, argv + argc, "-c")) {
        csvFileName = getCmdOption(argv, argv + argc, "-c");
    }

    std::string xmlFileName = "/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_5.xml";
    if(cmdOptionExists(argv, argv+argc, "-x")){
        xmlFileName = getCmdOption(argv, argv + argc, "-x");
    }

    bool dumpToJson = cmdOptionExists(argv, argv+argc, "-j");
    TTreeReader t;
    t.parseTree(inputFileName.c_str());
    ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), considerBend);
    t.setStation(&experimentSetup);
    experimentSetup.print();
    //TFile* newFile = new TFile((outputFileName + ".root").c_str(), "RECREATE");
    int binsStubsInModule[4] {0, 0, 0, 0};
    int binsStubsInModule_s2[4] {0, 0, 0, 0};
    //bin 0: tracks with all module 1, one module with less than 1
    //bin 1: tracks with all module 1
    //bin 2: tracks with all module 1 except one  > 1
    //other

    
    for(int i=0; i< t.getEntries(); i++){
        if(i%100000==0) std::cout << "Event " << i << " over " << t.getEntries() << " : " << i*1.0/t.getEntries()*100 << "%"<< std::endl;
        
        t.loadEventNumber(i);
        //std::cout << "We have: " << t.getNStubsInside() << " stubs" << std::endl;
        std::map<int, int> nStubsPerModuleInS0 = t.getNStubsPerModuleInStation(0);
        std::map<int, int> nStubsPerModuleInS1 = t.getNStubsPerModuleInStation(1);
        

        auto moduleWithDiffThanOneStub = allModulesHaveOneStubExcept(nStubsPerModuleInS0);
        if(moduleWithDiffThanOneStub.size() >= 1){
            bool allModulesHaveLessThanOneStub = true;
            bool allModulesHaveMoreThanOneStub = true;
            for(auto & mNumb : moduleWithDiffThanOneStub) {
                if(nStubsPerModuleInS0[mNumb] > 1) {
                    allModulesHaveLessThanOneStub = false;
                } else if(nStubsPerModuleInS0[mNumb] == 0) {
                    allModulesHaveMoreThanOneStub = false;
                }
            }
            if(allModulesHaveLessThanOneStub){
                binsStubsInModule[0]++;
            } else if(allModulesHaveMoreThanOneStub){
                binsStubsInModule[2]++;
            } else {
                binsStubsInModule[3]++;
            }
        } else if(moduleWithDiffThanOneStub.size() == 0) {
            binsStubsInModule[1]++;
        } else {
            throw std::runtime_error("This should not happen");
        }

        auto moduleWithDiffThanOneStubS1 = allModulesHaveTwoStubsExcept(nStubsPerModuleInS1);
        if(moduleWithDiffThanOneStubS1.size() >= 1){
            bool allModulesHaveLessThanTwoStubsS1 = true;
            bool allModulesHaveMoreThanTwoStubsS1 = true;
            for(auto & mNumb : moduleWithDiffThanOneStubS1) {
                if(nStubsPerModuleInS1[mNumb] > 2) {
                    allModulesHaveLessThanTwoStubsS1 = false;
                } else {
                    allModulesHaveMoreThanTwoStubsS1 = false;
                }
            }
            if(allModulesHaveLessThanTwoStubsS1){
                binsStubsInModule_s2[0]++;
            } else if(allModulesHaveMoreThanTwoStubsS1){
                binsStubsInModule_s2[2]++;
            } else {
                binsStubsInModule_s2[3]++;
            }
        } else if(moduleWithDiffThanOneStubS1.size() == 0) {
            binsStubsInModule_s2[1]++;
        } else {
            throw std::runtime_error("This should not happen");
        }
    }
    std::ofstream outputFile(csvFileName + "_s0.csv");
    if (outputFile.is_open()) {
        outputFile << "At least 1 < 1 stub, every with one stub, at least one > 1 stub, Other" << std::endl;
        outputFile << binsStubsInModule[0] << "," << binsStubsInModule[1] << "," << binsStubsInModule[2] << "," << binsStubsInModule[3] << std::endl;
        std::cout << "Data written to " << csvFileName << std::endl;
    } else {
        std::cout << "Unable to open file: " << csvFileName << std::endl;
    }
    std::ofstream outputFile_s2(csvFileName + "_s2.csv");
    if (outputFile_s2.is_open()) {
        outputFile_s2 << "At least 1 < 2, every with 2 stub, at least one > 2 stub, Other" << std::endl;
        outputFile_s2 << binsStubsInModule_s2[0] << "," << binsStubsInModule_s2[1] << "," << binsStubsInModule_s2[2] << "," << binsStubsInModule_s2[3] << std::endl;
        std::cout << "Data written to " << csvFileName << std::endl;
    } else {
        std::cout << "Unable to open file: " << csvFileName << std::endl;
    }
}
        