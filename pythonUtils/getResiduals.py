import os
import subprocess
from concurrent.futures import ThreadPoolExecutor

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/4422298_4448026.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/4036363_4062091.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/5245626_5271354.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/2827100_2852828.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/2569810_2595538.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/4782504_4808232.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/3779073_3804801.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/3573241_3598969.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/4525214_4550942.root",
"/eos/experiment/mu-e/staging/daq/2023/merged/run_2/4756775_4782503.root"
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("results/run_2/residuals/" + filename.replace(".root", "_residuals.root"))

xmlFile = "/muonedqm/muetrackreco/results/betterTest_12Nov_goodDimension.xml"

command = "./build/apps/getResiduals"

def process_file(rootfile, output_file):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor() as executor:
    for rootfile, output_file in zip(rootfiles, output_files):
        executor.submit(process_file, rootfile, output_file)

