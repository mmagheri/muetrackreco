#include "ExperimentalSetup.hpp"
#include "xmlUtils.hpp"
#include <gtest/gtest.h>


TEST(Struct, readout) {

    // Step 1: Create some Line objects
    Line line2(Point(1000, 0, 0), Point(1000, 1000, 0));
    Line line1(Point(0, 1000, 0), Point(1000, 1000, 0));
    Line line3(Point(1000,0, 7000), Point(1000, 1000, 7000));
    Line line4(Point(0, 1000, 7000), Point(1000, 1000, 7000));
    Line line5(Point(500, 500, 8000), Point(1500, 1500, 8000));
    std::vector<Line> lines = {line1, line2, line3, line4, line5};

    // Step 2: Create a StationClass object
    StationClass station;

    // Step 3: Call buildTrackFromLines
    //Track track = station.buildTrackFromLines(lines);
    //std::cout << track.hasEnoughLines() << std::endl;
//
    //// Step 4: Check the result Track object
    //// Here you should add your own checks depending on what you expect the result to be
    //// For example, you could check the number of lines in the track:
    //EXPECT_EQ(track.lines.size(), lines.size());
    //std::cout << track.trackLine << std::endl;
}

TEST(Struct, multistubs) {
    ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_reissued.xml");
    
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 1, 0, 0, 0, 0, 0});

    auto result = experimentSetup.cartesianProduct(0);
    for(auto&comb : result) {
        std::cout << "Combination:\n";
        for(auto& stub : comb) {
            std::cout << "Module: " << stub.superID << " - stub n. " << stub.bx << "\n";
        }
        std::cout << std::endl;
    }
}

TEST(Struct, multiLines) {
    ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_reissued.xml");
    
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[0].pushStub(Stub{0, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[1].pushStub(Stub{1, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[2].pushStub(Stub{2, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[3].pushStub(Stub{3, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[4].pushStub(Stub{4, 1, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 0, 0, 0, 0, 0, 0});
    experimentSetup.stationMap[0].modules[5].pushStub(Stub{5, 1, 0, 0, 0, 0, 0});

    auto result = experimentSetup.getAllLineFromStubsCombinations(0);
    for(auto&comb : result) {
        std::cout << "Combination:\n";
        for(auto& line : comb) {
            std::cout << line << "\n";
        }
        std::cout << std::endl;
    }

}
