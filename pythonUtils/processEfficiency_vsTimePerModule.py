import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1405206_1415206.root",
]
modules = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("/muonedqm/muetrackreco/test/efficiency_alsoPerStrip_perModule/run7/" + filename.replace(".root", "_efficiencyvsTime"))

xmlFile = "/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml"

command = "./build/apps/testEfficiencyVsTime_perModule"

def process_file(rootfile, output_file, module):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile, "-m", module])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() -1) as executor:
    for module in modules:
        executor.submit(process_file, rootfiles[0], output_files[0], module)

