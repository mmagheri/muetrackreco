#include "Plane.hpp"
#include <vector>
#include <gtest/gtest.h>


TEST(Geometry, distances) {

  Point p(1,1,1);
  Point pp(0,1,0);
  Point pp2(0,2,0);
  Point pp3(1,1,0);
  Point pp4(1,2,0);
  Point p2(2,2,2);
  Point p3(3,3,2);
  Point p4(4,4,2);
  Vector vp(p);
  Vector vp2(p2);

  Line l(p, p2);
  EXPECT_TRUE(l.distance(p) == 0);
  EXPECT_TRUE(l.distance(p2) == 0);
  EXPECT_TRUE(l.distance(p3) != 0);
  Line l2(p, p3);
  Line l3(pp, pp2);
  Line l6(pp3, pp4);
  EXPECT_TRUE(l2.distance(l) == 0);
  EXPECT_TRUE(l3.distance(l6) == 1);
}



TEST(Geometry, rotation){
  Point p(1,1,1);
  p.rotate(0, 0.233, 0);
  EXPECT_TRUE(p.y == 1);
  EXPECT_TRUE(fabs(p.x - 1.2) < 0.1);
  EXPECT_TRUE(fabs(p.z - 0.74) < 0.1);
  std::cout << p << std::endl;
  p=Point(1,1,1);
  p.rotate(0.233, 0, 0);
  EXPECT_TRUE(p.x == 1);
  EXPECT_TRUE(fabs(p.z - 1.2) < 0.1);
  EXPECT_TRUE(fabs(p.y - 0.74) < 0.1);
  std::cout << p << std::endl;
  p=Point(1,1,1);
  p.rotate(0,0,0.233);
  EXPECT_TRUE(p.z == 1);
  EXPECT_TRUE(fabs(p.y - 1.2) < 0.1);
  EXPECT_TRUE(fabs(p.x - 0.74) < 0.1);
  std::cout << p << std::endl;

}