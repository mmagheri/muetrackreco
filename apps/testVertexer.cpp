#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "Vertexer.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/7225206_7235206.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "/muonedqm/muetrackreco/results/dgm_updated/run_7/vertexer.root";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "/muonedqm/muetrackreco/results/dgm_updated/run_7/test_2.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	auto vertexer = std::make_unique<Vertexer>();
	vertexer->linkTracker(tracker);
	vertexer->linkTTreeReader(treeReader);
	vertexer->linkStation(&experimentSetup);
	vertexer->updateResolutionFromCsv("/muonedqm/muetrackreco/module_mean_sem.csv");
	vertexer->getVertex();
	vertexer->saveHistograms(outputFileName);

}
