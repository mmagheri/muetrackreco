#include "Point.hpp"

void Point::rotate(double angX, double angY, double angZ) {
    Eigen::Quaterniond q = Eigen::AngleAxisd(angX, Eigen::Vector3d::UnitX())
                        * Eigen::AngleAxisd(angY, Eigen::Vector3d::UnitY())
                        * Eigen::AngleAxisd(angZ, Eigen::Vector3d::UnitZ());

    Eigen::Vector3d v(x, y, z);
    v = q * v;

    x = v.x();
    y = v.y();
    z = v.z();
}

void Point::print() {
    std::cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
}

void Point::translateX(double x) {
    this->x += x;
}

void Point::translateY(double y) {
    this->y += y;
}

void Point::translateZ(double z) {
    this->z += z;
}

Point Point::operator*(double s) {
    return Point(x * s, y * s, z * s);
}


Point Point::operator+(Point p) {
    return Point(x + p.x, y + p.y, z + p.z);
}

Point Point::operator/(double s) {
    return Point(x / s, y / s, z / s);
}

bool Point::operator==(const Point& p) const {
    return (x - p.x)< 0.0001 && (y - p.y) < 0.0001 && (z - p.z) < 0.0001;
}

bool Point::operator!=(const Point& p) const {
    return !(*this == p);
}

Point Point::operator-(Point p) {
    return Point(x - p.x, y - p.y, z - p.z);
}

void Point::rotateX(double angle) {
    double y1 = y * cos(angle) - z * sin(angle);
    double z1 = y * sin(angle) + z * cos(angle);
    y = y1;
    z = z1;
    //if one of the values is in fabs less than 1E07, set it to 0
    if (fabs(y) < 1E-07) y = 0;
    if (fabs(z) < 1E-07) z = 0;
}
void Point::rotateY(double angle) {
    double x1 = x * cos(angle) + z * sin(angle);
    double z1 = - x * sin(angle) + z * cos(angle);
    x = x1;
    z = z1;
    //if one of the values is in fabs less than 1E07, set it to 0
    if (fabs(x) < 1E-07) x = 0;
    if (fabs(z) < 1E-07) z = 0;
}
void Point::rotateZ(double angle) {
    double x1 = x * cos(angle) - y * sin(angle); 	
    double y1 = x * sin(angle) + y * cos(angle);
    x = x1;
    y = y1;
    //if one of the values is in fabs less than 1E07, set it to 0
    if (fabs(x) < 1E-07) x = 0;
    if (fabs(y) < 1E-07) y = 0;
}

