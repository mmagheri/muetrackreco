#include "Track.hpp"

Track::Track() {
    trackLine = Line(Point(0, 0, Z0), Point(0, 0, Z1));
    for(auto& e: errorMatrix) e = -999;
    covMatrix.ResizeTo(4, 4);
};

void Track::addLine(Line line){
    lines.push_back(line);
};

void Track::addStub(Stub stub){
    stubs.push_back(stub);
};

bool Track::minimizeDistances() {
    std::sort(lines.begin(), lines.end());
    std::vector<double> resolutions;
    for(auto& l:lines) {
        resolutions.push_back(20);
        //resolutions.push_back( findCorrespondingResolutionFromLine(l));
    }
    SumDistance2 sdist(lines, resolutions);
    ROOT::Math::Functor f(sdist, 4);
    ROOT::Math::Minimizer* min = ROOT::Math::Factory::CreateMinimizer("Minuit2", "");
    min->ProvidesError();
    double step[4] = {5, 5, 5, 5};
    double variable[4] = {(lines[0].p1.x + lines[0].p2.x)/2, (lines[1].p1.y + lines[0].p2.y), (lines[2].p1.x + lines[2].p2.x), (lines[3].p1.y + lines[3].p2.y)}; //TODO: optimize starting point - NECESSARY!
    min->SetMaxFunctionCalls(100000000); // for Minuit/Minuit2 
    min->SetMaxIterations(1000000000);  // for GSL 
    ///min->SetTolerance(1000 * 100 * 100 * 5);
    min->SetTolerance(1);
    min->SetPrintLevel(0);
    min->SetFunction(f);
    min->SetVariable(0,"x0",variable[0], step[0]);
    min->SetVariableLimits(0, -50000, 50000);
    min->SetVariable(1,"y0",variable[1], step[1]);
    min->SetVariableLimits(1, -50000, 50000);
    min->SetVariable(2,"x1",variable[2], step[2]);
    min->SetVariableLimits(2, -50000, 50000);
    min->SetVariable(3,"y1",variable[3], step[3]);
    min->SetVariableLimits(3, -50000, 50000);
    bool isOk = min->Minimize();
    const double *xs = min->X();
    minDistance = min->MinValue(); // it's now chi2
    trackLine = Line(Point(xs[0], xs[1], Z0), Point(xs[2], xs[3], Z1));
    min->GetCovMatrix(covMatrix.GetMatrixArray());
    delete min;
    return isOk;
}

void Track::print() {
    std::cout << "Printing track:\n";
    trackLine.print();
    for(auto & stub: stubs) {
        std::cout << "stub: " << stub.link << " " << stub.bend << " " << stub.localX << " " << stub.localY << "\n";
    }
    std::cout << "min distance: " << minDistance << "\n";
}

bool Track::hasEnoughLines() {
    return lines.size() >= 4;
}

TMatrixD Track::getCovMatrix() {
    return covMatrix;
}

std::vector<double> Track::getErrorsFromTrackCalculatedAtT(double t) {
    std::vector<double> errors {
        sqrt(pow(1-t, 2)*covMatrix[0][0] + pow(t,2)*covMatrix[2][2] -2*(1-t)*t*covMatrix[0][2]),
        sqrt(pow(1-t, 2)*covMatrix[1][1] + pow(t,2)*covMatrix[3][3] -2*(1-t)*t*covMatrix[1][3]),
        0
    };
    return errors;
}

void Track::dumpToJson(nlohmann::json& existingJson) {

    nlohmann::json newEvent;

    newEvent["tracks"]["intercept"] = {trackLine.intercept.x, trackLine.intercept.y, trackLine.intercept.z};
    newEvent["tracks"]["slope"] = {trackLine.slope.x, trackLine.slope.y, trackLine.slope.z};
    newEvent["tracks"]["p1"] = {trackLine.p1.x, trackLine.p1.y, trackLine.p1.z};
    newEvent["tracks"]["p2"] = {trackLine.p2.x, trackLine.p2.y, trackLine.p2.z};
    for(auto&s: stubs) {
        std::cout << "adding stub\n";
        newEvent["tracks"]["stubs"].push_back(
            nlohmann::json{
                {"bend", s.bend},
                {"localx", s.localX},
                {"localy", s.localY},
                {"link", s.link}
            }
        );
    }
    existingJson["events"].push_back(newEvent);
}

bool Track::sharesHitWith(Track& otherTrack) {
    for (auto& stub : stubs) {
        for (auto& otherStub : otherTrack.stubs) {
            if (stub.link == otherStub.link && stub.bend == otherStub.bend && stub.localX == otherStub.localX && stub.localY == otherStub.localY && stub.bx == otherStub.bx) {
                //std::cout << "found shared hit: " << stub.link << " " << stub.bend << " " << stub.localX << " " << stub.localY << std::endl;
                return true;
            }
        }
    }
    return false;
}

bool Track::sharesHitWith(std::vector<Track>& otherTrack) {
    for(auto & tk: otherTrack) {
        if(sharesHitWith(tk)) {
            return true;
        }
    }
    return false;
}

bool Track::sharesMoreThanOneHitWith(Track& otherTrack) {
    int nStubsShared = 0;
    for (auto& stub : stubs) {
        for (auto& otherStub : otherTrack.stubs) {
            if (stub.link == otherStub.link && stub.bend == otherStub.bend && stub.localX == otherStub.localX && stub.localY == otherStub.localY && stub.bx == otherStub.bx) {
                nStubsShared++;
                if(nStubsShared>1) return true;
            }
        }
    }
    return false;
}

void Track::clear(){
    lines.clear();
    stubs.clear();
    minDistance = 999;
    trackLine = Line(Point(0, 0, Z0), Point(0, 0, Z1));
}

//define == operator for Track

bool Track::operator==(const Track& otherTrack) const {
    if(stubs.size() == 0 || otherTrack.stubs.size() == 0) {
        throw std::runtime_error("Track::operator== Trying to compare tracks with 0 stubs");
    }
    if (stubs.size() != otherTrack.stubs.size()) {
        return false;
    }
    for (auto& stub : stubs) {
        bool found = false;
        for (auto& otherStub : otherTrack.stubs) {
            if (stub.link == otherStub.link && stub.bend == otherStub.bend && stub.localX == otherStub.localX && stub.localY == otherStub.localY) {
                found = true;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}

bool Track::containsStub(Stub& s) {
    for (auto& stub : stubs) {
        if (stub.link == s.link && stub.bend == s.bend && stub.localX == s.localX && stub.localY == s.localY) {
            return true;
        }
    }
    return false;
}

std::vector<Stub> Track::selectStubssFromLink (int link) {
    std::vector<Stub> stubsFromLink;
    for (auto& stub : stubs) {
        if (stub.link == link) {
            stubsFromLink.push_back(stub);
        }
    }
    return stubsFromLink;
}

double Track::findCorrespondingResolutionFromLine(Line& l) {
    std::vector<double> mean_z_positions = {
        178336.33, 
        215446.00, 
        553788.00, 
        566184.67, 
        897577.67, 
        934653.33, 
        1189176.67, 
        1227806.67, 
        1564823.33, 
        1578000.00, 
        1908190.00, 
        1946773.33
    };
    auto p = Point(0, 0, 0);
    double z = l.distance(p);

    int nModule = 999;
    double minDist = 999999;
    for(int i=0; i<12; i++) {
        if(fabs(z - mean_z_positions[i]) < minDist) {
            nModule = i;
            minDist = fabs(z - mean_z_positions[i]);
        }
    }
    if(nModule == 999) {
        std::cout << "z: " << z << std::endl;
        std::cout << "minDist: " << minDist << std::endl;
        std::cout << "nModule: " << nModule << std::endl;
        throw std::runtime_error("Track::findCorrespondingResolutionFromLine: module not found");
    }
    return moduleResolutions[nModule];
}

void Track::setModulesResolutions(double resolutions[12]) {
    for(int i=0; i<12; i++) {
        moduleResolutions[i] = resolutions[i];
    }
}

void Track::printModulesResolutions() {
    std::cout << "Track -----" << std::endl;
    std::cout << "Modules resolutions: " << std::endl;
    for(int i=0; i<12; i++) {
        std::cout << moduleResolutions[i] << " ";
    }
    std::cout << std::endl;
}