#pragma once
#include "ExperimentalSetup.hpp"
#include "TrackerFromStubs.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include <vector>
#include <set>
#include <random> // add this header for generating random numbers



class Aligner {
  private:
	TrackerFromStubs* trackerFromStubs;
	TTreeReader* treeReader;

	std::vector<TH1F *> distanceFromInterceptedPointX;
	std::vector<TH1F *> distanceFromInterceptedPointY;
	std::vector<TH1F *> distanceFromInterceptedPointZ;
	std::vector<TH1F *> lineDistance;
	std::vector<TH1F *> sigmaTrackX;
	std::vector<TH1F *> sigmaTrackY;
	//int dut = 0;
	int nTracks = 0;
	int nTracksMax = 10000;



  public:
    ExperimentalSetup* experimentalSetup;
  	std::vector<ModuleClass> modules;
	std::vector<Track> tracks;

	Aligner() {};
	~Aligner();
	void linkTracker(TrackerFromStubs* trackerFromStubs_);
	void linkTTreeReader(TTreeReader* treeReader_);
    void linkStation(ExperimentalSetup* expSetup);
	void setNTrackMax(int nTracksMax_);

	void initHistograms();
	void clearHistograms();
	void fillHistograms(int dut);
	void fillHistograms_randomEntries(int dut);
	void saveHistograms(std::string outputFileName);

	double scanModuleTranslation(int mod, int axis);
	double scanModuleRotation(int mod, int axis);
	double scanModuleRotationOnX(int mod, int axis);
	double scanModuleRotationOnY(int mod, int axis);

	void getNTracksWithoutDUT(int dut);
	void setModulesResolutions(double moduleResolutions_[12]);
	void printModulesResolutions();

};