def z_at_xy_zero(direction_vector, point_on_line):
    """
    Calculate the z-coordinate when x and y are zero on the line defined by a direction vector and a point.

    :param direction_vector: The direction vector of the line
    :param point_on_line: A point on the line
    :return: The z-coordinate when x, y = 0
    """
    # Unpack the direction vector and the point
    dx, dy, dz = direction_vector
    x0, y0, z0 = point_on_line

    # If the direction vector for x or y is zero, the line is parallel to the xz or yz plane, and z cannot be determined
    if dx == 0 and dy == 0:
        return None

    # Calculate the z-coordinate when x and y are zero
    t_x = -x0 / dx if dx != 0 else 0
    t_y = -y0 / dy if dy != 0 else 0
    t = max(t_x, t_y)
    
    return z0 + t * dz

# Using the previously calculated direction vector and point
direction_vector = (140.32, -34003.6, 227141)
point_on_line = (50139.9, -33809.6, 227093)
print(z_at_xy_zero(direction_vector, point_on_line))
