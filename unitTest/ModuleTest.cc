#include "Converter.hpp"
#include "ExperimentalSetup.hpp"
#include <gtest/gtest.h>


TEST(MTest, prova) {
  Stub s2;
  s2.superID = 10;
  s2.bx = 11;
  s2.bend = 12;
  s2.localX = 0;
  s2.localY = 0.75;
  s2.meanModuleZ = 15;
  s2.link = 1;

  ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_reissued.xml");
  experimentSetup.print();
  s2.localX = 0;
  for(int i=0; i<6; i++) {
    s2.link = i;
    std::cout << "LINK: " << s2.link << "\n";
    experimentSetup.stationMap[0].modules[s2.link].pushStub(s2);
    Line l3 = experimentSetup.stationMap[0].modules[s2.link].getLineFromStub(0);
    l3.print();
    std::cout << "............\n";
    //getchar();
  }

  //std::cout << "Expected physical Y: " << PhysY << "\n";
}


