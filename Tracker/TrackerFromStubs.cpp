#include "TrackerFromStubs.hpp"


TrackerFromStubs::~TrackerFromStubs() {
}

void TrackerFromStubs::moveModule(int mod, int axis, double distance) {
	experimentSetup.stationMap[0].modules[mod].pos[axis] += distance;
}


bool TrackerFromStubs::buildTrackFromCurrentLines() {
	// std::cout << "Building track from event " << eventNumber << std::endl;
	currentTrack.clear();
	for (auto &l : currentStubLines) {
		currentTrack.addLine(l.line);
	}

	if (currentTrack.hasEnoughLines()) {
		currentTrack.minimizeDistances();
		currentEventTracks.push_back(currentTrack);
		//std::cout << "hey, here's the track:" << std::endl;
		//currentTrack.print();
	}
	
	if(currentTrack.minDistance < MIN_DISTANCE_FOR_ACCEPTANCE){
		return true;
	} else {
		return false;
	}
}

void TrackerFromStubs::printLines() {
	std::cout << "-----" << std::endl;
	std::cout << "We have: " << currentStubLines.size() << " lines" << std::endl;
	for (auto &l : currentStubLines) {
		l.line.print();
		//l.stub.print();
		std::cout << std::endl;
	}
}

void TrackerFromStubs::pushStubAndLine(Stub s_, Line line_) {
	currentStubLines.push_back({s_, line_});
}

void TrackerFromStubs::clear() {
	currentStubLines.clear();
	currentEventTracks.clear();
}

Track TrackerFromStubs::getCurrentTrack() {
	return currentTrack;
}

std::vector<Track> TrackerFromStubs::buildAllTracksFromCurrentLoadedEventInStation(int stationId) {
	//get all stubs combinations in the station
	auto stubsCombinations = experimentSetup.cartesianProduct(stationId);
	//now get all the lines from the stubs
	std::vector<std::vector<Line>> linesCombinations;
	for (auto &stubCombination : stubsCombinations) {
		std::vector<Line> lines;
		for (auto &stub : stubCombination) {
			lines.push_back(experimentSetup.stationMap[stationId].modules[stub.link].getLineFromStub(stub));
		}
		linesCombinations.push_back(lines);
	}
	//now we have all the lines combinations, we can build the tracks
	std::vector<Track> tracks;
	for(size_t counter = 0; counter < linesCombinations.size(); counter++){
		Track t;
		for (auto &line : linesCombinations[counter]) {
			t.addLine(line);
		}
		for(auto & stub : stubsCombinations[counter]){
			t.addStub(stub);
		}
		t.minimizeDistances();
		tracks.push_back(t);
	}

	//now we have to remove tracks which share a hit
	std::vector<bool> tracksToBeRemoved(tracks.size(), false);
	//std::cout << "\nNTracks: " << tracks.size() << std::endl;

	//for (size_t i = 0; i < tracks.size() - 1; ++i) {
	//experimentSetup.print();
	//std::cout << "---" << std::endl;
	auto firstStubInModule0 = experimentSetup.stationMap[stationId].modules[6].stubs[0];
	auto secondStubInModule0 = experimentSetup.stationMap[stationId].modules[6].stubs[1];
	//std::cout << "first stub in mod 0 "<< std::endl;
	//std::cout << firstStubInModule0.link << " " << firstStubInModule0.bend << " " << firstStubInModule0.localX << " " << firstStubInModule0.localY << std::endl;
	std::vector<Track> tracksWithFirstStubInModule0, tracksWithSecondStubInModule0;
	for (size_t j = 0; j < tracks.size(); ++j) {
		if (tracks[j].containsStub(firstStubInModule0)) {
			tracksWithFirstStubInModule0.push_back(tracks[j]);
		}
		if(tracks[j].containsStub(secondStubInModule0)){
			tracksWithSecondStubInModule0.push_back(tracks[j]);
		}
	}

	//std::cout << "We have: " << tracksWithFirstStubInModule0.size() << " tracks with first stub in module 0" << std::endl;
	double minDist = 100000;
	double counterForTheTrack = 0;
	for(size_t i = 0; i<tracksWithFirstStubInModule0.size() ; i++) {
		if(tracksWithFirstStubInModule0[i].minDistance < minDist) {
			minDist = tracksWithFirstStubInModule0[i].minDistance;
			counterForTheTrack = i;
		}
	}
	double minDist2 = 100000;
	double counterForTheTrack2 = 0;
	for(size_t i = 0; i<tracksWithSecondStubInModule0.size(); i++){
		if(tracksWithSecondStubInModule0[i].sharesHitWith(tracksWithFirstStubInModule0[counterForTheTrack])){
			continue;
		}
		if(tracksWithSecondStubInModule0[i].minDistance < minDist2){
			minDist2 = tracksWithSecondStubInModule0[i].minDistance;
			counterForTheTrack2 = i;
		}
	}
	//std::cout << "The track with the minimum distance is: " << std::endl;
	//tracksWithFirstStubInModule0[counterForTheTrack].print();
	//std::cout << "The track with the minimum distance is wrt 2: " << std::endl;
	//tracksWithSecondStubInModule0[counterForTheTrack2].print();


	//}
	//TODO: select good tracksppi
	std::vector<Track> tracksToBeKept;
	//add only if the total sum of min distances is less than 5*100*6 - 5 times 100 (reasonable resolution) times 6 (number of modules)
	if(tracksWithFirstStubInModule0[counterForTheTrack].minDistance < MIN_DISTANCE_FOR_ACCEPTANCE) {
		tracksToBeKept.push_back(tracksWithFirstStubInModule0[counterForTheTrack]);
	}
	if(tracksWithSecondStubInModule0[counterForTheTrack2].minDistance < MIN_DISTANCE_FOR_ACCEPTANCE) {
		tracksToBeKept.push_back(tracksWithSecondStubInModule0[counterForTheTrack2]);
	}
	
	return tracksToBeKept;
}


std::vector<Track> TrackerFromStubs::buildSingleTrackFromCurrentLinesInStation(int stationId) {
	//get all stubs combinations in the station
	auto stubsCombinations = experimentSetup.cartesianProduct(stationId);
	//now get all the lines from the stubs
	std::vector<std::vector<Line>> linesCombinations;
	for (auto &stubCombination : stubsCombinations) {
		std::vector<Line> lines;
		for (auto &stub : stubCombination) {
			lines.push_back(experimentSetup.stationMap[stationId].modules[stub.link].getLineFromStub(stub));
		}
		linesCombinations.push_back(lines);
	}
	//now we have all the lines combinations, we can build the tracks
	std::vector<Track> tracks;
	for(size_t counter = 0; counter < linesCombinations.size(); counter++){
		Track t;
		for (auto &line : linesCombinations[counter]) {
			t.addLine(line);
		}
		for(auto & stub : stubsCombinations[counter]){
			t.addStub(stub);
		}
		t.minimizeDistances();
		tracks.push_back(t);
	}

	if(tracks.size() != 1){
		throw std::runtime_error("Number of tracks is not equal to 1");
	}

	if(tracks.at(0).minDistance < MIN_DISTANCE_FOR_ACCEPTANCE){
		return tracks;
	} else {
		return std::vector<Track>();
	}
}

void TrackerFromStubs::setModulesResolutions(double resolutions[12]) {
	//for (size_t i = 0; i < 12; i++) {
	//	moduleResolutions[i] = resolutions[i];
	//}
	currentTrack.setModulesResolutions(resolutions);
	currentTrack.printModulesResolutions();
}
void TrackerFromStubs::printModulesResolutions() {
	currentTrack.printModulesResolutions();
}


void TrackerFromStubs::updateResolutionFromFile(std::string fileName) {
	
	TFile *file = TFile::Open(fileName.c_str());
	if (!file || file->IsZombie()) {
		throw std::runtime_error("updateResolutionFromFile::updateResolutionFromFile - Could not open file: " + fileName);
	}
	int xModules[4]  =  {0, 4, 6, 10};
	int yModules[4]  =  {1, 5, 7, 11};
	int UVModules[4] =  {2, 3, 8, 9};

	std::string histoBaseName {"hDistanceFromInterceptedPoint"};
	std::string sigmaBaseName {"hSigmaTrack"};

	double tmpResolutions[12] = {
        999,
        999,
        999,
        999,
        999,
        999,
        999,
        999,
        999,
        999,
        999,
        999
    }; //um

	for(int i = 0; i<12; i++) {
		tmpResolutions[i] = currentTrack.moduleResolutions[i];
	}
	for(int i = 0; i < 12; i++) {
		if (std::find(std::begin(modulesToBeUpdated), std::end(modulesToBeUpdated), i) == std::end(modulesToBeUpdated)) {
			continue;
		}
		//get X resolution
		std::string histoName_X = histoBaseName + "X_" + std::to_string(i);
		std::string histoName_Y = histoBaseName + "Y_" + std::to_string(i);

		std::string sigmaName_X = sigmaBaseName + "X_" + std::to_string(i);
		std::string sigmaName_Y = sigmaBaseName + "Y_" + std::to_string(i);
		TH1F* histoX = (TH1F*)file->Get(histoName_X.c_str());
		TH1F* histoY = (TH1F*)file->Get(histoName_Y.c_str());
		TH1F* sigmaX = (TH1F*)file->Get(sigmaName_X.c_str());
		TH1F* sigmaY = (TH1F*)file->Get(sigmaName_Y.c_str());

		// fit histo with a gaussian and get mean and std deviation
		TF1 *fitX = new TF1("fit", "gaus", histoX->GetMean() - 2*histoX->GetRMS(), histoX->GetMean() + 2*histoX->GetRMS());
		TF1 *fitY = new TF1("fit", "gaus", histoY->GetMean() - 2*histoY->GetRMS(), histoY->GetMean() + 2*histoY->GetRMS());
		histoX->Fit(fitX, "Q");
		histoY->Fit(fitY, "Q");
		
		double residualX = fabs(fitX->GetParameter(2)) < 2  ? 1000 : fitX->GetParameter(2);
		double residualY = fabs(fitY->GetParameter(2)) < 2  ? 1000 : fitY->GetParameter(2);
		double sigmaTrackX = sigmaX->GetMean();
		double sigmaTrackY = sigmaY->GetMean();
		std::cout << "Module " << i << " has residualX " << residualX << " and residualY " << residualY << std::endl;
		std::cout << "Module " << i << " has sigmaTrackX " << sigmaTrackX << " and sigmaTrackY " << sigmaTrackY << std::endl;
		
		if (std::find(std::begin(xModules), std::end(xModules), i) != std::end(xModules)) {
			if(sigmaTrackX > residualX) {
				throw std::runtime_error("updateResolutionFromFile::updateResolutionFromFile - sigmaTrackX > residualX");
			}
			tmpResolutions[i] = sqrt(pow(residualX, 2) - pow(sigmaTrackX, 2));
			std::cout << "Module " << i << " has resolution " << tmpResolutions[i] << std::endl;
		}
		else if(std::find(std::begin(yModules), std::end(yModules), i) != std::end(yModules)) {
			if(sigmaTrackY > residualY) {
				throw std::runtime_error("updateResolutionFromFile::updateResolutionFromFile - sigmaTrackY > residualY");
			}
			tmpResolutions[i] = sqrt(pow(residualY, 2) - pow(sigmaTrackY, 2));
			std::cout << "Module " << i << " has resolution " << tmpResolutions[i] << std::endl;
		}
		else if(std::find(std::begin(UVModules), std::end(UVModules), i) != std::end(UVModules)) {
			if(sigmaTrackX + sigmaTrackY > residualX + residualY  || sigmaTrackY > residualY) {
				throw std::runtime_error("updateResolutionFromFile::updateResolutionFromFile - sigmaTrackX > residualX || sigmaTrackY > residualY");
			}
			double totRes = sqrt(pow(residualX, 2) + pow(residualY, 2));
			double totSigma = sqrt(pow(sigmaTrackX,2) + pow(sigmaTrackY, 2));
			tmpResolutions[i] = sqrt(totRes*totRes - totSigma*totSigma);
			std::cout << "Module " << i << " has resolution " << tmpResolutions[i] << std::endl;
			//tmpResolutions[i] = 30;
		}
		else {
			std::cout << "Module " << i << " not found" << std::endl;
		}
		//experimentalSetup->stationMap[int(i/6)].modules[i].setResolution(0, 0);
	}
	currentTrack.setModulesResolutions(tmpResolutions);
	currentTrack.printModulesResolutions();
	updateResolutionHistogram(iteration, tmpResolutions);
	saveHistograms();
	iteration++;
	file->Close();
}

void TrackerFromStubs::createHistograms() {
	for(int i = 0; i < 12; i++) {
		std::string name = "module_" + std::to_string(i);
		TGraph *g = new TGraph();
		g->SetName(name.c_str());
		modulesResolutionPerIteration.push_back(g);
	}
}

void TrackerFromStubs::updateResolutionHistogram(int iterarion, double resolutions[12]) {
	for(int i = 0; i < 12; i++) {
		modulesResolutionPerIteration[i]->SetPoint(iteration, iterarion, resolutions[i]);
	}
}

void TrackerFromStubs::saveHistograms() {
	TFile *file = new TFile(outputFileName.c_str(), "RECREATE");
	for(int i = 0; i < 12; i++) {
		modulesResolutionPerIteration[i]->Write();
	}
	file->Close();
}
