#include "Plane.hpp"

void Plane::print() {
    std::cout << "p1: ";
    p1.print();
    std::cout << "p2: ";
    p2.print();
    std::cout << "p3: ";
    p3.print();
}
//rotation methods
void Plane::rotate(double angX, double angY, double angZ){
    p1.rotate(angX, angY, angZ);
    p2.rotate(angX, angY, angZ);
    p3.rotate(angX, angY, angZ);
}

void Plane::translateX(double posX) {
    p1.translateX(posX);
    p2.translateX(posX);
    p3.translateX(posX);
}

void Plane::translateY(double posY) {
    p1.translateY(posY);
    p2.translateY(posY);
    p3.translateY(posY);
}

void Plane::translateZ(double posZ) {
    p1.translateZ(posZ);
    p2.translateZ(posZ);
    p3.translateZ(posZ);
}

bool Plane::operator==(const Plane& p) const {
    return  p1 == p.p1 && p2 == p.p2 && p3 == p.p3;
}

//distance from point to plane
double Plane::distance(Point q) {
    Point &p = this->p1;
    Vector PQ(p, q);
    Vector vpp2 = Vector(p, p2);
    Vector vpp3 = Vector(p, p3);
    Vector normal = vpp2 ^ vpp3;
    return (normal * PQ) / normal.length();
}

//intersect with a line
Point Plane::intersect(Line l) {
    Vector p01(p2, p1);
    Vector p02(p3, p1);
    Vector lab(l.p2, l.p1);
    lab = lab*(-1);
    double detMatrix[3][3]{
        {lab.x, p01.x, p02.x},
        {lab.y, p01.y, p02.y},
        {lab.z, p01.z, p02.z}
    };

    double det {
        detMatrix[0][0] * detMatrix[1][1] * detMatrix[2][2] +
        detMatrix[0][1] * detMatrix[1][2] * detMatrix[2][0] +
        detMatrix[0][2] * detMatrix[1][0] * detMatrix[2][1] -
        detMatrix[0][2] * detMatrix[1][1] * detMatrix[2][0] -
        detMatrix[0][1] * detMatrix[1][0] * detMatrix[2][2] -
        detMatrix[0][0] * detMatrix[1][2] * detMatrix[2][1]
    };
    if(fabs(det) < 1E-12 ) {
        throw std::invalid_argument("No intersection between line and plane");
    }
    Point p;
    auto multiplier = (Vector(l.p1 - p1)) / (lab*(p01^p02));
    double t = (p01^p02)*multiplier;
    return l.p1 + Point(lab.x*t*(-1), lab.y*t*(-1), lab.z*t*(-1));
}
