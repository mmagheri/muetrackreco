
import ROOT
import os
import sys

# Define the path to the root file containing the histograms
root_file_path = "/muonedqm/muetrackreco/test/Resolution/run_2/5285332_5295332_resolution_start20_noUpdateOnUV_iteTotal_9_onModuleIt_4.root"
if len(sys.argv) < 2:
    print("Please provide a value for x or y.")
    sys.exit(1)

x_or_y = sys.argv[1]

if x_or_y == "X":
    # Your code for x goes here
    print("You chose x.")
elif x_or_y == "Y":
    # Your code for y goes here
    print("You chose y.")
else:
    print("Invalid choice. Please choose either x or y.")
    sys.exit()  # Break the execution here

    

xModules = ["0", "2", "3", "4", "6", "8", "9", "10"]
yModules = ["1", "2", "3", "5", "7", "8", "9", "11"]

modules = xModules if x_or_y == "X" else yModules

# Open the root file and get the histograms
root_file = ROOT.TFile(root_file_path)
xHistos = []

for f in modules:
    histo = root_file.Get("hDistanceFromInterceptedPoint" + x_or_y + "_" + f)
    histo.Rebin(6)
    histo.GetXaxis().SetRangeUser(-200, 200)
    histo.SetTitle("Residuals on " + x_or_y + " for module " + f)
    histo.SetXTitle("Distance from the intercepted point on " + x_or_y + " (um)")
    histo.SetYTitle("# events")
    # Fit the histogram with a Gaussian distribution
    fit_func = ROOT.TF1("fit_func", "gaus")
    histo.Fit(fit_func)
    xHistos.append(histo)

# Create a canvas to plot the histograms
canvas = ROOT.TCanvas("canvas", "Residuals", 600*2, 800*2)
canvas.Divide(2, 4)

for i, histo in enumerate(xHistos):
    pad = canvas.cd(i + 1)
    pad.SetLeftMargin(0.15)
    pad.SetRightMargin(0.15)
    pad.SetTopMargin(0.15)
    pad.SetBottomMargin(0.15)
    histo.Draw()
    # Draw the fit function on top of the histogram
    fit_func.Draw("same")

# Save the canvas as a pdf file
canvas.SaveAs("run2_residuals_" + x_or_y + "_update_design.pdf")
