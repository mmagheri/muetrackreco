#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include "TH1F.h"
#include "TFile.h"
#include <gtest/gtest.h>


TEST(TTreeTest, prova) {
  TTreeReader t;
  t.parseTree("../3086_0348f00a0_0348f409f_4201232.root");
}

TEST(TTreeTest, provaEv) {

  //th1f with 100 bins from -100 to 100
  
  TTreeReader t;
  t.parseTree("../3086_0348f00a0_0348f409f_4201232.root");
  ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_RH_RF.xml");
  t.setStation(&experimentSetup);
  t.experimentalSetup->print();
  TFile* newFile = new TFile("../testTest.root", "RECREATE");
  std::vector<TH1F*> allResidualsX, allResidualsY, allResidualsPerpendicular, allResidualsDistance;
  for(int i=0; i<6; i++){
    allResidualsX.push_back(new TH1F(("hX_"+std::to_string(i)).c_str(), ("hX_"+std::to_string(i) + "; deltaLines [um]; entries").c_str(), 10000, -10, 10));
  }
  for(int i=0; i<6; i++){
    allResidualsY.push_back(new TH1F(("hY_"+std::to_string(i)).c_str(), ("hY_"+std::to_string(i) + "; deltaLines [um]; entries").c_str(), 10000, -10, 10));
  }
  for(int i=0; i<6; i++){
    allResidualsPerpendicular.push_back(new TH1F(("hP_"+std::to_string(i)).c_str(), ("hP_"+std::to_string(i) + "; deltaLines [um]; entries").c_str(), 10000, -10, 10));
  }
  for(int i=0; i<6; i++){
    allResidualsDistance.push_back(new TH1F(("hD_"+std::to_string(i)).c_str(), ("hD_"+std::to_string(i) + "; deltaLines [um]; entries").c_str(), 10000, -10, 10));
  }
  for(int dut = 0; dut < 6; dut++) {
    std::cout << "------ DUT " << dut << " ------" << std::endl;
    int nTracks = 0;
    //if(dut != 0) continue;
    for(int i=0; i< t.getEntries() && nTracks<10000; i++){
      std::cout << "\r" << std::flush;
      t.loadEventNumber(i);
      if(!t.hasOneStubPerModule()) continue;
      //Track track = (t.experimentalSetup->stationMap[0]).buildTrackExceptModule(dut);
      Track track = (t.experimentalSetup->stationMap[0]).buildTrack();
      if(track.hasEnoughLines() && track.minDistance < 0.5) {
        if((t.experimentalSetup->stationMap[0]).modules[dut].getNStubs() == 0) continue;
        Line l = (t.experimentalSetup->stationMap[0]).modules[dut].getLineFromStub(0);
        
        //allResidualsPerpendicular[dut]->Fill(l.distancePerpendicularTo(track.trackLine));
        allResidualsDistance[dut]->Fill(l.distance(track.trackLine));
        nTracks++;
        //Have to implement plane intersection with line -> https://math.stackexchange.com/questions/83990/line-and-plane-intersection-in-3d
        if(nTracks%1000==0) std::cout << "Track " << nTracks << std::endl;
      }
    }
  }
  for(auto& h : allResidualsX) h->Write();
  for(auto& h : allResidualsY) h->Write();
  for(auto& h : allResidualsPerpendicular) h->Write();
  for(auto& h : allResidualsDistance) h->Write();
  newFile->Write();
}