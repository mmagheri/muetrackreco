#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "EfficiencyCalculator.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>
#include <fstream>

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]){

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/7225206_7235206.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	
	std::string outputFileName = "output.txt"; // default output file name
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::ofstream outputFile(outputFileName); // create output file

	std::string xmlFileName = "/muonedqm/muetrackreco/results/dgm_updated/run_7/test_2.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	auto efficiencyCalculator = std::make_unique<EfficiencyCalculator>();
	efficiencyCalculator->linkTracker(tracker);
	efficiencyCalculator->linkTTreeReader(treeReader);
	efficiencyCalculator->linkStation(&experimentSetup);
	efficiencyCalculator->updateResolutionFromFile("/muonedqm/muetrackreco/results/dgm_updated/run_7/test_2.root");

	for(auto& stationPair: experimentSetup.stationMap){
		for(auto& modulePair: stationPair.second.modules){
			for(int i = 0; i < 2; i++){
				std::cout << "Module " << modulePair.first << " axis " << i << " resolution: " << modulePair.second.getResolution(i) << std::endl;
				std::cout << "Module " << modulePair.first << " axis " << i << " meanPosition: " << modulePair.second.getMeanResidualPosition(i) << std::endl;
			}
		}
	}
	//efficiencyCalculator->getEfficiencyForModuleNumber(0);
	std::vector<efficiency> efficiencies;
	
	for(int i = 0; i<12; i++) {
		efficiencies.push_back(efficiencyCalculator->getEfficiencyForModuleNumber(i));
	}
	//efficiencies.push_back(efficiencyCalculator->getEfficiencyForModuleNumber(5));
	std::cout << "Overall efficiencies: " << std::endl;
	int count = 0;
	for(auto& eff: efficiencies){
		std::cout << count << " : " << eff.nGood << " over " << eff.nTotal << std::endl;
		std::cout << count << " : " << eff.efficiency<< " over " << eff.error << std::endl;
		outputFile << count << " : " << eff.nGood << " over " << eff.nTotal << std::endl;
		outputFile << count << " : " << eff.efficiency<< " over " << eff.error << std::endl;
		count++;
	}

	outputFile.close(); // close the output file

	return 0;

	
}