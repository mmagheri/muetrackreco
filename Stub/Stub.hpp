#pragma once
#include <cinttypes>
#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <vector>
const int NUMBER_OF_MODULES = 6;

struct Stub {
	uint32_t superID;
	uint16_t bx;
	float bend;
	float localX;
	float localY;
	float meanModuleZ;
	uint16_t link;

};

class Event {
  public:
	virtual void print() = 0;
	virtual bool pushBack(Stub s) = 0;
	virtual void clear() = 0;
};
