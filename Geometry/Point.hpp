#pragma once
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <vector>
#include <Eigen/Geometry> 

class Point {
public:
    double x, y, z;
	//default constructor
	Point() : x(0), y(0), z(0) {}
    Point(double x, double y, double z) : x(x), y(y), z(z) {}

	void rotate(double angX, double angY, double angZ);

    void print();
	//translate by z
	void translateZ(double z);
	void translateX(double x);
	void translateY(double y);

	//product with scalar *
	Point operator*(double s);
	Point operator+(Point p);
	Point operator/(double s);
	Point operator-(Point p);

	bool operator==(const Point& p) const;
	bool operator!=(const Point& p) const ;

	friend std::ostream& operator<<(std::ostream& os, const Point& p) {
		os << "x: " << p.x << " y: " << p.y << " z: " << p.z;
		return os;
	}


private:
	void rotateX(double angle);
	void rotateY(double angle);
	void rotateZ(double angle);
};