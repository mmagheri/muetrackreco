import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5285332_5295332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5265332_5275332.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_2/5275332_5285332.root",
]

xmlFiles = [
"/muonedqm/muetrackreco/test/run_2/5265332_5275332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml",
"/muonedqm/muetrackreco/test/run_2/5285332_5295332_alignment_5.xml",
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("/muonedqm/muetrackreco/test/residuals/run_2/" + filename.replace(".root", "_alignment"))

print(output_files)

command = "./build/apps/testAligner"

def process_file(rootfile, output_file, xmlFile):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
    for rootfile, output_file, xmlFile in zip(rootfiles, output_files, xmlFiles):
        executor.submit(process_file, rootfile, output_file, xmlFile)

