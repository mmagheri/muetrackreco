import ROOT

def process_histogram(hist, title, x_title, y_title, x_range=None, normalize=False, draw_option=""):
    """
    Process a histogram: set titles, range, normalize, and draw.
    """
    hist.SetTitle(title)
    hist.SetXTitle(x_title)
    hist.SetYTitle(y_title)
    if title == "Position of the reconstructed vertex on the beam axis":
        n_bins = hist.GetNbinsX()
        x_min = hist.GetXaxis().GetXmin() / 1e4
        x_max = hist.GetXaxis().GetXmax() / 1e4

        # Create a new histogram with the rescaled x-axis
        rescaled_hist = ROOT.TH1F("rescaled_hist", x_title, n_bins, x_min, x_max)
        rescaled_hist.SetYTitle(y_title)
        rescaled_hist.SetTitle(title)
        rescaled_hist.SetXTitle(x_title)
        # Fill the new histogram with the contents of the original histogram
        for bin in range(1, n_bins + 1):
            rescaled_hist.SetBinContent(bin, hist.GetBinContent(bin))
            rescaled_hist.SetBinError(bin, hist.GetBinError(bin))
            rescaled_hist.Draw("L")
        return rescaled_hist

    
    if x_range:
        hist.GetXaxis().SetRangeUser(*x_range)

    if normalize:
        hist.DrawNormalized(draw_option)
    else:
        hist.Draw(draw_option)

    return hist


def drawDoubleChiSquare(outputFolder):
    """
    Draw the chi square of the first and second stations on the same canvas.
    """
    # Get the histograms
    chiSquareFirstStation = file.Get("chiSquareFirstStation")
    chiSquareSecondStation = file.Get("chiSquareSecondStation")

    # Set titles
    chiSquareFirstStation.SetTitle("Chi square of tracks in the first station")
    chiSquareFirstStation.SetXTitle("chi2")
    chiSquareFirstStation.SetYTitle("fraction of entries")
    chiSquareSecondStation.SetTitle("Chi square of tracks in the second station")
    chiSquareSecondStation.SetXTitle("chi2")
    chiSquareSecondStation.SetYTitle("fraction of entries")

    # Set range
    chiSquareFirstStation.GetXaxis().SetRangeUser(0, 100)
    chiSquareSecondStation.GetXaxis().SetRangeUser(0, 100)

    # Draw
    chiSquareFirstStation.DrawNormalized()
    chiSquareSecondStation.SetLineColor(ROOT.kRed+2)
    chiSquareSecondStation.DrawNormalized("SAME")

    # Create a legend
    legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    legend.AddEntry(chiSquareFirstStation, "First Station", "l")
    legend.AddEntry(chiSquareSecondStation, "Second Station", "l")
    legend.Draw("SAME")

    # Save the canvas as a png file
    canvas.SaveAs(outputFolder + "chiSquare.png")

# Open the root file
file = ROOT.TFile.Open("/muonedqm/muetrackreco/test/vertex_update_chi2/vertexSum.root")
outputFolder = "test/plot/"

# Create a canvas
canvas = ROOT.TCanvas("canvas", "canvas", 800, 600)
canvas.SetLeftMargin(0.15)  # Increase left margin
canvas.SetRightMargin(0.15)  # Increase right margin
canvas.SetTopMargin(0.15)    # Increase top margin
canvas.SetBottomMargin(0.15) # Increase bottom margin

# Set gStyle to not show the statistics
ROOT.gStyle.SetOptStat(0)

# List of histograms to process
histograms = [
    ("vertexPosition", "Position of the reconstructed vertex on the beam axis", "Z vertex [cm]", "Fraction of the analyzed tracks", None, True, ""),
    ("vertexTrackS1Distance", "Distance between the reconstructed vertex and the S1 track", "Distance [um]", "Fraction of the analyzed tracks", (0, 2000), False, ""),
    ("angleBetweenTracks", "Angle between the two reconstructed tracks", "angle tk1 [rad]", "angle tk2 [rad]", None, False, "colz"),
    #("chiSquareFirstStation", "Chi square of tracks in the first station", "chi2", "fraction of entries", None, True, ""),
    #("chiSquareSecondStation", "Chi square of tracks in the second station", "chi2", "fraction of entries", None, True, "SAME"),
]

# Process each histogram
for hist_name, title, x_title, y_title, x_range, normalize, draw_option in histograms:
    print("Processing histogram: " + hist_name)
    hist = file.Get(hist_name)
    processed_hist = process_histogram(hist, title, x_title, y_title, x_range, normalize, draw_option)

    # Save the canvas as a png file
    canvas.SaveAs(outputFolder + hist_name + ".png")

drawDoubleChiSquare(outputFolder)
# Draw the chi square of the first and second stations on the same canvas