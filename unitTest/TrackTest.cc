#include "Converter.hpp"
#include "ExperimentalSetup.hpp"
#include "Track.hpp"
#include <gtest/gtest.h>


TEST(TrackTest, prova) {
  Stub s0;
  s0.superID = 10;
  s0.bx = 11;
  s0.bend = 12;
  s0.localX = 13;
  s0.localY = 0.75;
  s0.meanModuleZ = 15;
  s0.link = 0;

  Stub s1;
  s1.superID = 10;
  s1.bx = 11;
  s1.bend = 12;
  s1.localX = 13;
  s1.localY = 0.75;
  s1.meanModuleZ = 15;
  s1.link = 1;

  Stub s2;
  s2.superID = 10;
  s2.bx = 11;
  s2.bend = 12;
  s2.localX = 13;
  s2.localY = 0.75;
  s2.meanModuleZ = 15;
  s2.link = 2;

  Stub s4;
  s4.superID = 10;
  s4.bx = 11;
  s4.bend = 12;
  s4.localX = 13;
  s4.localY = 0.75;
  s4.meanModuleZ = 15;
  s4.link = 4;
  
  Stub s5;
  s5.superID = 10;
  s5.bx = 11;
  s5.bend = 12;
  s5.localX = 13;
  s5.localY = 0.75;
  s5.meanModuleZ = 15;
  s5.link = 5;

  std::vector<Stub> vStubs;
  vStubs.push_back(s0);
  vStubs.push_back(s1);
  vStubs.push_back(s2);
  //vStubs.push_back(s3);
  vStubs.push_back(s4);
  vStubs.push_back(s5);


  ExperimentalSetup experimentSetup = getStationClassFromXml("../Structure/MUonEStructure_TB2022_RH_RF_pnew.xml");
  experimentSetup.print();
  Track t;
  
  for(auto &s: vStubs) {
    experimentSetup.stationMap[0].modules[s.link].pushStub(s);
    experimentSetup.stationMap[0].modules[s.link].getLineFromStub(0).print();
    if(s.link!=2) t.addLine(experimentSetup.stationMap[0].modules[s.link].getLineFromStub(0));
  }
  
  t.minimizeDistances();
  std::cout << "Track: " << "\n";
  t.print();
  EXPECT_TRUE(t.minDistance < 10);
  experimentSetup.stationMap[0].modules[3].print();
  std::cout << "Plane:\n";
  std::cout << experimentSetup.stationMap[0].modules[3].plane << "\n";
  std::cout << "intersection point on module 3: \n";
  std::cout << experimentSetup.stationMap[0].modules[3].intersect(t.trackLine) << "\n";
  std::cout << "Covariance matrix: \n";
  t.getCovMatrix().Print();
  for(auto & err: t.getErrorsFromTrackCalculatedAtT(3)) {
    std::cout << err << "\n";
  }
}
