#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "Tracker.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]) {

	bool allModules = cmdOptionExists(argv, argv + argc, "-a");
	bool considerBend = cmdOptionExists(argv, argv + argc, "-b");
	std::string inputFileName = "../3090_014e0f3e0_014e133df_4201232.root";
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	std::string outputFileName = "../prova";
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}
	std::string xmlFileName = "../Structure/MUonEStructure_TB2022_reissued.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}

	TTreeReader t;
	t.parseTree(inputFileName.c_str());
	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), considerBend);
	t.setStation(&experimentSetup);
	experimentSetup.print();

	Tracker tracker(experimentSetup);

	TFile *newFile = new TFile((outputFileName).c_str(), "RECREATE");

	std::vector<TH1F *> distanceFromInterceptedPointX, lineDistance, distanceFromInterceptedPointY,
		distanceFromInterceptedPointZ, sigmaTrackX, sigmaTrackY, hPModuleDistance, distanceFromInterceptedPointX_even,
		distanceFromInterceptedPointX_odd, distanceY_CIC0, distanceY_CIC1;
	std::vector<TH2F *> hdYVsBend, hdYVSCIC, hdYVSLocalX, hdYVSbx;
	int nModules = experimentSetup.getNModules();
	for (int i = 0; i < nModules; i++) {
		std::string iStr = std::to_string(i);

		distanceFromInterceptedPointX.push_back(new TH1F(
			("hDistanceFromInterceptedPointX_" + iStr).c_str(),
			("hDistanceFromInterceptedPointX_" + iStr + "; deltaLines [um]; entries").c_str(), 10000, -10000, 10000));

		distanceFromInterceptedPointY.push_back(new TH1F(
			("hDistanceFromInterceptedPointY_" + iStr).c_str(),
			("hDistanceFromInterceptedPointY_" + iStr + "; deltaLines [um]; entries").c_str(), 10000, -10000, 10000));

		distanceFromInterceptedPointZ.push_back(new TH1F(
			("hDistanceFromInterceptedPointZ_" + iStr).c_str(),
			("hDistanceFromInterceptedPointZ_" + iStr + "; deltaLines [um]; entries").c_str(), 10000, -10000, 10000));

		sigmaTrackX.push_back(new TH1F(("hSigmaTrackX_" + iStr).c_str(),
									   ("hSigmaTrackX_" + iStr + "; sigmaTrack on X[um]; entries").c_str(), 1000, 0,
									   100));

		sigmaTrackY.push_back(new TH1F(("hSigmaTrackY_" + iStr).c_str(),
									   ("hSigmaTrackY_" + iStr + "; sigmaTrack on Y[um]; entries").c_str(), 1000, 0,
									   100));

		lineDistance.push_back(new TH1F(("hLineDistance_" + iStr).c_str(),
										("hLineDistance_" + iStr + "; deltaLines [um]; entries").c_str(), 10000, -10000,
										10000));
	}

	nlohmann::json StubsJson;
	nlohmann::json TracksJson;

	int nTracksMax = 100000;
	int nTracks = 0;
	int nEventsWith6Stubs = 0;
	int nStation = 0; // TODO: Get this from the xml
	for (int i = 0; i < t.getEntries() && nTracks < nTracksMax; i++) {
		if (i % 1000 == 0) {
			std::cout << "Event " << i << std::endl;
		}
		t.loadNextNBx(i, 2);
		tracker.buildAllTracksFromCurrentLoadedEvent(0);
		if (tracker.allTracksGood.size() == 1 ) {
			//std::cout << "Hey, just one track." << std::endl;
			for (int dut = 0; dut < 6; dut++) {
				Line stubLine = (t.experimentalSetup->stationMap[0]).modules.at(dut).getLineFromStub(0);
				auto track = tracker.allTracksGood.at(0);
				for(auto& tk : tracker.allTracksGood) {
					if(tk.trackLine.distance(stubLine) < track.trackLine.distance(stubLine)) {
						track = tk;
					}
				}
				if (fabs(stubLine.distance(track.trackLine)) > 1000 * 5) {
					continue;
				}
				Point pointInterceptAtDUT =
					experimentSetup.stationMap[0].modules.at(dut).intersect(track.trackLine);
				//sigmaTrackX[dut]->Fill(tracker.currentTrack.getErrorsFromTrackCalculatedAtT(t)[0]);
				//sigmaTrackY[dut]->Fill(tracker.currentTrack.getErrorsFromTrackCalculatedAtT(t)[1]);
				distanceFromInterceptedPointX[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).x);
				distanceFromInterceptedPointY[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).y);
				distanceFromInterceptedPointZ[dut]->Fill(stubLine.vectorDistance(pointInterceptAtDUT).z);
				lineDistance[dut]->Fill(stubLine.distance(track.trackLine));

				if(dut == 0) {
					nTracks++;
					if (nTracks % 100 == 0) 
						std::cout << "We have " << nTracks << " tracks" << std::endl;
				}
			}

		}
	}

	std::cout << "Saving---" << nTracks << "\n";
	for (auto &h : distanceFromInterceptedPointX)
		h->Write();
	for (auto &h : distanceFromInterceptedPointY)
		h->Write();
	for (auto &h : distanceFromInterceptedPointZ)
		h->Write();
	for (auto &h : sigmaTrackX)
		h->Write();
	for (auto &h : sigmaTrackY)
		h->Write();
	newFile->Close();
	return 0;
}