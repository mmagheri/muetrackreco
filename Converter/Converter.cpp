#include "Converter.hpp"
using namespace std;

Converter::Converter(string xmlLUT, string xmlStructure){
    cout << "------- Initializing values from xml for the converter -------" << endl;
    readStructure(xmlStructure);
    cout << "------- Finished initilization of values from xml for the converter -------" << endl;

};

Converter::Converter() {
    CIC_MAPPER[0] = 0;
    CIC_MAPPER[1] = 1;
    CIC_MAPPER[2] = 2;
    CIC_MAPPER[3] = 3;
    CIC_MAPPER[4] = 7;
    CIC_MAPPER[5] = 6;
    CIC_MAPPER[6] = 5;
    CIC_MAPPER[7] = 4;
};

void Converter::readStructure(std::string xmlStructureFile) {
    std::cout << "xml structure file is: " << xmlStructureFile << "\n";
    PositionStation = getStationFromXml(xmlStructureFile);
    std::cout << "Got station postition\n";
    LINKID_MAPPER = getLinkIDMapFromXml(xmlStructureFile);
    std::cout << LINKID_MAPPER[0] << "\n";
    std::cout << LINKID_MAPPER[1] << "\n";
    std::cout << LINKID_MAPPER[2] << "\n";
    std::cout << LINKID_MAPPER[3] << "\n";
    std::cout << LINKID_MAPPER[4] << "\n";
    std::cout << LINKID_MAPPER[5] << "\n";
    std::cout << "Got linkID mapper\n";
};

double  Converter::getPhysicalX(Stub s) {
    int moduleID  = LINKID_MAPPER[s.link];
    int stationID = moduleID/10;
    Station stat = PositionStation[stationID];
    Module  m = stat.modules[moduleID];

    double posX = -999;

    if(moduleID == 0 || moduleID == 4) {//if the module reads the X coordinate...
        //get position in cm in the module local reference frame
        posX = (s.localX-NSTRIPS/2)*STRIP_PITCH;//do the conversion from strips to cm in the local reference frame
    }
    else if(moduleID == 1 || moduleID == 5) {//if the modul reads the Y coordinate...
        //get position in cm in the module local reference frame
        posX = (s.localY - 0.5)*FULL_DIMENSIONS_Y;
    }

    //adjust orientation of the X-axis in case modules are rotated around global Z-axis
    posX = posX*std::cos(m.rot[2]);

    //local posX projection along the X-axis of the global reference frame (which means... apply the tilt if the modules is tilted around global Y-axis)
    posX = posX*std::cos(m.rot[1]);

    //adjust the offset
    posX = posX + (PositionStation[stationID].modules[moduleID]).pos[0] + PositionStation[stationID].pos[0];

    return posX;//in cm
}

double  Converter::getPhysicalY(Stub s) {
    int moduleID  = LINKID_MAPPER[s.link];
    int stationID = moduleID/10;
    Station stat = PositionStation[stationID];
    Module  m = stat.modules[moduleID];

    double posY = -999;

    if(moduleID == 0 || moduleID == 4) {
        //get position in cm in the module local reference frame
        posY = (s.localY-0.5)*FULL_DIMENSIONS_Y;
    }
    else if(moduleID == 1 || moduleID == 5) {
        //get position in cm in the module local reference frame
        double posStrip = s.localX;//arrange strip in range [-0.5, 1015]
        posY = (posStrip-NSTRIPS/2)*STRIP_PITCH - STRIP_PITCH/2.;//do the conversion from strips to cm in the local reference frame
    }
    //adjust orientation of the Y-axis in case modules are rotated around global Z-axis
    posY = posY*std::cos(m.rot[2]);
    //local posY projection along the Y-axis of the global reference frame (which means... apply the tilt if the modules is tilted around global X-axis)
    posY = posY*std::cos(m.rot[0]);
    //adjust the offset
    posY = posY + (PositionStation[stationID].modules[moduleID]).pos[1] + PositionStation[stationID].pos[1];
    return posY;//in cm
}

double Converter::getPhysicalZ(Stub s) {
    int moduleID  = LINKID_MAPPER[s.link];
    int stationID = moduleID/10;
    Station stat = PositionStation[stationID];
    Module  m = stat.modules[moduleID];

    //get X-position in cm in the module local reference frame
    double posStrip = s.localX;//arrange strip in range [-0.5, 1015]
    double pos = - (posStrip - NSTRIPS/2.)*STRIP_PITCH - STRIP_PITCH/2.;//do the conversion from strips to cm in the local reference frame

    //adjust orientation of the X-axis in case modules are rotated around global Z-axis
    pos = pos*std::cos(m.rot[2]);

    //get Z-position along Z-axis of the global reference frame
    double posZ = -999;
    if(moduleID == 0 || moduleID == 4) {
        posZ = - pos*std::sin(m.rot[1]);
    }
    else if(moduleID == 1 || moduleID == 5) {
        posZ =   pos*std::sin(m.rot[0]);
    }
    else {
        posZ = 0;
    }
    
    //adjust the offset
    posZ = posZ + m.pos[2] + stat.pos[2];
    return posZ;
}

double Converter::getMeanModuleZ(unsigned link) {
    int moduleID  = LINKID_MAPPER[link];
    int stationID = moduleID/10;
    return PositionStation[stationID].modules[moduleID].pos[2]+ PositionStation[stationID].pos[2];
}

Point Converter::revertCoordinateToModule(Point p, unsigned link) {
    int moduleID  = LINKID_MAPPER[link];
    int stationID = moduleID/10;
    Station stat = PositionStation[stationID];
    Module  m = stat.modules[moduleID];

    p.y = p.y - (PositionStation[stationID].modules[moduleID]).pos[1] - PositionStation[stationID].pos[1];
    p.x = (p.x - (PositionStation[stationID].modules[moduleID]).pos[0] - PositionStation[stationID].pos[0]);///std::cos(m.rot[2])/std::cos(m.rot[1]);
    
    p.rotate(-m.rot[0], -m.rot[1], -m.rot[2]);

    std::cout << p.x << "\n";

    double localX = p.x/STRIP_PITCH;
    std::cout <<"localX " << localX << "\n";
    std::cout << "---\n";

    p.z = getMeanModuleZ(link);
    return p;//in cm
}

