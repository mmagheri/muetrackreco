import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

xml_files = [
    '/muonedqm/muetrackreco/test/run_2/5265332_5275332_alignment_5.xml',
    '/muonedqm/muetrackreco/test/run_2/5275332_5285332_alignment_5.xml',
    '/muonedqm/muetrackreco/test/run_2/5285332_5295332_alignment_5.xml',
    '/muonedqm/muetrackreco/test/run_7/1405206_1415206_alignment_5.xml',
    '/muonedqm/muetrackreco/test/run_7/1415206_1425206_alignment_5.xml',
    '/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_5.xml',
    ]


def get_module_data(xml_content):
    tree = ET.ElementTree(ET.fromstring(xml_content))
    module_data = {}
    for module in tree.findall(".//Module"):
        number = module.attrib['number']
        position = module.find('Position')
        rotation = module.find('Rotation')
        module_data[number] = {
            'positionX': float(position.attrib['positionX']),
            'positionY': float(position.attrib['positionY']),
            'positionZ': float(position.attrib['positionZ']),
            'rotationX': float(rotation.attrib['rotationX']),
            'rotationY': float(rotation.attrib['rotationY']),
            'rotationZ': float(rotation.attrib['rotationZ'])
        }
    return module_data

def calculate_differences(xml_files, reference_xml):
    with open(reference_xml, 'r') as file:
        ref_xml_content = file.read()
    ref_data = get_module_data(ref_xml_content)

    differences = {attr: {file: {} for file in xml_files} for attr in ref_data[next(iter(ref_data))]}
    for xml_file in xml_files:
        with open(xml_file, 'r') as file:
            xml_content = file.read()
        current_data = get_module_data(xml_content)
        for number, data in current_data.items():
            ref_module_data = ref_data.get(number, {})
            for attr in data:
                differences[attr][xml_file][number] = data[attr] - ref_module_data.get(attr, 0)
    return differences

def plot_differences(differences):
    fig, axes = plt.subplots(3, 2, figsize=(12, 18))  # Creating a 3x2 grid of subplots
    #fig.suptitle('Differences in Positions and Rotations for Each Module Across Files')

    axes = axes.flatten()  # Flattening the 2D axes array for easy iteration
    file_legend_handles = {}  # Dictionary to store unique handles for legend

    for i, (attr, files_diffs) in enumerate(differences.items()):
        for file, diffs in files_diffs.items():
            module_numbers = list(diffs.keys())
            values = list(diffs.values())
            label = f'File: {file.split("/")[-1]}'
            if label not in file_legend_handles:  # Only add new handles
                line, = axes[i].plot(module_numbers, values, marker='o', linestyle='none', label=label)
                file_legend_handles[label] = line
            else:
                axes[i].plot(module_numbers, values, marker='o', linestyle='none', color=line.get_color())  # Use same color as in legend

        axes[i].set_xlabel('Module Number')
        if('position' in attr):
            axes[i].set_ylabel(f'Difference from nominal value in {attr} [cm]')
            axes[i].set_title(f'Difference in {attr}')
            axes[i].set_ylim([-0.2, 0.2])
        if('rotation' in attr):
            axes[i].set_ylabel(f'Difference from nominal value in {attr} [rad]')
            axes[i].set_title(f'Difference in {attr}')
            axes[i].set_ylim([-0.1, 0.1])


    # Creating a single legend outside the subplots
    ncol_legend = max(1, len(xml_files) // 3)
    fig.legend(file_legend_handles.values(), file_legend_handles.keys(), loc='lower center', bbox_to_anchor=(0.5, 0), ncol=ncol_legend)

    plt.tight_layout(rect=[0, 0.03, 1, 0.97])  # Adjust layout to fit the main title and legend
    plt.savefig('all_differences_updated_2.png')
    plt.show()


# The rest of your script remains the same


def main():

    reference_xml = "/muonedqm/muetrackreco/Structure/TB2023_test.xml"
    differences = calculate_differences(xml_files, reference_xml)
    plot_differences(differences)

if __name__ == "__main__":
    main()
