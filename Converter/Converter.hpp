#pragma once
#include "xmlUtils.hpp"
#include "Stub.hpp"
#include <math.h>
//#include "Stub.hpp"
#include <numeric>
#include "Plane.hpp"
using namespace std; 

//Class with functions to convert local stub coordinates to physical coordinates in the global
//reference frame, and to convert bend code into strip units according to the LookUpTable

class Converter
{
    private:
        std::map<int, int>           LINKID_MAPPER;
        std::map<int, int>           CIC_MAPPER;
        const int NSTRIPS = 1016;
        const double STRIP_PITCH = 0.009;                       //cm (90um)
        const double FULL_DIMENSIONS_X = NSTRIPS*STRIP_PITCH;   //cm. full dimensions of a 2S module along the measured coordinate (9.144cm)
        const double FULL_DIMENSIONS_Y = 10;                    //cm. full dimensions of a 2S module along strip axis (2 sets of strips, each set is 5cm long)

    public:
        map<int, Station>            PositionStation;   //key = stationID
        Converter(string xmlLUT, string xmlStructure);
        Converter();
        void readStructure(std::string xmlStructureFile_);

        double getPhysicalX(Stub s);//unsigned address, unsigned Cbc, unsigned Cic, unsigned link);
        double getPhysicalY(Stub s);//unsigned address, unsigned Cbc, unsigned Cic, unsigned link);
        double getPhysicalZ(Stub s);//unsigned address, unsigned Cbc, unsigned Cic, unsigned link);
        Point revertCoordinateToModule(Point p, unsigned link);
        double getMeanModuleZ(unsigned link);
        std::vector<float> getPointFromXY(Stub sFromX, Stub sFromY);
        Point getPointFromUV(Stub sFromU, Stub sFromV);
};
