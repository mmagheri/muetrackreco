import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

rootfiles = [
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1405206_1415206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1415206_1425206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1425206_1435206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1435206_1445206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1725206_1735206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1735206_1745206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1745206_1755206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/1755206_1765206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2055206_2065206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2065206_2075206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2075206_2085206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2375206_2385206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2385206_2395206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2395206_2405206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2405206_2415206.root",
##"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2695206_2705206.root",
##"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2705206_2715206.root",
##"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2715206_2725206.root",
##"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/2725206_2735206.root",
##"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3025206_3035206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3035206_3045206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3045206_3055206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3055206_3065206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3345206_3355206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3355206_3365206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3365206_3375206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3375206_3385206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3665206_3675206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3675206_3685206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3685206_3695206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3695206_3705206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/3995206_4005206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4005206_4015206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4015206_4025206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4025206_4035206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4315206_4325206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4325206_4335206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4335206_4345206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4345206_4355206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4645206_4655206.root",
#"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4665206_4675206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/4965206_4975206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5315206_5325206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5615206_5625206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5625206_5635206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5635206_5645206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5645206_5655206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5935206_5945206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5945206_5955206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5955206_5965206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/5965206_5975206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6255206_6265206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6265206_6275206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6275206_6285206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6585206_6595206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6595206_6605206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6605206_6615206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6615206_6625206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6915206_6925206.root",
"/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/6925206_6935206.root",
]

output_files = []
for rootfile in rootfiles:
    filename = os.path.basename(rootfile)
    output_files.append("vertexWithChisquareCutAt1_withTree/" + filename.replace(".root", "_vertex.root"))

xmlFile = "/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_5.xml"

command = "./build/apps/testVertexer"

def process_file(rootfile, output_file):
    subprocess.run([command, "-i", rootfile, "-o", output_file, "-x", xmlFile])

with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() - 10) as executor:
    for rootfile, output_file in zip(rootfiles, output_files):
        executor.submit(process_file, rootfile, output_file)

