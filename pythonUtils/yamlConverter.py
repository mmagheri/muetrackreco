import yaml
import math
import xml.etree.ElementTree as ET

# Load the YAML data from a file
with open('/muonedqm/muetrackreco/alignParams_run3220_3221_FixedBend_it10.yaml', 'r') as yaml_file:
    yaml_data = yaml.safe_load(yaml_file)

# Parse the existing XML file
tree = ET.parse('/muonedqm/muetrackreco/Structure/lun6Nov_rotTrasl.xml')
root = tree.getroot()

# Function to convert degrees to radians
def degrees_to_radians(degrees):
    return degrees * math.pi / 180

# Modify XML data using YAML data
# Assuming that the XML file has elements that correspond to each module configuration
for module_index, module_configs in enumerate(yaml_data):
    for config in module_configs:
        # Find the XML element that corresponds to this module configuration
        # Here you need to replace 'module_element_tag' with the actual tag used in your XML
        # and find a way to select the right module (e.g., by an attribute or tag)
        module_element = root.find(f'.//module_element_tag[@id="{module_index}"]')
        if module_element is not None:
            # Apply the transformations from the YAML data to the XML element
            print("AAAAA")
            module_element.find('PositionX').text = str(float(module_element.find('PositionX').text) + config['xOffset'])
            module_element.find('PositionY').text = str(float(module_element.find('PositionY').text) + config['yOffset'])
            module_element.find('PositionZ').text = str(float(module_element.find('PositionZ').text) + config['zOffset'])
            # Convert angle offset from degrees to radians and add it to existing rotationZ
            current_rotation = float(module_element.find('rotationZ').text)
            new_rotation = current_rotation + degrees_to_radians(config['angleOffset'])
            module_element.find('rotationZ').text = str(new_rotation)

# Write the modified XML data back to the file
tree.write('modified_data.xml')
