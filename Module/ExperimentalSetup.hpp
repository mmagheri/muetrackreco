#pragma once

#include "StationClass.hpp"
#include "rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include <map>
#include <string>
#include <vector>

class ExperimentalSetup {
  public:
	std::map<int, StationClass> stationMap;

	ExperimentalSetup(){};
	ExperimentalSetup(std::map<int, StationClass> stationMap) : stationMap(stationMap){};

	void print();
	void printStub();
	void clear();
	void pushStub(Stub s);
	std::vector<std::vector<Stub>> cartesianProduct(int stationId);
	std::vector<std::vector<Line>> getAllLineFromStubsCombinations(int stationId);
	void writeStationClassToXml(const std::string &xmlName);
	int getNModules();

  private:
	void helper(std::vector<std::vector<Stub>> &input, std::vector<Stub> &current,
				std::vector<std::vector<Stub>> &result, unsigned int depth);

	void LineHelper(std::vector<std::vector<Line>> &input, std::vector<Line> &current,
					std::vector<std::vector<Line>> &result, unsigned int depth);
};
