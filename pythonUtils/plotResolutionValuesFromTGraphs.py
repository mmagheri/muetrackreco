import ROOT
import ctypes
import plotly.graph_objs as go
import plotly.io as pio
import csv
import numpy as np
# /muonedqm/muetrackreco/results/dgm_updated/run_2/OneModuleAtATime_chisquareUpd/5275332_5285332_resolution_start20_noUpdateOnUV_resoFinal.root
# /muonedqm/muetrackreco/results/dgm_updated/run_2/OneModuleAtATime_chisquareUpd/5285332_5295332_resolution_start20_noUpdateOnUV_resoFinal.root
# Open the ROOT file

# Names of the ROOT files
root_files = [
            "/muonedqm/muetrackreco/test/test_startingAt10/run2/5265332_5275332_resolution_start20_noUpdateOnUV_resoFinal.root",
            "/muonedqm/muetrackreco/test/test_startingAt10/run2/5275332_5285332_resolution_start20_noUpdateOnUV_resoFinal.root",
            "/muonedqm/muetrackreco/test/test_startingAt10/run2/5285332_5295332_resolution_start20_noUpdateOnUV_resoFinal.root",
            "/muonedqm/muetrackreco/test/test_startingAt10/run7/1405206_1415206_start20_noUpdateOnUV_resoFinal.root",
            "/muonedqm/muetrackreco/test/test_startingAt10/run7/1415206_1425206_start20_noUpdateOnUV_resoFinal.root",
            "/muonedqm/muetrackreco/test/test_startingAt10/run7/1425206_1435206_start20_noUpdateOnUV_resoFinal.root",
        ]

legend_labels = [
            "5265332_5275332",
            "5275332_5285332",
            "5285332_5295332",
            "1405206_1415206",
            "1415206_1425206",
            "1425206_1435206"
        ]

module_data = {i: [] for i in range(12)}

# Create Plotly figure
fig = go.Figure()
for file_name, legend_label in zip(root_files, legend_labels):
    # Open the ROOT file
    file = ROOT.TFile(file_name)

    # Prepare arrays for plotting
    module_numbers = []
    last_y_values = []

    # Loop over the TGraphs
    for i in range(12):

        graph_name = f"module_{i}"
        graph = file.Get(graph_name)
        print (graph_name)
        if graph:
            n_points = graph.GetN()
            x = ctypes.c_double(0)
            y = ctypes.c_double(0)
            
            # Get the last point
            graph.GetPoint(n_points - 1, x, y)
            last_y_values.append(y.value)
            module_data[i].append(y.value)
            module_numbers.append(i)

    # Add a trace for this file with a custom legend label
    fig.add_trace(go.Scatter(x=module_numbers, y=last_y_values, mode='markers', name=legend_label))

# Update layout and set y-axis range
fig.update_layout(
    title='Module Number vs resolution for different datasets',
    xaxis_title='Module Number',
    yaxis_title='Resolution [um]',
    legend_title="Datasets"
)
fig.update_yaxes(range=[0, 60])

# Save the plot as PNG
fig.write_image('combined_module_plot_updated_alignment_start10.png')

csv_data = [["Module Number", "Mean", "SEM"]]
for module, values in module_data.items():
    if values:
        mean = np.mean(values)
        sem = np.std(values, ddof=1) / np.sqrt(len(values))
        formatted_mean = f"{mean:.1f}"
        formatted_sem = f"{sem:.1f}"
        csv_data.append([module, formatted_mean, formatted_sem])
        #csv_data.append([module, mean, sem])
with open('module_mean_sem_start10.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(csv_data)