#include "Converter.hpp"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTreeReader.hpp"
#include "TrackerFromStubs.hpp"
#include "EfficiencyCalculator.hpp"
#include "parser.hpp"
#include "xmlUtils.hpp"
#include <memory>
#include <fstream>

// const int DIMSENSOR = 50000; //IN UM
int main(int argc, char *argv[]){

	std::string inputFileName = "/eos/experiment/mu-e/staging/daq/2023/dgm_merged/run_7/7225206_7235206.root"; //merged file with reasonable number of stubs
	if (cmdOptionExists(argv, argv + argc, "-i")) {
		inputFileName = getCmdOption(argv, argv + argc, "-i");
	}
	
	std::string outputFileName = "output.txt"; // default output file name
	if (cmdOptionExists(argv, argv + argc, "-o")) {
		outputFileName = getCmdOption(argv, argv + argc, "-o");
	}

	std::string xmlFileName = "/muonedqm/muetrackreco/test/run_7/1425206_1435206_alignment_5.xml";
	if (cmdOptionExists(argv, argv + argc, "-x")) {
		xmlFileName = getCmdOption(argv, argv + argc, "-x");
	}
	int moduleNumber = 0; // default module number
	if (cmdOptionExists(argv, argv + argc, "-m")) {
		std::string moduleNumberStr = getCmdOption(argv, argv + argc, "-m");
		moduleNumber = std::stoi(moduleNumberStr);
	}

	ExperimentalSetup experimentSetup = getStationClassFromXml(xmlFileName.c_str(), false);
	//t.setStation(&experimentSetup);
	//experimentSetup.print();
	std::cout << "-----" << std::endl;
	auto treeReader = new TTreeReader();
	treeReader->parseTree(inputFileName.c_str());
	treeReader->setStation(&experimentSetup);
	auto tracker = new TrackerFromStubs(experimentSetup);
	auto efficiencyCalculator = std::make_unique<EfficiencyCalculator>();
	efficiencyCalculator->linkTracker(tracker);
	efficiencyCalculator->linkTTreeReader(treeReader);
	efficiencyCalculator->linkStation(&experimentSetup);
	efficiencyCalculator->updateResolutionFromCsv("/muonedqm/muetrackreco/module_mean_sem.csv");
	efficiencyCalculator->initHistograms();
	for(auto& stationPair: experimentSetup.stationMap){
		for(auto& modulePair: stationPair.second.modules){
			for(int i = 0; i < 2; i++){
				std::cout << "Module " << modulePair.first << " axis " << i << " resolution: " << modulePair.second.getResolution(i) << std::endl;
				std::cout << "Module " << modulePair.first << " axis " << i << " meanPosition: " << modulePair.second.getMeanResidualPosition(i) << std::endl;
			}
		}
	}

	std::vector<efficiency> efficiencies;
	
	for(int i = 0; i<12; i++) {
		if(i!=moduleNumber) continue;
		std::cout << "Module: " << i << std::endl;
		efficiencies = (efficiencyCalculator->getEfficiencyInTimeForModuleNumber(i));
		std::ofstream outputFile(outputFileName + "_module" + std::to_string(i) + ".csv"); // create output file
		
		outputFile << "nGood,nTotal,efficiency,superID" << std::endl;

		for(auto& eff: efficiencies){
			std::cout <<  eff.nGood << " over " << eff.nTotal << " " << eff.efficiency << " time: " << eff.superID << std::endl;
			outputFile << eff.nGood << "," << eff.nTotal << "," << eff.efficiency << "," << eff.superID << std::endl;
		}
		outputFile.close(); // close the output file
		efficiencyCalculator->saveHistograms(outputFileName + "_" + std::to_string(i) + ".root");
	}
	//efficiencies.push_back(efficiencyCalculator->getEfficiencyForModuleNumber(5));

	return 0;

	
}