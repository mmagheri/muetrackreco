
import ROOT
import os
import math
import matplotlib.pyplot as plt
import sys

# Define the path to the root file containing the histograms
root_file_path = "/muonedqm/muetrackreco/results/dgm_updated/run_2/residuals.root"

if len(sys.argv) < 2:
    print("Please provide a value for x or y.")
    sys.exit(1)

x_or_y = sys.argv[1]

if x_or_y == "X":
    # Your code for x goes here
    print("You chose x.")
elif x_or_y == "Y":
    # Your code for y goes here
    print("You chose y.")
else:
    print("Invalid choice. Please choose either x or y.")
    sys.exit()  # Break the execution here

xModules = ["0", "2", "3", "4", "6", "8", "9", "10"]
yModules = ["1", "2", "3", "5", "7", "8", "9", "11"]
modules = xModules if x_or_y == "X" else yModules

# Open the root file and get the histograms
root_file = ROOT.TFile(root_file_path)
xHistos = []
StdDevs = []
for f in modules:
    histo = root_file.Get("hDistanceFromInterceptedPoint" + x_or_y + "_" + f)
    std_dev = histo.GetStdDev()
    StdDevs.append(std_dev)
    print(std_dev)

plt.plot(modules, StdDevs, marker='o', linestyle='')

plt.xlabel('Module number')
plt.ylabel('Residual [um]')
plt.title('Y Residual per module number')

plt.ylim(0, 70)  # Set the y range from 0 to -70


plt.grid(True)
plt.savefig(x_or_y + 'ResidualCompared.png')

