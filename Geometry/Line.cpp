#include "Line.hpp"

void Line::calculateInterceptAndSlope() {
    slope.x = (p2.x - p1.x) / (p2.z - p1.z);
    slope.y = (p2.y - p1.y) / (p2.z - p1.z);
    slope.z = 1; // La pendenza lungo l'asse Z è 1 per definizione

    intercept.x = p1.x - slope.x * p1.z;
    intercept.y = p1.y - slope.y * p1.z;
    intercept.z = 0; // L'intercetta lungo l'asse Z è 0 per definizione
}

void Line::print() {
    std::cout << "p1: ";
    p1.print();
    std::cout << "p2: ";
    p2.print();
}


//line at point t
Point Line::at(double z) {
    double t = (z - p1.z) / (p2.z - p1.z);
    double x = p1.x + (p2.x - p1.x) * t;
    double y = p1.y + (p2.y - p1.y) * t;
    return Point(x, y, z);
}

//distance from point to line
double Line::distance(Point p) {
    Vector v21 = Vector(p2, p1);
    Vector v10 = Vector(p1, p);
    return (v21 ^ v10).length() / v21.length();
}

double Line::distanceX(Point p) {
    Vector v21 = Vector(p2, p1);
    Vector v10 = Vector(p1, p);
    return (v21 ^ v10).x / v21.length();
}

double Line::distanceY(Point p) {
    Vector v21 = Vector(p2, p1);
    Vector v10 = Vector(p1, p);
    return (v21 ^ v10).y / v21.length();
}

double Line::distanceZ(Point p) {
    Vector v21 = Vector(p2, p1);
    Vector v10 = Vector(p1, p);
    return (v21 ^ v10).z / v21.length();
}

Vector Line::getPerpendicularVector() {
    return Vector(p1, p2) ^ Vector(0, 0, 1);
}

Vector Line::vectorDistance(Point m) {
    double dist = (Vector(p1, p2) * Vector(p1, m))/(pow(Vector(p1, p2).length(),2));
    Vector p1q = Vector(p1, p2) * dist;
    Point q = Point(p1.x + p1q.x, p1.y + p1q.y, p1.z + p1q.z);
    return Vector(q, m); 
}

//is parallel
bool Line::isParallel(Line l) {
    return (v2 ^ l.v2).length() < 1E-07;
}

//distance from line to line
double Line::distance(Line l) {
    if(isParallel(l)) {
        return distance(l.p1);
    }
    Vector crossBetweenLines = v2 ^ l.v2;
    Vector deltaPoints = Vector(p1) - Vector(l.p1);
    return fabs(crossBetweenLines*deltaPoints) / crossBetweenLines.length();
}

double Line::signedDistance(Line l) {
    if(isParallel(l)) {
        return distance(l.p1);
    }
    Vector crossBetweenLines = v2 ^ l.v2;
    Vector deltaPoints = Vector(p1) - Vector(l.p1);
    return fabs(crossBetweenLines*deltaPoints) / crossBetweenLines.length();
}

void Line::rotate(double angX, double angY, double angZ){
    p1.rotate(angX, angY, angZ);
    p2.rotate(angX, angY, angZ);
    v1 = Vector(p1);
    v2 = Vector(p2) - v1;
}


//translate line in z direction
void Line::translateZ(double z) {
    p1.translateZ(z);
    p2.translateZ(z);
    v1 = Vector(p1);
    v2 = Vector(p2) - v1;
}
//translate line in x direction
void Line::translateX(double x) {
    p1.translateX(x);
    p2.translateX(x);
    v1 = Vector(p1);
    v2 = Vector(p2) - v1;
}
//translate line in y direction
void Line::translateY(double y) {
    p1.translateY(y);
    p2.translateY(y);
    v1 = Vector(p1);
    v2 = Vector(p2) - v1;
}

//< operator returning true if the line has p1.z less than another line
bool Line::operator<(const Line& l) const {
    return p1.z < l.p1.z;
}

//== operator
bool Line::operator==(const Line& l) const {
    return (p1 == l.p1 && p2 == l.p2) || (p1 == l.p2 && p2 == l.p1);
}

double Line::getZatX(double x) {
    double t = (x - p1.x) / (p2.x - p1.x);
    double z = p1.z + (p2.z - p1.z) * t;
    return z;
}

//get t from point on line
double Line::getT(Point p) {
    return (p.x - p1.x) / (p2.x - p1.x);
}

Point Line::getPointAtT(double t) {
    return p1 + (p2 - p1) * t;
}

Point Line::closestPointWrtLine(Line otherLine) {
    Vector u = v2;
    Vector v = otherLine.v2;
    Vector w = p1 - otherLine.p1;
    double a = u * u;
    double b = u * v;
    double c = v * v;
    double d = u * w;
    double e = v * w;
    double D = a * c - b * b;
    double tU, tV;
    if (D < 1e-6) {
        tU = 0.0;
        tV = (b > c ? d / b : e / c);
    }
    else {
        tU = (b * e - c * d) / D;
        tV = (a * e - b * d) / D;
    }
    Point p1Closest = p1 + (u * tU).toPoint();
    Point p2Closest = otherLine.p1 + (v * tV).toPoint();
    return (p1Closest + p2Closest) / 2.0;
}

double Line::angle(Line& l) {
    return l.v2.angle(v2);
}
