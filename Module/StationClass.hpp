#pragma once
#include "ModuleClass.hpp"
#include <array>
#include <cmath>
#include <fstream>
#include <map>
#include <nlohmann/json.hpp>
#include <unordered_map>
#include <vector>

class StationClass {
  public:
	std::string name;
	int nModules;
	int ID;
	std::map<int, ModuleClass> modules;
	double pos[3], rot[3];

	void print();
	void printStub();
	void printNModules();
	void clear();
	void pushStub(Stub s);
	Track buildTrackExceptModule(int nModuleToRemove);
	Track buildMultipleTrackExceptModule(int nModuleToRemove);
	Track buildXYTrack();
	Track buildSimpleXYTrack();
	Track buildTrack();
	void dumpStubsToJson(nlohmann::json &existingJson, int eventNumber);
	Track buildTrackFromStubs(std::vector<Stub> stubs);
	bool removeStub(Stub s);
};
