import pandas as pd
import sys
import os
import matplotlib.pyplot as plt

# Read the CSV file

input_file = sys.argv[1]  # Get the input file from command-line argument

input_file = sys.argv[1]  # Get the input file from command-line argument
output_file = sys.argv[2]  # Get the output file from command-line argument
module_number = sys.argv[3]  # Get the output file from command-line argument
#output_file = os.path.splitext(os.path.basename(input_file))[0] + '.png'
#print(output_file)

folder_path = input_file  # Replace with the actual folder path

# Get all CSV files in the folder
csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv')]
# Get all CSV files in the folder that contain a chosen string
chosen_string = "_module" + module_number  # Replace "chosen_string" with your desired string
csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv') and chosen_string in file]


# Initialize an empty list to store the dataframes
dfs = []

# Iterate over the CSV files and load them into dataframes
for file in csv_files:
    file_path = os.path.join(folder_path, file)
    df = pd.read_csv(file_path)
    dfs.append(df)

# Concatenate the dataframes into a single dataframe
combined_df = pd.concat(dfs, ignore_index=True)

# Filter out rows where superID is 0
df_filtered = combined_df[combined_df['superID'] != 0]
df_filtered = combined_df[combined_df['nTotal'] > 100]

# Plotting
plt.figure(figsize=(10, 6))
plt.scatter(df_filtered['superID'], df_filtered['efficiency'], alpha=0.7)
plt.title('Efficiency vs SuperID')
plt.xlabel('SuperID')
plt.ylabel('Efficiency')
plt.grid(True)

# Save the plot as a PNG file
plt.savefig(output_file, dpi=300)

# Optionally display the plot
#plt.show()
