#include "xmlUtils.hpp"
#include <rapidxml/rapidxml_print.hpp>

using namespace std;

std::map<int, Station> getStationFromXml(std::string xmlName)
{
    std::map<int, Station> vS;

    rapidxml::xml_document<> doc;
    rapidxml::xml_node<> * root_node = NULL;
    std::ifstream theFile (xmlName);
    if(!theFile.is_open()) {
     	std::cout<< "CANNOT OPEN THE FILE - " << xmlName << std::endl;
        return vS;
    }
    std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);
    root_node = doc.first_node("MUonE_structure");

    for (rapidxml::xml_node<> * station_node = root_node->first_node("Station"); station_node; station_node = station_node->next_sibling()) {
        Station stat;
        stat.nModules = std::stoi(station_node->first_attribute("nModules")->value());
        stat.ID = std::stoi(station_node->first_attribute("stationId")->value());
        //initialize station position...
        stat.pos[0] = std::stod(station_node->first_node("Position")->first_attribute("positionX")->value());
        stat.pos[1] = std::stod(station_node->first_node("Position")->first_attribute("positionY")->value());
        stat.pos[2] = std::stod(station_node->first_node("Position")->first_attribute("positionZ")->value());
        //... and rotation
        stat.rot[0] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationX")->value());
        stat.rot[1] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationY")->value());
        stat.rot[2] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationZ")->value());

        for (rapidxml::xml_node<> * module_node = station_node->first_node("Module"); module_node; module_node = module_node->next_sibling())
        {
            Module m;
            std::string name = module_node->first_attribute("name")->value();
            m.name = name;
            int link = std::stoi(module_node->first_attribute("linkId")->value());
            m.linkId = link;
            int module_number = std::stoi(module_node->first_attribute("number")->value());
            m.module_number   = module_number;

            m.pos[0] = std::stod(module_node->first_node("Position")->first_attribute("positionX")->value());
            m.pos[1] = std::stod(module_node->first_node("Position")->first_attribute("positionY")->value());
            m.pos[2] = std::stod(module_node->first_node("Position")->first_attribute("positionZ")->value());

            m.rot[0] = std::stod(module_node->first_node("Rotation")->first_attribute("rotationX")->value());
            m.rot[1] = std::stod(module_node->first_node("Rotation")->first_attribute("rotationY")->value());
            m.rot[2] = std::stod(module_node->first_node("Rotation")->first_attribute("rotationZ")->value());

            stat.modules.insert(std::pair<int, Module>(module_number, m));
        }

        vS.insert(std::pair<int, Station>(stat.ID, stat));
    }
    return vS;
}

std::map<int, int> getLinkIDMapFromXml(std::string xmlname)
{
    std::cout << "Getting the linkID map" << std::endl;
    std::map<int, int> linkID_modules;
    rapidxml::xml_document<> doc;
    rapidxml::xml_node<> * root_node = NULL;
    std::ifstream theFile (xmlname);
    std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);
    root_node = doc.first_node("MUonE_structure");
    // Iterate over the student nodes
    for (rapidxml::xml_node<> * station_node = root_node->first_node("Station"); station_node; station_node = station_node->next_sibling())
    {
        for (rapidxml::xml_node<> * module_node = station_node->first_node("Module"); module_node; module_node = module_node->next_sibling())
        {
            linkID_modules.insert(std::pair<int, int>(std::stoi(module_node->first_attribute("linkId")->value()),
                                                      std::stoi(module_node->first_attribute("number")->value())));
        }
    }
    return linkID_modules;
}

std::map<int, StationClass> getStationClassFromXml(std::string xmlName, bool considerBend)
{
    std::map<int, StationClass> vS;

    rapidxml::xml_document<> doc;
    rapidxml::xml_node<> * root_node = NULL;
    std::ifstream theFile (xmlName);
    if(!theFile.is_open()) {
     	std::cout<< "CANNOT OPEN THE FILE - " << xmlName << std::endl;
        return vS;
    }
    std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);
    root_node = doc.first_node("MUonE_structure");

    for (rapidxml::xml_node<> * station_node = root_node->first_node("Station"); station_node; station_node = station_node->next_sibling()) {
        StationClass stat;
        stat.nModules = std::stoi(station_node->first_attribute("nModules")->value());
        stat.ID = std::stoi(station_node->first_attribute("stationId")->value());
        //initialize station position...
        stat.pos[0] = std::stod(station_node->first_node("Position")->first_attribute("positionX")->value());
        stat.pos[1] = std::stod(station_node->first_node("Position")->first_attribute("positionY")->value());
        stat.pos[2] = std::stod(station_node->first_node("Position")->first_attribute("positionZ")->value());
        //... and rotation
        stat.rot[0] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationX")->value());
        stat.rot[1] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationY")->value());
        stat.rot[2] = std::stod(station_node->first_node("Rotation")->first_attribute("rotationZ")->value());

        for (rapidxml::xml_node<> * module_node = station_node->first_node("Module"); module_node; module_node = module_node->next_sibling())
        {
            
            std::unique_ptr<ModuleClass> m (new ModuleClass());
            std::string name = module_node->first_attribute("name")->value();
            m->name = name;
            int link = std::stoi(module_node->first_attribute("linkId")->value());
            m->link = link;
            int module_number = std::stoi(module_node->first_attribute("number")->value());
            m->module_number   = module_number;

            m->setSensorSpacing(std::stod(module_node->first_attribute("sensorSpacing")->value()) * 10000);
            m->setBendOffset(std::stod(module_node->first_attribute("bendOffset")->value()));
            m->applyBend(considerBend);
            std::cout << "Applying bend: " << considerBend << "\n";

            m->setRotation(
                std::stod(module_node->first_node("Rotation")->first_attribute("rotationX")->value()) + stat.rot[0],
                std::stod(module_node->first_node("Rotation")->first_attribute("rotationY")->value()) + stat.rot[1],
                std::stod(module_node->first_node("Rotation")->first_attribute("rotationZ")->value()) + stat.rot[2]
            );
            m->setPosition(
                std::stod(module_node->first_node("Position")->first_attribute("positionX")->value()) + stat.pos[0],
                std::stod(module_node->first_node("Position")->first_attribute("positionY")->value()) + stat.pos[1],
                std::stod(module_node->first_node("Position")->first_attribute("positionZ")->value()) + stat.pos[2]
            );

            stat.modules.insert(std::pair<int, ModuleClass>(module_number, *m));
            std::cout << "Plane at link " << m->link << ":\n";
            std::cout << m->plane << std::endl;
        }

        vS.insert(std::pair<int, StationClass>(stat.ID, stat));
    }
    return vS;
}



std::string writeStationToXml(const StationClass& station, std::string outFileName) {
    return "a";
    //using namespace rapidxml;
//
    //// Create xml_document<> object and root node.
    //xml_document<> doc;
    //xml_node<>* root_node = doc.allocate_node(node_element, "MUonE_structure");
    //doc.append_node(root_node);
//
    //// Create Station node.
    //xml_node<>* station_node = doc.allocate_node(node_element, "Station");
    //root_node->append_node(station_node);
//
    //// Set attributes for Station.
    //station_node->append_attribute(doc.allocate_attribute("nModules", std::to_string(station.nModules).c_str()));
    //station_node->append_attribute(doc.allocate_attribute("stationId", std::to_string(station.ID).c_str()));
//
    //// Create Position and Rotation nodes and add to Station node.
    //xml_node<>* position_node = doc.allocate_node(node_element, "Position");
    //position_node->append_attribute(doc.allocate_attribute("positionX", std::to_string(station.pos[0]).c_str()));
    //position_node->append_attribute(doc.allocate_attribute("positionY", std::to_string(station.pos[1]).c_str()));
    //position_node->append_attribute(doc.allocate_attribute("positionZ", std::to_string(station.pos[2]).c_str()));
    //station_node->append_node(position_node);
//
    //xml_node<>* rotation_node = doc.allocate_node(node_element, "Rotation");
    //rotation_node->append_attribute(doc.allocate_attribute("rotationX", std::to_string(station.rot[0]).c_str()));
    //rotation_node->append_attribute(doc.allocate_attribute("rotationY", std::to_string(station.rot[1]).c_str()));
    //rotation_node->append_attribute(doc.allocate_attribute("rotationZ", std::to_string(station.rot[2]).c_str()));
    //station_node->append_node(rotation_node);
//
    //// Loop over modules and create nodes.
    //for (const auto& pair : station.modules) {
    //    const ModuleClass& module = pair.second;
//
    //    xml_node<>* module_node = doc.allocate_node(node_element, "Module");
    //    station_node->append_node(module_node);
//
    //    // Set attributes for Module.
    //    module_node->append_attribute(doc.allocate_attribute("name", module.name.c_str()));
    //    module_node->append_attribute(doc.allocate_attribute("linkId", std::to_string(module.link).c_str()));
    //    module_node->append_attribute(doc.allocate_attribute("number", std::to_string(module.module_number).c_str()));
    //    
    //    xml_node<>* rotationModule_node = doc.allocate_node(node_element, "Rotation");
    //    rotationModule_node->append_attribute(doc.allocate_attribute("rotationX", std::to_string(module.rot[0]).c_str()));
    //    rotationModule_node->append_attribute(doc.allocate_attribute("rotationY", std::to_string(module.rot[1]).c_str()));
    //    rotationModule_node->append_attribute(doc.allocate_attribute("rotationZ", std::to_string(module.rot[2]).c_str()));
    //    module_node->append_node(rotationModule_node);
//
//
    //    xml_node<>* positionModule_node = doc.allocate_node(node_element, "Position");
    //    positionModule_node->append_attribute(doc.allocate_attribute("positionX", std::to_string(module.pos[0]).c_str()));
    //    positionModule_node->append_attribute(doc.allocate_attribute("positionY", std::to_string(module.pos[1]).c_str()));
    //    positionModule_node->append_attribute(doc.allocate_attribute("positionZ", std::to_string(module.pos[2]).c_str()));
    //    module_node->append_node(positionModule_node);
    //}
//
    //// Convert to string and return.
    //std::ostringstream ss;
    //rapidxml::print(std::back_inserter(ss), doc, 0); // convert the xml_document to a string
    //std::string xmlAsString = ss.str();
    //std::ofstream file_out(outFileName);
    //file_out << ss;
    //file_out.close();
    //return xmlAsString;
    ////return oss.str();
}
