#pragma once
#include "ExperimentalSetup.hpp"
#include "TF1.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TTreeReader.hpp"
#include "xmlUtils.hpp"
#include <vector>
#include <set>

struct stubLine {
	Stub stub;
	Line line;
};


class TrackerFromStubs {
  private:
	float MIN_DISTANCE_FOR_ACCEPTANCE = 30 * 5 * 6;

	std::vector<Line> currentLinesFromStubs;
	std::vector<Stub> currentStubs;
	std::vector<stubLine> currentStubLines;
	Track currentTrack;
	std::vector<Track> currentEventTracks;
	std::vector<TGraph *> modulesResolutionPerIteration;
	std::string outputFileName = "output.txt"; // default output file name
	int iteration = 0;
	std::vector<int> modulesToBeUpdated;

  public:
	ExperimentalSetup &experimentSetup;

	TrackerFromStubs(ExperimentalSetup &experimentSetup_) : experimentSetup(experimentSetup_){};
    ~TrackerFromStubs();
	void moveModule(int mod, int axis, double distance);
	void pushStubAndLine(Stub s_, Line line_); //well, maybe I can pass this by reference?
	void printLines();
	bool buildTrackFromCurrentLines();
	Track getCurrentTrack();
	void clear();

	std::vector<Track> buildAllTracksFromCurrentLoadedEventInStation(int stationId);
	std::vector<Track> buildSingleTrackFromCurrentLinesInStation(int stationId);

	void setModulesResolutions(double resolutions[12]);
	void printModulesResolutions();
	void updateResolutionFromFile(std::string fileName);

	void updateResolutionHistogram(int iterarion, double resolutions[12]);
	void createHistograms();
	void setOutputFileName(std::string fileName) {
		outputFileName = fileName;
	};
	void saveHistograms();

	void setModulesToBeUpdated(std::vector<int> modules) {
		modulesToBeUpdated = modules;
	};
};
