#pragma once
#include "Point.hpp"
#include "Vector.hpp"
#include "Line.hpp"

class Plane {
public:
	Point p1, p2, p3;
	Vector v1, v2, v3, normal;
	//default constructor
	Plane() : p1(0, 0, 0), p2(0, 0, 0), p3(0, 0, 0) {}
	Plane(Point p1, Point p2, Point p3) : p1(p1), p2(p2), p3(p3) {
		v1 = Vector(p1);
		v2 = Vector(p2);
		v3 = Vector(p3);
		normal = (v2 - v1) ^ (v3 - v1);
	}
	//constructor from normal and one poin
	Plane(Vector normal, Point p) : p1(p), p2(p), p3(p) {
		v1 = Vector(p1);
		v2 = Vector(p2);
		v3 = Vector(p3);
		this->normal = normal;
	}

	void rotate(double angX, double angY, double angZ);
	void translateX(double posX);
	void translateY(double posY);
	void translateZ(double posZ);

	void print();
	friend std::ostream& operator<<(std::ostream& os, const Plane& p) {
		os << "p1: ";
		os << p.p1 << "\n";
		os << "p2: ";
		os << p.p2 << "\n";
		os << "p3: ";
		os << p.p3 << "\n";
		return os;
	}

	bool operator==(const Plane& p) const;

	double distance(Point q);
	Point intersect(Line l);
	
};